import {Action, action, createStore} from 'easy-peasy';
import createDebugger from 'redux-flipper';
import {Theme, lightTheme, darkTheme} from './theme';
import {Settings, settings} from './settings';
import {Prediction, prediction} from './prediction';

export interface StoreModel {
  theme: Theme;
  settings: Settings;
  prediction: Prediction;
  toggleTheme: Action<StoreModel>;
  setDarkTheme: Action<StoreModel>;
  setLightTheme: Action<StoreModel>;
}

const storeModel: StoreModel = {
  prediction,
  settings,
  theme: lightTheme,
  toggleTheme: action((state) => {
    state.theme = state.theme.dark ? lightTheme : darkTheme;
  }),
  setDarkTheme: action((state) => {
    state.theme = darkTheme;
  }),
  setLightTheme: action((state) => {
    state.theme = lightTheme;
  }),
};

const middleware = [];

if (__DEV__) {
  middleware.push(createDebugger());
}

export const store = createStore(storeModel, {middleware});
