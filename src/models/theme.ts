import color from 'color';

export const lightTheme = {
  dark: false,
  fonts: {
    inputFontSize: 16,
    buttonFontSize: 20,
    captionFontSize: 13,
  },

  colors: {
    primary: '#35D8A6',
    secondary: '#329D9C',
    background: '#fff',
    card: '#ffffff',
    text: '#3D3D3D',
    border: '#c7c7cc',
    primaryDark: '#fff',
    androidRipple: 'rgba(0, 0, 0, .32)',
    boxShadow: '#bbb',
    error: '#dd2c00',
    placeholder: 'rgba(61,61,61,0.6)',
  },
};

lightTheme.colors.primaryDark = color(lightTheme.colors.primary)
  .darken(0.3)
  .toString();

export const darkTheme = {
  dark: true,
  fonts: {
    inputFontSize: 16,
    buttonFontSize: 20,
    captionFontSize: 13,
  },

  colors: {
    text: '#eeeeee',
    primary: '#35D8A6',
    secondary: '#329D9C',
    card: '#393e46',
    background: '#222831',
    border: '#26282b',
    primaryDark: '#fff',
    androidRipple: 'rgba(255, 255, 255, .32)',
    boxShadow: '#000',
    error: '#FF80AB',
    placeholder: '#fff',
  },
};

darkTheme.colors.primaryDark = color(darkTheme.colors.primary)
  .darken(0.3)
  .toString();

export type Theme = typeof lightTheme;

declare module 'styled-components' {
  export interface DefaultTheme extends Theme {}
}
