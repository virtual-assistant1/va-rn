import {Action, action, Thunk, thunk} from 'easy-peasy';
import {client} from 'apollo.client';
import {PredictDocument} from 'generated/types';
import moment from 'moment';

type GenderType = 'male' | 'female';

type QuestionPayload = {
  question: number;
  answers: number[];
  questionType: string;
};

type PopQuestionPayload = {
  question: number;
  questionType: string;
};

type PredictionInput = {
  gender: GenderType;
  dob: number;
  weight: number;
  height: number;
  smoke: number;
  alcoholic: number;
  physicalActivity: number;
  cholesterol: number;
  glucose: number;
  systolic: number;
  diastolic: number;
};

export interface Prediction {
  dataCollected: boolean;
  questionsCount: number;
  questionsTotal: number;
  predictionData: PredictionInput;
  predictionResult: null | boolean;
  questions: Map<number, number[]>;
  setQuestionTotal: Action<Prediction, number>;
  pushQuestion: Action<Prediction, QuestionPayload>;
  popQuestion: Action<Prediction, PopQuestionPayload>;
  resetQuestions: Action<Prediction>;
  setPredictionData: Action<Prediction, PredictionInput>;
  setPredictionResult: Action<Prediction, boolean | null>;
  predict: Thunk<Prediction, PredictionInput>;
}

export const prediction: Prediction = {
  questionsCount: 0,
  questionsTotal: 0,
  dataCollected: false,
  predictionResult: null,
  predictionData: {
    alcoholic: 0,
    cholesterol: 0,
    diastolic: 0,
    dob: 0,
    gender: 'female',
    glucose: 0,
    height: 0,
    physicalActivity: 0,
    smoke: 0,
    systolic: 0,
    weight: 0,
  },
  questions: new Map(),
  setQuestionTotal: action((state, count) => {
    state.questionsTotal = count;
  }),
  pushQuestion: action((state, payload) => {
    state.questions.set(payload.question, payload.answers);
    if (payload.questionType === 'main') {
      state.questionsCount = state.questionsCount + 1;
    }
  }),

  popQuestion: action((state, payload) => {
    if (state.questions.has(payload.question)) {
      state.questions.delete(payload.question);
      if (payload.questionType === 'main') {
        state.questionsCount = state.questionsCount - 1;
      }
    }
  }),
  resetQuestions: action((state) => {
    state.questions.clear();
    state.questionsCount = 0;
    state.questionsTotal = 0;
  }),

  setPredictionData: action((state, data) => {
    state.predictionData = data;
    state.dataCollected = true;
  }),

  setPredictionResult: action((state, result) => {
    state.predictionResult = result;
  }),

  predict: thunk((actions, input) => {
    actions.setPredictionData(input);
    const age = moment().diff(moment.unix(input.dob), 'years');

    client
      .query({
        query: PredictDocument,
        variables: {
          active: input.physicalActivity,
          age_year: age,
          alcohol: input.alcoholic,
          cholesterol: input.cholesterol,
          diastolic: input.diastolic,
          gender: input.gender === 'male' ? 2 : 1,
          glucose: input.glucose,
          height: input.height,
          smoke: input.smoke,
          systolic: input.systolic,
          weight: input.weight,
        },
      })
      .then(({data}) => {
        if (data.predict) {
          if (data.predict.message === 'You have no Cardiovascular') {
            actions.setPredictionResult(false);
          } else if (data.predict.message === 'You have Cardiovascular') {
            actions.setPredictionResult(true);
          } else {
            actions.setPredictionResult(null);
          }
        }
      })
      .catch((e) => {
        console.log(e);
      });
  }),
};
