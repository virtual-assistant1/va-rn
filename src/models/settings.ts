import {Action, action} from 'easy-peasy';

export interface Settings {
  isAuth: boolean | null;
  setIsAuth: Action<Settings, boolean>;
}

export const settings: Settings = {
  isAuth: null,
  setIsAuth: action((state, isAuth) => {
    state.isAuth = isAuth;
  }),
};
