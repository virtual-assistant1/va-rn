import React, {useEffect} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {Gradient, Icon} from 'components';
import {
  Login,
  RegisterStack,
  Splash,
  UserPropsStack,
  PredictorStack,
  HomeStack,
  RegisterUserPropsStack,
  ProfileStack,
  SettingsStack,
  AppointmentStack,
  ValidationStack,
  SideMenu,
} from 'screens';

import {useAction, useStore} from 'hooks';
import AsyncStorage from '@react-native-community/async-storage';
import config from 'config';
import {MeDocument} from 'generated/types';
import {useLazyQuery} from '@apollo/client';

type AuthStackParams = {
  HomeStack: undefined;
  PredictorStack: undefined;
  userPropsStack: undefined;
  Assessment: undefined;
  RegisterUserPropsStack: undefined;
  ProfileStack: undefined;
  SettingsStack: undefined;
  AppointmentStack: undefined;
  ValidationStack: undefined;
};

const Drawer = createDrawerNavigator<AuthStackParams>();

const AuthNavigator = () => (
  <Drawer.Navigator
    initialRouteName="HomeStack"
    drawerContent={(props) => <SideMenu {...props} />}>
    <Drawer.Screen
      component={HomeStack}
      name="HomeStack"
      options={{
        drawerLabel: 'Home',
        drawerIcon: ({color, size}) => (
          <Icon name="home" style={{color, fontSize: size}} />
        ),
      }}
    />
    <Drawer.Screen
      component={UserPropsStack}
      name="userPropsStack"
      options={{drawerLabel: 'none'}}
    />
    <Drawer.Screen
      component={RegisterUserPropsStack}
      name="RegisterUserPropsStack"
      options={{drawerLabel: 'none'}}
    />
    <Drawer.Screen
      component={PredictorStack}
      name="PredictorStack"
      options={{drawerLabel: 'none'}}
    />
    <Drawer.Screen
      component={ProfileStack}
      name="ProfileStack"
      options={{
        drawerLabel: 'Diagnostic History',
        drawerIcon: ({color, size}) => (
          <Icon
            name="diagnoses"
            type="FontAwesome5"
            style={{color, fontSize: size}}
          />
        ),
      }}
    />
    <Drawer.Screen
      component={SettingsStack}
      name="SettingsStack"
      options={{
        drawerLabel: 'Profile Settings',
        drawerIcon: ({color, size}) => (
          <Icon
            name="settings"
            type="Feather"
            style={{color, fontSize: size}}
          />
        ),
      }}
    />
    <Drawer.Screen
      component={AppointmentStack}
      name="AppointmentStack"
      options={{drawerLabel: 'none'}}
    />
    <Drawer.Screen
      component={ValidationStack}
      name="ValidationStack"
      options={{drawerLabel: 'none'}}
    />
  </Drawer.Navigator>
);

const useAuth = () => {
  const setIsAuth = useAction((actions) => actions.settings.setIsAuth);
  const [getUser] = useLazyQuery(MeDocument, {
    onCompleted: (e) => {
      if (e.me) {
        setIsAuth(true);
      } else {
        setIsAuth(false);
      }
    },
  });

  useEffect(() => {
    setTimeout(() => {
      AsyncStorage.getItem(config.accessTokenKey).then((v) => {
        if (v) {
          //start loading user
          getUser();
        } else {
          setIsAuth(false);
        }
      });
    }, 1000);
  }, [setIsAuth, getUser]);

  return useStore((state) => state.settings.isAuth);
};

const Root = createStackNavigator();

export const Navigator = () => {
  const isAuth = useAuth();

  return (
    <Root.Navigator
      screenOptions={{
        headerShown: false,
        headerBackground: () => <Gradient />,
        headerTintColor: '#fff',
      }}>
      {isAuth === null ? (
        <Root.Screen name="Splash" component={Splash} />
      ) : isAuth === true ? (
        <Root.Screen name="authStack" component={AuthNavigator} />
      ) : (
        <>
          <Root.Screen component={Login} name="Login" />
          <Root.Screen
            component={RegisterStack}
            name="RegisterStack"
            options={{headerShown: true}}
          />
        </>
      )}
    </Root.Navigator>
  );
};
