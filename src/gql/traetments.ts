import {gql} from '@apollo/client';

export const diseaseTreatments = gql`
  query diseaseTreatments($diseaseCode: Float!) {
    diseaseTreatments(diseaseCode: $diseaseCode) {
      id
      title
      summary
      url
      diseaseCode
      createdAt
      updatedAt
    }
  }
`;
