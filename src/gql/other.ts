import {gql} from '@apollo/client';

export const doctors = gql`
  query doctors {
    doctors {
      id
      code
      specialization
      name
      address
      telephone
      mobile
      city
      experience
      openingTime
      closingTime
      createdAt
      updatedAt
    }
  }
`;

export const createAppointment = gql`
  mutation createAppointment($appointment: CreateAppointmentDto!) {
    createAppointment(appointment: $appointment) {
      id
      doctorId
      userId
      date
      time
      email
      mobile
      name
      createdAt
      updatedAt
    }
  }
`;

export const userAppointments = gql`
  query userAppointments {
    appointments {
      id
      doctorId
      userId
      date
      time
      email
      mobile
      name
      createdAt
      updatedAt
    }
  }
`;
