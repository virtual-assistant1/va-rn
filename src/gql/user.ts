import {gql} from '@apollo/client';

export const createUser = gql`
  mutation createUser($user: CreateDto!) {
    createUser(user: $user) {
      id
      email
      name
      gender
      dob
      height
      weight
      createdAt
      updatedAt
    }
  }
`;

export const login = gql`
  mutation login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      user {
        id
        email
        name
        createdAt
        updatedAt
      }
      accessToken
      refreshToken
    }
  }
`;

export const me = gql`
  query me {
    me {
      id
      email
      name
      gender
      weight
      height
      dob
      userType
      createdAt
      updatedAt
    }
  }
`;

export const updateUser = gql`
  mutation updateUser($user: UpdateDto!) {
    updateUser(user: $user) {
      id
      email
      name
      gender
      dob
      weight
      height
      userType
      createdAt
      updatedAt
    }
  }
`;
