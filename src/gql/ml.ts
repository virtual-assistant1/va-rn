import {gql} from '@apollo/client';

export const questionsCount = gql`
  query questionCount($questionType: String!, $parentQuestion: Float!) {
    questionCount(questionType: $questionType, parentQuestion: $parentQuestion)
  }
`;

export const predict = gql`
  query predict(
    $systolic: Float!
    $diastolic: Float!
    $age_year: Float!
    $cholesterol: Float!
    $weight: Float!
    $height: Float!
    $gender: Float!
    $alcohol: Float!
    $smoke: Float!
    $glucose: Float!
    $active: Float!
  ) {
    predict(
      systolic: $systolic
      diastolic: $diastolic
      age_year: $age_year
      cholesterol: $cholesterol
      weight: $weight
      height: $height
      gender: $gender
      alcohol: $alcohol
      smoke: $smoke
      glucose: $glucose
      active: $active
    ) {
      error
      message
    }
  }
`;

export const getFirstQuestion = gql`
  query getFirstQuestion {
    firstQuestions {
      id
      code
      question
      url
      parentQuestion
      questionType
      description {
        arabic
        english
      }
      answers {
        id
        code
        answer
      }
    }
  }
`;

export const getQuestions = gql`
  query getQuestions($answer: Float!) {
    questions(answer: $answer) {
      id
      code
      question
      url
      parentQuestion
      questionType
      description {
        arabic
        english
      }
      answers {
        id
        code
        answer
      }
    }
  }
`;

export const evaluate = gql`
  query evaluate($questions: [QADto!]!) {
    evaluateAnswers(questions: $questions) {
      id
      code
      disease
      rank
    }
  }
`;

export const saveEvaluation = gql`
  mutation savePrediction(
    $questions: [QADto!]!
    $userProps: SaveUserPropsDto!
    $diseases: [SaveDiseasesDto!]!
    $risk: Boolean!
  ) {
    saveEvaluation(
      questions: $questions
      userProps: $userProps
      diseases: $diseases
      risk: $risk
    ) {
      id
      user {
        userId
      }
      questions {
        id
        code
        question
      }
      diseases {
        id
        code
        disease
      }
      risk
      validated
      validationResult
    }
  }
`;

export const registerValidation = gql`
  mutation registerValidation($validation: RegisterValidation!) {
    registerValidation(validation: $validation) {
      id
      user {
        userId
      }
      questions {
        id
        code
        question
      }
      diseases {
        id
        code
        disease
      }
      risk
      validated
      validationResult
    }
  }
`;

export const userEvaluation = gql`
  query userEvaluation {
    userEvaluations {
      id
      risk
      validated
      validationResult
      user {
        userId
        gender
        age_year
        weight
        height
        alcohol
        smoke
        glucose
        active
        cholesterol
        systolic
        diastolic
      }
      createdAt
      diseases {
        id
        code
        disease
        rank
      }
      questions {
        id
        code
        question
        answers {
          id
          code
          answer
        }
      }
    }
  }
`;
