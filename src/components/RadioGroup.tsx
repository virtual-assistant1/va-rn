import React, {FC, useState} from 'react';
import {TouchableOpacity} from 'react-native';
import Animated, {Easing} from 'react-native-reanimated';
import {makeStyles} from 'hooks';
import {View} from './View';
import {Text} from './Text';

import {useTransition} from 'react-native-redash';

interface Item {
  id: any;
  label: string;
}

interface RadioGroupProps {
  options: Item[] | undefined;
  onChange: (ev: Item) => void;
  activeButtonId?: any;
  horizontal?: boolean;
  circleStyle?: any;
  itemStyle?: any;
}

export const RadioGroup: FC<RadioGroupProps> = ({
  activeButtonId,
  options,
  horizontal,
  itemStyle,
  onChange,
}) => {
  const [selected, setSelected] = useState(activeButtonId);
  const {styles} = useStyles();
  return (
    <View style={[styles.container, horizontal && {flexDirection: 'row'}]}>
      {options?.map((option) => (
        <TouchableOpacity
          key={option.id}
          style={[styles.radio, itemStyle]}
          onPress={() => {
            setSelected(option.id);
            if (onChange) {
              onChange(option);
            }
          }}>
          <Circle active={selected === option.id} />
          <Text style={styles.label}>{option.label}</Text>
        </TouchableOpacity>
      ))}
    </View>
  );
};

const Circle: FC<{active: boolean}> = ({active}) => {
  const {styles} = useStyles();
  const scale = useTransition(active, {
    duration: 200,
    easing: Easing.inOut(Easing.ease),
  });
  return (
    <View style={styles.circle}>
      <Animated.View style={[styles.fill, {transform: [{scale}]}]} />
    </View>
  );
};

const useStyles = makeStyles(({theme}) => ({
  container: {
    flexWrap: 'wrap',
  },
  radio: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
    marginRight: 10,
  },

  circle: {
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',

    width: 22,
    height: 22,
    borderColor: theme.dark ? '#ffffff80' : '#00000080',
    borderWidth: 0.8,
    marginRight: 10,
  },
  fill: {
    borderRadius: 8,
    backgroundColor: theme.colors.secondary,
    width: 16,
    height: 16,
    transform: [{scale: 0}],
  },
  label: {
    opacity: theme.dark ? 1 : 0.9,
  },
}));
