import React, {FC} from 'react';
import {ScrollView} from 'react-native';
import JSONTree from 'react-native-json-tree';
import {useTheme} from 'hooks';

export const Error: FC<{error: any}> = ({error}) => {
  const {colors} = useTheme();
  const theme = {
    base00: colors.card,
    base01: '#282a2e',
    base02: '#373b41',
    base03: '#969896',
    base04: '#b4b7b4',
    base05: '#c5c8c6',
    base06: '#e0e0e0',
    base07: '#ffffff',
    base08: '#CC342B',
    base09: '#F96A38',
    base0A: '#FBA922',
    base0B: '#198844',
    base0C: '#3971ED',
    base0D: '#3971ED',
    base0E: '#A36AC7',
    base0F: '#3971ED',
  };
  return (
    <ScrollView>
      <JSONTree data={error} theme={theme} invertTheme={false} />
    </ScrollView>
  );
};
