import React from 'react';
import Spinkit, {SpinnerProps} from 'react-native-spinkit';

export const Loader: React.FC<SpinnerProps> = (props) => {
  return <Spinkit type="Bounce" size={20} {...props} />;
};
