import styled from 'styled-components/native';
import {View} from './View';

export const Card = styled(View)`
  background: ${({theme}) => theme.colors.card};
  border-radius: 5px;
  box-shadow: 0px 1px 2px ${({theme: {colors}}) => colors.boxShadow};
  elevation: 2;
`;
