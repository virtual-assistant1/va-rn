import React, {FC} from 'react';
import {StyleSheet} from 'react-native';
import {makeStyles} from 'hooks';
import {Icon} from '../Icon';
import {View} from '../View';
import {Text} from '../Text';
import {Picker as PickerRaw} from '@react-native-community/picker';
import {PickerProps} from './props';

export const AndroidPicker: FC<PickerProps> = ({
  value,
  error,
  icon,
  placeholder,
  onChangeText,
  onBlur,
  items,
}) => {
  const {styles, theme} = useStyles();

  return (
    <View style={styles.root}>
      <View style={styles.container}>
        {icon && (
          <Icon
            style={[
              styles.icon,
              {
                color: error ? theme.colors.error : theme.colors.primary,
              },
            ]}
            {...icon}
          />
        )}
        <View style={styles.textContainer}>
          <PickerRaw
            prompt={placeholder}
            selectedValue={value}
            onValueChange={(v) => {
              if (onChangeText) {
                onChangeText(v);
              }
              if (onBlur) {
                onBlur();
              }
            }}>
            {items.map((i, index) => (
              <PickerRaw.Item
                value={i.value}
                label={i.label}
                key={`${i.value}-${index}`}
                color={i.color || theme.colors.text}
              />
            ))}
          </PickerRaw>
        </View>
      </View>
      {error && <Text style={styles.error}>{error}</Text>}
    </View>
  );
};

const useStyles = makeStyles(({theme}) => ({
  root: {
    backgroundColor: theme.colors.card,
    paddingHorizontal: 10,
    marginBottom: 10,
  },
  container: {
    flexDirection: 'row',
    borderBottomColor: theme.colors.border,
    borderBottomWidth: StyleSheet.hairlineWidth,
    alignItems: 'center',
  },
  text: {
    color: theme.colors.text,
    fontSize: theme.fonts.inputFontSize,
  },
  textContainer: {
    flex: 1,
    height: 40,
    justifyContent: 'center',
  },
  iconButton: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 5,
  },
  error: {
    color: theme.colors.error,
    fontSize: 12,
  },
  icon: {
    fontSize: 24,
    marginRight: 5,
  },
  placeholder: {
    color: theme.colors.placeholder,
    fontSize: theme.fonts.inputFontSize,
  },
}));
