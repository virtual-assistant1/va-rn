import {IconProps} from '../Icon';

export interface PickerItemProps {
  value: string;
  label: string;
  color?: string;
  data?: any;
}
export interface PickerProps {
  items: PickerItemProps[];
  error?: string;
  icon?: IconProps;
  confirmText?: string;
  cancelText?: string;
  placeholder?: string;
  value?: any;
  onChangeText?: (date: any) => void;
  onBlur?: () => void;
}
