import React, {FC} from 'react';
import {Platform} from 'react-native';
import {AndroidPicker} from './AndroidPicker';
import {IOSPicker} from './IOSPicker';
import {PickerProps} from './props';

export const Picker: FC<PickerProps> = ({...rest}) => {
  return Platform.OS === 'ios' ? (
    <IOSPicker {...rest} />
  ) : (
    <AndroidPicker {...rest} />
  );
};
