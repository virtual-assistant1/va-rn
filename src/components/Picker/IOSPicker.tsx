import React, {FC, useState} from 'react';
import {StyleSheet} from 'react-native';
import {makeStyles} from 'hooks';
import {Icon} from '../Icon';
import {View} from '../View';
import {Text} from '../Text';
import {Button} from '../Button';
import {RectButton} from 'react-native-gesture-handler';
import {Picker as PickerRaw} from '@react-native-community/picker';
import Modal from 'react-native-modal';
import {PickerProps} from './props';

export const IOSPicker: FC<PickerProps> = ({
  value,
  error,
  icon,
  placeholder,
  confirmText,
  cancelText,
  onChangeText,
  onBlur,
  items,
}) => {
  const {styles, theme} = useStyles();
  const [open, setOpen] = useState(false);
  const [selectedValue, setSelectedValue] = useState('');

  return (
    <>
      <View style={styles.root}>
        <View style={styles.container}>
          {icon && (
            <Icon
              style={[
                styles.icon,
                {
                  color: error ? theme.colors.error : theme.colors.primary,
                },
              ]}
              {...icon}
            />
          )}
          <View style={styles.textContainer}>
            {value !== null && value !== '' ? (
              <Text style={styles.text}>
                {items.find((i) => i.value === value)?.label}
              </Text>
            ) : (
              <Text style={styles.placeholder}>{placeholder}</Text>
            )}
          </View>
        </View>
        {error && <Text style={styles.error}>{error}</Text>}
        <RectButton
          style={StyleSheet.absoluteFill}
          onPress={() => {
            setOpen(true);
          }}
        />
      </View>
      <Modal
        style={styles.modal}
        isVisible={open}
        backdropOpacity={0.3}
        onBackdropPress={() => {
          setOpen(false);
        }}
        onBackButtonPress={() => {
          setOpen(false);
        }}>
        <View style={styles.iosPicker}>
          <View style={styles.iosControls}>
            <Button
              style={styles.iosDone}
              title={cancelText || 'Cancel'}
              onPress={() => {
                setOpen(false);
                if (onBlur) {
                  onBlur();
                }
              }}
            />
            <Text>{placeholder}</Text>
            <Button
              style={styles.iosDone}
              title={confirmText || 'Confirm'}
              onPress={() => {
                setOpen(false);
                if (onBlur) {
                  onBlur();
                }
                if (onChangeText) {
                  onChangeText(
                    selectedValue.length > 0 ? selectedValue : items[0].value,
                  );
                }
              }}
            />
          </View>
          <PickerRaw
            selectedValue={selectedValue}
            onValueChange={(v) => {
              setSelectedValue(v.toString());
            }}>
            {items.map((i, index) => (
              <PickerRaw.Item
                value={i.value}
                label={i.label}
                key={`${i.value}-${index}`}
                color={i.color || theme.colors.text}
              />
            ))}
          </PickerRaw>
        </View>
      </Modal>
    </>
  );
};

const useStyles = makeStyles(({theme}) => ({
  root: {
    backgroundColor: theme.colors.card,
    paddingHorizontal: 10,
    marginBottom: 10,
  },
  container: {
    flexDirection: 'row',
    borderBottomColor: theme.colors.border,
    borderBottomWidth: StyleSheet.hairlineWidth,
    alignItems: 'center',
  },
  text: {
    color: theme.colors.text,
    fontSize: theme.fonts.inputFontSize,
  },
  textContainer: {
    flex: 1,
    height: 40,
    justifyContent: 'center',
  },
  iconButton: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 5,
  },
  error: {
    color: theme.colors.error,
    fontSize: 12,
  },
  icon: {
    fontSize: 24,
    marginRight: 5,
  },
  placeholder: {
    color: theme.colors.placeholder,
    fontSize: theme.fonts.inputFontSize,
  },
  buttonsContainer: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonsText: {
    color: theme.colors.primary,
    fontSize: theme.fonts.buttonFontSize,
  },
  iosPicker: {
    backgroundColor: theme.colors.background,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    overflow: 'hidden',
  },
  iosControls: {
    backgroundColor: theme.colors.card,
    height: 48,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  modal: {
    margin: 0,
  },
  iosDone: {
    padding: 10,
    marginHorizontal: 10,
  },
}));
