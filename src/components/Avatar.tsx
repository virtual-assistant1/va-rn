import React, {FC} from 'react';
import {ViewProps, StyleSheet, Platform} from 'react-native';

import {Image} from './Image';
import {View} from './View';
import {Touchable} from './MenuItem';
import {makeStyles} from 'hooks';

const defaultImage = require('assets/avatar.png');

export interface AvatarProps extends ViewProps {
  onPress?: () => void;
  image?: any;
  size: number;
}

export const Avatar: FC<AvatarProps> = ({onPress, image, size}) => {
  const {styles} = useStyles();
  const WrappedComponent = onPress ? Touchable : View;
  return (
    <View
      style={[
        {
          borderRadius: size / 2,
          width: size,
          height: size,
        },
        styles.root,
      ]}>
      {Platform.OS === 'android' ? (
        <Image
          source={image || defaultImage}
          resizeMode="stretch"
          style={[{width: size, height: size, borderRadius: size / 2}]}>
          <WrappedComponent onPress={onPress}>
            <View style={[StyleSheet.absoluteFill, {borderRadius: size / 2}]} />
          </WrappedComponent>
        </Image>
      ) : (
        <WrappedComponent onPress={onPress}>
          <Image
            source={image || defaultImage}
            resizeMode="stretch"
            style={[{width: size, height: size, borderRadius: size / 2}]}
          />
        </WrappedComponent>
      )}
    </View>
  );
};

const useStyles = makeStyles(({theme}) => ({
  root: {
    shadowColor: theme.colors.boxShadow,
    shadowOffset: {width: 2, height: 2},
    shadowRadius: 3,
    shadowOpacity: 0.3,
    // elevation: 3,
  },
}));
