import React, {FC, forwardRef, RefObject} from 'react';
import {TextInput, TextInputProps, StyleSheet, TextStyle} from 'react-native';
import {Icon, IconProps} from './Icon';
import {IconButton} from './IconButton';
import {Text} from './Text';
import {View} from './View';
import {makeStyles} from 'hooks';
import {Loader} from './Loader';

export interface InputProps extends TextInputProps {
  error?: string;
  icon?: IconProps;
  ref?: RefObject<TextInput>;
  value?: any;
  loading?: boolean;
  disabled?: boolean;
}

export const Input: FC<InputProps> = forwardRef(
  (
    {style, error, icon, value, onChangeText, loading, disabled, ...rest},
    ref,
  ) => {
    const {styles, theme} = useStyles();
    const customStyle = Array.isArray(style)
      ? [styles.input, ...style]
      : style
      ? {...styles.input, ...(style as TextStyle)}
      : {...styles.input};
    return (
      <View style={styles.root}>
        <View style={styles.container}>
          {icon && (
            <Icon
              style={[
                styles.icon,
                {
                  color: error ? theme.colors.error : theme.colors.primary,
                },
              ]}
              {...icon}
            />
          )}
          {disabled ? (
            <View style={styles.disabledContainer}>
              <Text style={styles.disabled}>
                {value || rest.placeholder || ''}
              </Text>
            </View>
          ) : (
            <TextInput
              ref={ref}
              style={customStyle}
              onChangeText={onChangeText}
              value={`${value}`}
              placeholderTextColor={theme.colors.placeholder}
              {...rest}
            />
          )}
          {value !== '' && !loading && (
            <IconButton
              icon={{
                name: 'close',
                style: [
                  styles.closeIcon,
                  {color: error ? theme.colors.error : theme.colors.primary},
                ],
              }}
              style={styles.iconButton}
              onPress={() => {
                if (onChangeText) {
                  onChangeText!('');
                }
              }}
            />
          )}

          {loading && <Loader color={theme.colors.primary} />}
        </View>
        {error && <Text style={styles.error}>{error}</Text>}
      </View>
    );
  },
);

const useStyles = makeStyles(({theme}) => ({
  root: {
    marginBottom: 10,
  },
  container: {
    paddingHorizontal: 10,
    backgroundColor: theme.colors.card,
    flexDirection: 'row',
    borderBottomColor: theme.colors.border,
    borderBottomWidth: StyleSheet.hairlineWidth,
    alignItems: 'center',
  },
  input: {
    height: 40,
    flex: 1,
    color: theme.colors.text,
    fontSize: theme.fonts.inputFontSize,
  },
  disabledContainer: {
    height: 40,
    flex: 1,
    marginLeft: 3,
    justifyContent: 'center',
  },
  disabled: {
    color: theme.colors.text,
    fontSize: theme.fonts.inputFontSize,
  },
  iconButton: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 5,
  },
  error: {
    color: theme.colors.error,
    fontSize: 12,
  },
  icon: {
    fontSize: 24,
    marginRight: 5,
  },
  closeIcon: {
    fontSize: 20,
  },
}));
