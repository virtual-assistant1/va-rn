import React, {FC} from 'react';
import {
  TextProps,
  ViewProps,
  StyleSheet,
  TouchableOpacity,
  TouchableNativeFeedback,
  TouchableOpacityProps,
  TouchableNativeFeedbackProps,
  Platform,
} from 'react-native';
import styled from 'styled-components/native';
import {makeStyles, useTheme} from 'hooks';
import {View} from './View';
import {Text} from './Text';
import {Icon, IconProps} from './Icon';

export interface MenuItemProps extends ViewProps {
  title?: string;
  subTitle?: string;
  titleProps?: TextProps;
  subTitleProps?: TextProps;
  leftIcon?: IconProps;
  rightIcon?: IconProps;
  onPress?: () => void;
  active?: boolean;
  chevron?: boolean;
}

export const Touchable: FC<
  TouchableOpacityProps | TouchableNativeFeedbackProps
> = ({...rest}) => {
  const theme = useTheme();
  return Platform.OS === 'android' ? (
    <TouchableNativeFeedback
      hitSlop={Platform.select({
        ios: undefined,
        default: {top: 5, right: 5, bottom: 5, left: 5},
      })}
      useForeground={TouchableNativeFeedback.canUseNativeForeground()}
      background={TouchableNativeFeedback.Ripple(
        theme.colors.androidRipple,
        false,
      )}
      {...rest}
    />
  ) : (
    <TouchableOpacity {...rest} />
  );
};

const MenuItemCustom: FC<MenuItemProps> = ({
  title,
  titleProps,
  leftIcon,
  rightIcon,
  subTitle,
  subTitleProps,
  onPress,
  active,
  chevron,
}) => {
  const {styles, theme} = useStyles();

  return (
    <Touchable onPress={onPress}>
      <View style={styles.container}>
        {leftIcon && (
          <Icon
            style={[
              styles.icons,
              {color: active ? theme.colors.primary : theme.colors.text},
            ]}
            {...leftIcon}
          />
        )}
        <View style={[styles.rowContainer]}>
          <View style={styles.textContainer}>
            <Text
              style={{color: active ? theme.colors.primary : theme.colors.text}}
              {...titleProps}>
              {title}
            </Text>
            {subTitle && (
              <Text
                style={[
                  styles.subTitle,
                  {color: active ? theme.colors.primary : theme.colors.text},
                ]}
                {...subTitleProps}>
                {subTitle}
              </Text>
            )}
          </View>
          {rightIcon && (
            <Icon
              style={[
                styles.icons,
                {color: active ? theme.colors.primary : theme.colors.text},
              ]}
              {...rightIcon}
            />
          )}
          {chevron && (
            <Icon
              name={'chevron-right'}
              type="Feather"
              style={[
                styles.icons,
                {color: active ? theme.colors.primary : theme.colors.text},
              ]}
            />
          )}
        </View>
      </View>
    </Touchable>
  );
};

const useStyles = makeStyles(({theme}) => ({
  container: {
    flexDirection: 'row',
    backgroundColor: theme.colors.card,
    alignItems: 'center',
  },
  icons: {
    padding: 10,
  },
  textContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  rowContainer: {
    flexDirection: 'row',
    flex: 1,
    borderBottomColor: theme.colors.border,
    borderBottomWidth: StyleSheet.hairlineWidth,
    alignItems: 'center',
  },
  subTitle: {
    opacity: 0.5,
    fontSize: theme.fonts.captionFontSize,
  },
}));

export const MenuItem = styled(MenuItemCustom)``;
