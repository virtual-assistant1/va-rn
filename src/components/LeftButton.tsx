import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {DrawerNavigationProp} from '@react-navigation/drawer';
import {IconButton} from './IconButton';
import {makeStyles} from 'hooks';

export const LeftButton = () => {
  const navigation = useNavigation<DrawerNavigationProp<any>>();
  const {styles} = useStyles();

  return (
    <IconButton
      style={styles.icon}
      icon={{name: 'menu', style: {color: '#fff'}}}
      onPress={() => {
        navigation.toggleDrawer();
      }}
    />
  );
};

const useStyles = makeStyles(() => ({
  icon: {
    marginHorizontal: 10,
    padding: 5,
  },
}));
