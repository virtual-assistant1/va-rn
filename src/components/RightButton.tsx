import React from 'react';
import {IconButton} from './IconButton';
import {makeStyles, useAction} from 'hooks';
import AsyncStorage from '@react-native-community/async-storage';
import config from 'config';
import {client} from 'apollo.client';

export const RightButton = () => {
  const setIsAuth = useAction((actions) => actions.settings.setIsAuth);
  const {styles} = useStyles();

  return (
    <IconButton
      style={styles.icon}
      icon={{
        name: 'logout',
        type: 'MaterialCommunityIcons',
        style: {color: '#fff'},
      }}
      onPress={async () => {
        setIsAuth(false);
        await AsyncStorage.removeItem(config.accessTokenKey);
        await client.resetStore();
      }}
    />
  );
};

const useStyles = makeStyles(() => ({
  icon: {
    marginHorizontal: 10,
    padding: 5,
  },
}));
