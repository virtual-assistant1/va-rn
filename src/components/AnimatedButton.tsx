import React from 'react';
import Animated, {Easing} from 'react-native-reanimated';
import {TapGestureHandler, State} from 'react-native-gesture-handler';
import {onGestureEvent, timing} from 'react-native-redash';

const {
  Value,
  useCode,
  onChange,
  block,
  eq,
  cond,
  call,
  or,
  set,
  Clock,
} = Animated;

type Props = {
  onPress: () => void;
  children: React.ReactNode;
  animation: Animated.Value<number>;
};

const AnimatedButton = ({onPress, children, animation}: Props) => {
  const clock = new Clock();
  const state = new Value(State.UNDETERMINED);
  const shouldAnimate = new Value(-1);
  const handelGesture = onGestureEvent({state});
  useCode(
    () =>
      block([
        onChange(state, cond(eq(state, State.END), call([], onPress))),
        cond(eq(state, State.BEGAN), set(shouldAnimate, 1)),
        cond(
          or(
            eq(state, State.CANCELLED),
            eq(state, State.FAILED),
            eq(state, State.END),
          ),
          set(shouldAnimate, 0),
        ),
        cond(
          eq(shouldAnimate, 1),
          set(
            animation,
            timing({
              clock,
              from: 0,
              to: 1,
              duration: 250,
              easing: Easing.elastic(1.3),
            }),
          ),
        ),
        cond(
          eq(shouldAnimate, 0),
          set(
            animation,
            timing({
              clock,
              from: 1,
              to: 0,
              duration: 250,
            }),
          ),
        ),
      ]),
    [],
  );
  return <TapGestureHandler {...handelGesture}>{children}</TapGestureHandler>;
};

export default AnimatedButton;
