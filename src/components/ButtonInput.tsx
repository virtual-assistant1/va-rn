import React, {FC} from 'react';
import {StyleSheet} from 'react-native';
import {makeStyles} from 'hooks';
import {Icon, IconProps} from './Icon';
import {View} from './View';
import {Text} from './Text';
import {RectButton} from 'react-native-gesture-handler';

export interface ButtonInputProps {
  value?: string;
  error?: string;
  icon?: IconProps;
  placeholder?: string;
  onPress: () => void;
  renderIcon?: () => JSX.Element;
  renderValue?: () => JSX.Element;
}

export const ButtonInput: FC<ButtonInputProps> = ({
  value,
  error,
  icon,
  placeholder,
  onPress,
  renderIcon,
  renderValue,
}) => {
  const {styles, theme} = useStyles();

  return (
    <View style={styles.root}>
      <View style={styles.container}>
        {renderIcon && renderIcon()}
        {icon && (
          <Icon
            style={[
              styles.icon,
              {
                color: error ? theme.colors.error : theme.colors.primary,
              },
            ]}
            {...icon}
          />
        )}
        <View style={styles.textContainer}>
          {renderValue ? (
            renderValue()
          ) : value ? (
            <Text style={styles.text}>{value}</Text>
          ) : (
            <Text style={styles.placeholder}>{placeholder}</Text>
          )}
        </View>
      </View>
      {error && <Text style={styles.error}>{error}</Text>}
      <RectButton style={StyleSheet.absoluteFill} onPress={onPress} />
    </View>
  );
};

const useStyles = makeStyles(({theme}) => ({
  root: {
    backgroundColor: theme.colors.card,
    paddingHorizontal: 10,
    marginBottom: 10,
  },
  container: {
    flexDirection: 'row',
    borderBottomColor: theme.colors.border,
    borderBottomWidth: StyleSheet.hairlineWidth,
    alignItems: 'center',
  },
  text: {
    color: theme.colors.text,
    fontSize: theme.fonts.inputFontSize,
  },
  textContainer: {
    flex: 1,
    height: 40,
    justifyContent: 'center',
  },
  error: {
    color: theme.colors.error,
    fontSize: 12,
  },
  icon: {
    fontSize: 24,
    marginRight: 5,
  },
  placeholder: {
    color: theme.colors.placeholder,
    fontSize: theme.fonts.inputFontSize,
  },
  buttonsText: {
    color: theme.colors.primary,
    fontSize: theme.fonts.buttonFontSize,
  },
}));
