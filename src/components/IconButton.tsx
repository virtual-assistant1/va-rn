import React, {FC} from 'react';
import {
  TouchableOpacity,
  TouchableNativeFeedback,
  TouchableOpacityProps,
  TouchableNativeFeedbackProps,
  Platform,
} from 'react-native';

import {Icon, IconProps} from './Icon';
import {View} from './View';
import {useTheme} from 'hooks';

export interface IconButtonProps
  extends TouchableOpacityProps,
    TouchableNativeFeedbackProps {
  icon: IconProps;
}

export const IconButton: FC<IconButtonProps> = ({icon, ...rest}) => {
  const theme = useTheme();
  return Platform.OS === 'android' ? (
    <TouchableNativeFeedback
      hitSlop={Platform.select({
        ios: undefined,
        default: {top: 16, right: 16, bottom: 16, left: 16},
      })}
      useForeground={TouchableNativeFeedback.canUseNativeForeground()}
      background={TouchableNativeFeedback.Ripple(
        theme.colors.androidRipple,
        true,
      )}
      {...rest}>
      <View style={rest.style}>
        <Icon {...icon} />
      </View>
    </TouchableNativeFeedback>
  ) : (
    <TouchableOpacity {...rest}>
      <Icon {...icon} />
    </TouchableOpacity>
  );
};
