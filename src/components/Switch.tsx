import React, {FC} from 'react';
import {Switch as RawSwitch, SwitchProps} from 'react-native';
import {useTheme} from 'hooks';

export const Switch: FC<SwitchProps> = props => {
  const {colors} = useTheme();
  return (
    <RawSwitch
      thumbColor={colors.background}
      trackColor={{true: colors.primary, false: colors.primaryDark}}
      {...props}
    />
  );
};
