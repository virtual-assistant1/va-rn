import styled from 'styled-components/native';
import {ComponentType} from 'react';
import {TextProps as RawTextProps} from 'react-native';

export interface TextProps {
  muted?: boolean;
}

export const Text = styled.Text<ComponentType<RawTextProps & TextProps>>`
  color: ${(props) => props.theme.colors.text};
`;
