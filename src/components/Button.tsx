import React, {FC, ComponentType} from 'react';
import {Platform, TextProps, StyleSheet} from 'react-native';
import {
  RectButton,
  TouchableOpacity,
  BaseButtonProperties,
} from 'react-native-gesture-handler';
import {SpinnerProps} from 'react-native-spinkit';
import styled from 'styled-components/native';
import {Icon, IconProps} from './Icon';
import {Text} from './Text';
import {Loader} from './Loader';
import {useTheme} from 'hooks';
import LinearGradient from 'react-native-linear-gradient';
import RadialGradient from 'react-native-radial-gradient';

interface ButtonTypes {
  transparent?: boolean;
  outline?: boolean;
}

const CustomButtonLoader = styled(Loader)`
  position: absolute;
  right: 5px;
`;

export const Gradient = () => {
  return (
    <LinearGradient
      style={{...StyleSheet.absoluteFillObject}}
      colors={['#19769F', '#35D8A6']}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
    />
  );
};

export const ButtonGradient = () => {
  return (
    <RadialGradient
      style={{...StyleSheet.absoluteFillObject}}
      colors={['#7BE495', '#40AB9B', '#35A09C', '#329D9C']}
      center={[0, 0]}
      radius={300}
    />
  );
};

const ButtonLoader: FC<SpinnerProps & ButtonTypes> = ({
  outline,
  transparent,
  ...rest
}) => {
  const theme = useTheme();
  return (
    <CustomButtonLoader
      color={outline || transparent ? theme.colors.primary : 'white'}
      {...rest}
    />
  );
};

const Title = styled<ComponentType<TextProps & ButtonTypes>>(Text)`
  color: ${({theme, transparent, outline}) =>
    transparent || outline ? theme.colors.primary : 'white'};
  text-transform: ${({transparent}) => (transparent ? 'none' : 'uppercase')};
`;

const ButtonIcon = styled<ComponentType<IconProps & ButtonTypes>>(Icon)`
  font-size: 20px;
  color: ${({theme, transparent, outline}) =>
    transparent || outline ? theme.colors.primary : 'white'};
`;

export interface ButtonProps extends BaseButtonProperties, ButtonTypes {
  title: string;
  titleProps?: TextProps;
  leftIcon?: IconProps;
  rightIcon?: IconProps;
  loading?: boolean;
  loaderProps?: SpinnerProps;
}

const CustomButton: FC<ButtonProps> = ({
  title,
  titleProps,
  leftIcon,
  rightIcon,
  loading,
  loaderProps,
  transparent,
  outline,
  ...rest
}) => {
  return Platform.OS === 'android' ? (
    <RectButton {...rest}>
      <ButtonGradient />
      {leftIcon && (
        <ButtonIcon outline={outline} transparent={transparent} {...leftIcon} />
      )}
      <Title outline={outline} transparent={transparent} {...titleProps}>
        {title}
      </Title>
      {rightIcon && (
        <ButtonIcon
          outline={outline}
          transparent={transparent}
          {...rightIcon}
        />
      )}
      {loading && (
        <ButtonLoader
          outline={outline}
          transparent={transparent}
          {...loaderProps}
        />
      )}
    </RectButton>
  ) : (
    <TouchableOpacity {...rest}>
      <ButtonGradient />
      {leftIcon && (
        <ButtonIcon outline={outline} transparent={transparent} {...leftIcon} />
      )}
      <Title outline={outline} transparent={transparent} {...titleProps}>
        {title}
      </Title>
      {rightIcon && (
        <ButtonIcon
          outline={outline}
          transparent={transparent}
          {...rightIcon}
        />
      )}
      {loading && (
        <ButtonLoader
          outline={outline}
          transparent={transparent}
          {...loaderProps}
        />
      )}
    </TouchableOpacity>
  );
};

export const Button = styled(CustomButton)`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  background-color: ${({theme, transparent, outline}) =>
    transparent || outline ? 'transparent' : theme.colors.primary};
  border-radius: 25px;
  border-color: ${({theme}) => theme.colors.primary};
  border-width: ${({outline}) => (outline ? '1px' : 0)};
  overflow: hidden;
`;
