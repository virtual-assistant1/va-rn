/* auto generated types */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {DocumentNode} from 'graphql';
import {TypedDocumentNode} from '@graphql-typed-document-node/core';
export type Maybe<T> = T | null;
export type Exact<T extends {[key: string]: any}> = {[K in keyof T]: T[K]};

/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** A date-time string at UTC, such as 2019-12-03T09:54:33Z, compliant with the date-time format. */
  DateTime: any;
};

export type Answer = {
  __typename?: 'Answer';
  id: Scalars['ID'];
  code: Scalars['Float'];
  question: Scalars['Float'];
  answer: Scalars['String'];
};

export type Appointment = {
  __typename?: 'Appointment';
  id: Scalars['ID'];
  doctorId: Scalars['String'];
  userId: Scalars['String'];
  date: Scalars['DateTime'];
  time: Scalars['String'];
  email: Scalars['String'];
  mobile: Scalars['String'];
  name: Scalars['String'];
  createdAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
};

export type Auth = {
  __typename?: 'Auth';
  user: User;
  accessToken: Scalars['String'];
  refreshToken: Scalars['String'];
};

export type CreateAppointmentDto = {
  doctorId: Scalars['String'];
  date: Scalars['DateTime'];
  time: Scalars['String'];
  name: Scalars['String'];
  email: Scalars['String'];
  mobile: Scalars['String'];
};

export type CreateDto = {
  email: Scalars['String'];
  password: Scalars['String'];
  name: Scalars['String'];
  gender?: Maybe<Scalars['String']>;
  dob?: Maybe<Scalars['DateTime']>;
  weight?: Maybe<Scalars['Float']>;
  height?: Maybe<Scalars['Float']>;
  userType: Scalars['String'];
};

export type Description = {
  __typename?: 'Description';
  arabic: Scalars['String'];
  english: Scalars['String'];
};

export type Disease = {
  __typename?: 'Disease';
  id: Scalars['ID'];
  code: Scalars['Float'];
  disease: Scalars['String'];
  symptoms: Array<Scalars['Float']>;
  rank: Scalars['Float'];
};

export type Doctor = {
  __typename?: 'Doctor';
  id: Scalars['ID'];
  code: Scalars['Float'];
  specialization: Scalars['String'];
  name: Scalars['String'];
  address: Scalars['String'];
  telephone: Scalars['String'];
  mobile: Scalars['String'];
  city: Scalars['String'];
  experience: Scalars['String'];
  openingTime: Scalars['String'];
  closingTime: Scalars['String'];
  createdAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
};

export type EvaluationAnswer = {
  __typename?: 'EvaluationAnswer';
  id: Scalars['ID'];
  code: Scalars['Float'];
  answer: Scalars['String'];
};

export type EvaluationDisease = {
  __typename?: 'EvaluationDisease';
  id: Scalars['ID'];
  code: Scalars['Float'];
  disease: Scalars['String'];
  rank: Scalars['Float'];
};

export type EvaluationQuestion = {
  __typename?: 'EvaluationQuestion';
  id: Scalars['ID'];
  code: Scalars['Float'];
  question: Scalars['String'];
  answers: Array<EvaluationAnswer>;
};

export type Mutation = {
  __typename?: 'Mutation';
  createUser: User;
  updateUser: User;
  saveEvaluation: Prediction;
  registerValidation: Prediction;
  login: Auth;
  refreshTokens: Auth;
  createAppointment: Appointment;
};

export type MutationCreateUserArgs = {
  user: CreateDto;
};

export type MutationUpdateUserArgs = {
  user: UpdateDto;
};

export type MutationSaveEvaluationArgs = {
  questions: Array<QaDto>;
  userProps: SaveUserPropsDto;
  diseases: Array<SaveDiseasesDto>;
  risk: Scalars['Boolean'];
};

export type MutationRegisterValidationArgs = {
  validation: RegisterValidation;
};

export type MutationLoginArgs = {
  email: Scalars['String'];
  password: Scalars['String'];
};

export type MutationRefreshTokensArgs = {
  accessToken: Scalars['String'];
  refreshToken: Scalars['String'];
};

export type MutationCreateAppointmentArgs = {
  appointment: CreateAppointmentDto;
};

export type Prediction = {
  __typename?: 'Prediction';
  id: Scalars['ID'];
  user: UserProps;
  questions: Array<EvaluationQuestion>;
  diseases: Array<EvaluationDisease>;
  risk: Scalars['Boolean'];
  validated?: Maybe<Scalars['Boolean']>;
  validationResult?: Maybe<Scalars['Boolean']>;
  createdAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
};

export type PredictResults = {
  __typename?: 'PredictResults';
  error: Scalars['Boolean'];
  message: Scalars['String'];
};

export type QaDto = {
  question: Scalars['Float'];
  answers: Array<Scalars['Float']>;
};

export type Query = {
  __typename?: 'Query';
  users: Array<User>;
  me: User;
  predict: PredictResults;
  answers: Array<Answer>;
  questions: Array<Question>;
  firstQuestions: Array<Question>;
  questionCount: Scalars['Int'];
  evaluateAnswers: Array<Disease>;
  userEvaluations: Array<Prediction>;
  diseaseTreatments: Array<Treatment>;
  doctors: Array<Doctor>;
  appointments: Array<Appointment>;
};

export type QueryPredictArgs = {
  systolic: Scalars['Float'];
  diastolic: Scalars['Float'];
  age_year: Scalars['Float'];
  cholesterol: Scalars['Float'];
  weight: Scalars['Float'];
  height: Scalars['Float'];
  gender: Scalars['Float'];
  alcohol: Scalars['Float'];
  smoke: Scalars['Float'];
  glucose: Scalars['Float'];
  active: Scalars['Float'];
};

export type QueryAnswersArgs = {
  question: Scalars['Float'];
};

export type QueryQuestionsArgs = {
  answer: Scalars['Float'];
};

export type QueryQuestionCountArgs = {
  questionType: Scalars['String'];
  parentQuestion: Scalars['Float'];
};

export type QueryEvaluateAnswersArgs = {
  questions: Array<QaDto>;
};

export type QueryDiseaseTreatmentsArgs = {
  diseaseCode: Scalars['Float'];
};

export type Question = {
  __typename?: 'Question';
  id: Scalars['ID'];
  code: Scalars['Float'];
  /** the name of question */
  question: Scalars['String'];
  description: Description;
  parentQuestion: Scalars['Float'];
  questionType: Scalars['String'];
  url: Scalars['String'];
  answers: Array<Answer>;
};

export type RegisterValidation = {
  id: Scalars['String'];
  validationResults: Scalars['Boolean'];
};

export type SaveDiseasesDto = {
  code: Scalars['Float'];
  disease: Scalars['String'];
  rank: Scalars['Float'];
};

export type SaveUserPropsDto = {
  userId: Scalars['String'];
  gender: Scalars['String'];
  age_year: Scalars['Float'];
  weight: Scalars['Float'];
  height: Scalars['Float'];
  alcohol: Scalars['Float'];
  smoke: Scalars['Float'];
  glucose: Scalars['Float'];
  active: Scalars['Float'];
  cholesterol: Scalars['Float'];
  systolic: Scalars['Float'];
  diastolic: Scalars['Float'];
};

export type Treatment = {
  __typename?: 'Treatment';
  id: Scalars['ID'];
  title: Scalars['String'];
  summary: Scalars['String'];
  url: Scalars['String'];
  diseaseCode: Scalars['Float'];
  createdAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
};

export type UpdateDto = {
  name?: Maybe<Scalars['String']>;
  gender?: Maybe<Scalars['String']>;
  dob?: Maybe<Scalars['DateTime']>;
  weight?: Maybe<Scalars['Float']>;
  height?: Maybe<Scalars['Float']>;
  userType?: Maybe<Scalars['String']>;
};

export type User = {
  __typename?: 'User';
  id: Scalars['ID'];
  email: Scalars['String'];
  name: Scalars['String'];
  gender: Scalars['String'];
  dob: Scalars['DateTime'];
  weight: Scalars['Float'];
  height: Scalars['Float'];
  userType: Scalars['String'];
  createdAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
};

export type UserProps = {
  __typename?: 'UserProps';
  userId: Scalars['String'];
  gender: Scalars['String'];
  age_year: Scalars['Float'];
  weight: Scalars['Float'];
  height: Scalars['Float'];
  alcohol: Scalars['Float'];
  smoke: Scalars['Float'];
  glucose: Scalars['Float'];
  active: Scalars['Float'];
  cholesterol: Scalars['Float'];
  systolic: Scalars['Float'];
  diastolic: Scalars['Float'];
};

export type QuestionCountQueryVariables = Exact<{
  questionType: Scalars['String'];
  parentQuestion: Scalars['Float'];
}>;

export type QuestionCountQuery = {__typename?: 'Query'} & Pick<
  Query,
  'questionCount'
>;

export type PredictQueryVariables = Exact<{
  systolic: Scalars['Float'];
  diastolic: Scalars['Float'];
  age_year: Scalars['Float'];
  cholesterol: Scalars['Float'];
  weight: Scalars['Float'];
  height: Scalars['Float'];
  gender: Scalars['Float'];
  alcohol: Scalars['Float'];
  smoke: Scalars['Float'];
  glucose: Scalars['Float'];
  active: Scalars['Float'];
}>;

export type PredictQuery = {__typename?: 'Query'} & {
  predict: {__typename?: 'PredictResults'} & Pick<
    PredictResults,
    'error' | 'message'
  >;
};

export type GetFirstQuestionQueryVariables = Exact<{[key: string]: never}>;

export type GetFirstQuestionQuery = {__typename?: 'Query'} & {
  firstQuestions: Array<
    {__typename?: 'Question'} & Pick<
      Question,
      'id' | 'code' | 'question' | 'url' | 'parentQuestion' | 'questionType'
    > & {
        description: {__typename?: 'Description'} & Pick<
          Description,
          'arabic' | 'english'
        >;
        answers: Array<
          {__typename?: 'Answer'} & Pick<Answer, 'id' | 'code' | 'answer'>
        >;
      }
  >;
};

export type GetQuestionsQueryVariables = Exact<{
  answer: Scalars['Float'];
}>;

export type GetQuestionsQuery = {__typename?: 'Query'} & {
  questions: Array<
    {__typename?: 'Question'} & Pick<
      Question,
      'id' | 'code' | 'question' | 'url' | 'parentQuestion' | 'questionType'
    > & {
        description: {__typename?: 'Description'} & Pick<
          Description,
          'arabic' | 'english'
        >;
        answers: Array<
          {__typename?: 'Answer'} & Pick<Answer, 'id' | 'code' | 'answer'>
        >;
      }
  >;
};

export type EvaluateQueryVariables = Exact<{
  questions: Array<QaDto>;
}>;

export type EvaluateQuery = {__typename?: 'Query'} & {
  evaluateAnswers: Array<
    {__typename?: 'Disease'} & Pick<Disease, 'id' | 'code' | 'disease' | 'rank'>
  >;
};

export type SavePredictionMutationVariables = Exact<{
  questions: Array<QaDto>;
  userProps: SaveUserPropsDto;
  diseases: Array<SaveDiseasesDto>;
  risk: Scalars['Boolean'];
}>;

export type SavePredictionMutation = {__typename?: 'Mutation'} & {
  saveEvaluation: {__typename?: 'Prediction'} & Pick<
    Prediction,
    'id' | 'risk' | 'validated' | 'validationResult'
  > & {
      user: {__typename?: 'UserProps'} & Pick<UserProps, 'userId'>;
      questions: Array<
        {__typename?: 'EvaluationQuestion'} & Pick<
          EvaluationQuestion,
          'id' | 'code' | 'question'
        >
      >;
      diseases: Array<
        {__typename?: 'EvaluationDisease'} & Pick<
          EvaluationDisease,
          'id' | 'code' | 'disease'
        >
      >;
    };
};

export type RegisterValidationMutationVariables = Exact<{
  validation: RegisterValidation;
}>;

export type RegisterValidationMutation = {__typename?: 'Mutation'} & {
  registerValidation: {__typename?: 'Prediction'} & Pick<
    Prediction,
    'id' | 'risk' | 'validated' | 'validationResult'
  > & {
      user: {__typename?: 'UserProps'} & Pick<UserProps, 'userId'>;
      questions: Array<
        {__typename?: 'EvaluationQuestion'} & Pick<
          EvaluationQuestion,
          'id' | 'code' | 'question'
        >
      >;
      diseases: Array<
        {__typename?: 'EvaluationDisease'} & Pick<
          EvaluationDisease,
          'id' | 'code' | 'disease'
        >
      >;
    };
};

export type UserEvaluationQueryVariables = Exact<{[key: string]: never}>;

export type UserEvaluationQuery = {__typename?: 'Query'} & {
  userEvaluations: Array<
    {__typename?: 'Prediction'} & Pick<
      Prediction,
      'id' | 'risk' | 'validated' | 'validationResult' | 'createdAt'
    > & {
        user: {__typename?: 'UserProps'} & Pick<
          UserProps,
          | 'userId'
          | 'gender'
          | 'age_year'
          | 'weight'
          | 'height'
          | 'alcohol'
          | 'smoke'
          | 'glucose'
          | 'active'
          | 'cholesterol'
          | 'systolic'
          | 'diastolic'
        >;
        diseases: Array<
          {__typename?: 'EvaluationDisease'} & Pick<
            EvaluationDisease,
            'id' | 'code' | 'disease' | 'rank'
          >
        >;
        questions: Array<
          {__typename?: 'EvaluationQuestion'} & Pick<
            EvaluationQuestion,
            'id' | 'code' | 'question'
          > & {
              answers: Array<
                {__typename?: 'EvaluationAnswer'} & Pick<
                  EvaluationAnswer,
                  'id' | 'code' | 'answer'
                >
              >;
            }
        >;
      }
  >;
};

export type DoctorsQueryVariables = Exact<{[key: string]: never}>;

export type DoctorsQuery = {__typename?: 'Query'} & {
  doctors: Array<
    {__typename?: 'Doctor'} & Pick<
      Doctor,
      | 'id'
      | 'code'
      | 'specialization'
      | 'name'
      | 'address'
      | 'telephone'
      | 'mobile'
      | 'city'
      | 'experience'
      | 'openingTime'
      | 'closingTime'
      | 'createdAt'
      | 'updatedAt'
    >
  >;
};

export type CreateAppointmentMutationVariables = Exact<{
  appointment: CreateAppointmentDto;
}>;

export type CreateAppointmentMutation = {__typename?: 'Mutation'} & {
  createAppointment: {__typename?: 'Appointment'} & Pick<
    Appointment,
    | 'id'
    | 'doctorId'
    | 'userId'
    | 'date'
    | 'time'
    | 'email'
    | 'mobile'
    | 'name'
    | 'createdAt'
    | 'updatedAt'
  >;
};

export type UserAppointmentsQueryVariables = Exact<{[key: string]: never}>;

export type UserAppointmentsQuery = {__typename?: 'Query'} & {
  appointments: Array<
    {__typename?: 'Appointment'} & Pick<
      Appointment,
      | 'id'
      | 'doctorId'
      | 'userId'
      | 'date'
      | 'time'
      | 'email'
      | 'mobile'
      | 'name'
      | 'createdAt'
      | 'updatedAt'
    >
  >;
};

export type DiseaseTreatmentsQueryVariables = Exact<{
  diseaseCode: Scalars['Float'];
}>;

export type DiseaseTreatmentsQuery = {__typename?: 'Query'} & {
  diseaseTreatments: Array<
    {__typename?: 'Treatment'} & Pick<
      Treatment,
      | 'id'
      | 'title'
      | 'summary'
      | 'url'
      | 'diseaseCode'
      | 'createdAt'
      | 'updatedAt'
    >
  >;
};

export type CreateUserMutationVariables = Exact<{
  user: CreateDto;
}>;

export type CreateUserMutation = {__typename?: 'Mutation'} & {
  createUser: {__typename?: 'User'} & Pick<
    User,
    | 'id'
    | 'email'
    | 'name'
    | 'gender'
    | 'dob'
    | 'height'
    | 'weight'
    | 'createdAt'
    | 'updatedAt'
  >;
};

export type LoginMutationVariables = Exact<{
  email: Scalars['String'];
  password: Scalars['String'];
}>;

export type LoginMutation = {__typename?: 'Mutation'} & {
  login: {__typename?: 'Auth'} & Pick<Auth, 'accessToken' | 'refreshToken'> & {
      user: {__typename?: 'User'} & Pick<
        User,
        'id' | 'email' | 'name' | 'createdAt' | 'updatedAt'
      >;
    };
};

export type MeQueryVariables = Exact<{[key: string]: never}>;

export type MeQuery = {__typename?: 'Query'} & {
  me: {__typename?: 'User'} & Pick<
    User,
    | 'id'
    | 'email'
    | 'name'
    | 'gender'
    | 'weight'
    | 'height'
    | 'dob'
    | 'userType'
    | 'createdAt'
    | 'updatedAt'
  >;
};

export type UpdateUserMutationVariables = Exact<{
  user: UpdateDto;
}>;

export type UpdateUserMutation = {__typename?: 'Mutation'} & {
  updateUser: {__typename?: 'User'} & Pick<
    User,
    | 'id'
    | 'email'
    | 'name'
    | 'gender'
    | 'dob'
    | 'weight'
    | 'height'
    | 'userType'
    | 'createdAt'
    | 'updatedAt'
  >;
};

export const QuestionCountDocument: TypedDocumentNode<
  QuestionCountQuery,
  QuestionCountQueryVariables
> = {
  kind: 'Document',
  definitions: [
    {
      kind: 'OperationDefinition',
      operation: 'query',
      name: {kind: 'Name', value: 'questionCount'},
      variableDefinitions: [
        {
          kind: 'VariableDefinition',
          variable: {
            kind: 'Variable',
            name: {kind: 'Name', value: 'questionType'},
          },
          type: {
            kind: 'NonNullType',
            type: {kind: 'NamedType', name: {kind: 'Name', value: 'String'}},
          },
          directives: [],
        },
        {
          kind: 'VariableDefinition',
          variable: {
            kind: 'Variable',
            name: {kind: 'Name', value: 'parentQuestion'},
          },
          type: {
            kind: 'NonNullType',
            type: {kind: 'NamedType', name: {kind: 'Name', value: 'Float'}},
          },
          directives: [],
        },
      ],
      directives: [],
      selectionSet: {
        kind: 'SelectionSet',
        selections: [
          {
            kind: 'Field',
            name: {kind: 'Name', value: 'questionCount'},
            arguments: [
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'questionType'},
                value: {
                  kind: 'Variable',
                  name: {kind: 'Name', value: 'questionType'},
                },
              },
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'parentQuestion'},
                value: {
                  kind: 'Variable',
                  name: {kind: 'Name', value: 'parentQuestion'},
                },
              },
            ],
            directives: [],
          },
        ],
      },
    },
  ],
};
export const PredictDocument: TypedDocumentNode<
  PredictQuery,
  PredictQueryVariables
> = {
  kind: 'Document',
  definitions: [
    {
      kind: 'OperationDefinition',
      operation: 'query',
      name: {kind: 'Name', value: 'predict'},
      variableDefinitions: [
        {
          kind: 'VariableDefinition',
          variable: {kind: 'Variable', name: {kind: 'Name', value: 'systolic'}},
          type: {
            kind: 'NonNullType',
            type: {kind: 'NamedType', name: {kind: 'Name', value: 'Float'}},
          },
          directives: [],
        },
        {
          kind: 'VariableDefinition',
          variable: {
            kind: 'Variable',
            name: {kind: 'Name', value: 'diastolic'},
          },
          type: {
            kind: 'NonNullType',
            type: {kind: 'NamedType', name: {kind: 'Name', value: 'Float'}},
          },
          directives: [],
        },
        {
          kind: 'VariableDefinition',
          variable: {kind: 'Variable', name: {kind: 'Name', value: 'age_year'}},
          type: {
            kind: 'NonNullType',
            type: {kind: 'NamedType', name: {kind: 'Name', value: 'Float'}},
          },
          directives: [],
        },
        {
          kind: 'VariableDefinition',
          variable: {
            kind: 'Variable',
            name: {kind: 'Name', value: 'cholesterol'},
          },
          type: {
            kind: 'NonNullType',
            type: {kind: 'NamedType', name: {kind: 'Name', value: 'Float'}},
          },
          directives: [],
        },
        {
          kind: 'VariableDefinition',
          variable: {kind: 'Variable', name: {kind: 'Name', value: 'weight'}},
          type: {
            kind: 'NonNullType',
            type: {kind: 'NamedType', name: {kind: 'Name', value: 'Float'}},
          },
          directives: [],
        },
        {
          kind: 'VariableDefinition',
          variable: {kind: 'Variable', name: {kind: 'Name', value: 'height'}},
          type: {
            kind: 'NonNullType',
            type: {kind: 'NamedType', name: {kind: 'Name', value: 'Float'}},
          },
          directives: [],
        },
        {
          kind: 'VariableDefinition',
          variable: {kind: 'Variable', name: {kind: 'Name', value: 'gender'}},
          type: {
            kind: 'NonNullType',
            type: {kind: 'NamedType', name: {kind: 'Name', value: 'Float'}},
          },
          directives: [],
        },
        {
          kind: 'VariableDefinition',
          variable: {kind: 'Variable', name: {kind: 'Name', value: 'alcohol'}},
          type: {
            kind: 'NonNullType',
            type: {kind: 'NamedType', name: {kind: 'Name', value: 'Float'}},
          },
          directives: [],
        },
        {
          kind: 'VariableDefinition',
          variable: {kind: 'Variable', name: {kind: 'Name', value: 'smoke'}},
          type: {
            kind: 'NonNullType',
            type: {kind: 'NamedType', name: {kind: 'Name', value: 'Float'}},
          },
          directives: [],
        },
        {
          kind: 'VariableDefinition',
          variable: {kind: 'Variable', name: {kind: 'Name', value: 'glucose'}},
          type: {
            kind: 'NonNullType',
            type: {kind: 'NamedType', name: {kind: 'Name', value: 'Float'}},
          },
          directives: [],
        },
        {
          kind: 'VariableDefinition',
          variable: {kind: 'Variable', name: {kind: 'Name', value: 'active'}},
          type: {
            kind: 'NonNullType',
            type: {kind: 'NamedType', name: {kind: 'Name', value: 'Float'}},
          },
          directives: [],
        },
      ],
      directives: [],
      selectionSet: {
        kind: 'SelectionSet',
        selections: [
          {
            kind: 'Field',
            name: {kind: 'Name', value: 'predict'},
            arguments: [
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'systolic'},
                value: {
                  kind: 'Variable',
                  name: {kind: 'Name', value: 'systolic'},
                },
              },
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'diastolic'},
                value: {
                  kind: 'Variable',
                  name: {kind: 'Name', value: 'diastolic'},
                },
              },
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'age_year'},
                value: {
                  kind: 'Variable',
                  name: {kind: 'Name', value: 'age_year'},
                },
              },
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'cholesterol'},
                value: {
                  kind: 'Variable',
                  name: {kind: 'Name', value: 'cholesterol'},
                },
              },
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'weight'},
                value: {
                  kind: 'Variable',
                  name: {kind: 'Name', value: 'weight'},
                },
              },
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'height'},
                value: {
                  kind: 'Variable',
                  name: {kind: 'Name', value: 'height'},
                },
              },
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'gender'},
                value: {
                  kind: 'Variable',
                  name: {kind: 'Name', value: 'gender'},
                },
              },
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'alcohol'},
                value: {
                  kind: 'Variable',
                  name: {kind: 'Name', value: 'alcohol'},
                },
              },
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'smoke'},
                value: {kind: 'Variable', name: {kind: 'Name', value: 'smoke'}},
              },
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'glucose'},
                value: {
                  kind: 'Variable',
                  name: {kind: 'Name', value: 'glucose'},
                },
              },
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'active'},
                value: {
                  kind: 'Variable',
                  name: {kind: 'Name', value: 'active'},
                },
              },
            ],
            directives: [],
            selectionSet: {
              kind: 'SelectionSet',
              selections: [
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'error'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'message'},
                  arguments: [],
                  directives: [],
                },
              ],
            },
          },
        ],
      },
    },
  ],
};
export const GetFirstQuestionDocument: TypedDocumentNode<
  GetFirstQuestionQuery,
  GetFirstQuestionQueryVariables
> = {
  kind: 'Document',
  definitions: [
    {
      kind: 'OperationDefinition',
      operation: 'query',
      name: {kind: 'Name', value: 'getFirstQuestion'},
      variableDefinitions: [],
      directives: [],
      selectionSet: {
        kind: 'SelectionSet',
        selections: [
          {
            kind: 'Field',
            name: {kind: 'Name', value: 'firstQuestions'},
            arguments: [],
            directives: [],
            selectionSet: {
              kind: 'SelectionSet',
              selections: [
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'id'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'code'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'question'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'url'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'parentQuestion'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'questionType'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'description'},
                  arguments: [],
                  directives: [],
                  selectionSet: {
                    kind: 'SelectionSet',
                    selections: [
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'arabic'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'english'},
                        arguments: [],
                        directives: [],
                      },
                    ],
                  },
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'answers'},
                  arguments: [],
                  directives: [],
                  selectionSet: {
                    kind: 'SelectionSet',
                    selections: [
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'id'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'code'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'answer'},
                        arguments: [],
                        directives: [],
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
};
export const GetQuestionsDocument: TypedDocumentNode<
  GetQuestionsQuery,
  GetQuestionsQueryVariables
> = {
  kind: 'Document',
  definitions: [
    {
      kind: 'OperationDefinition',
      operation: 'query',
      name: {kind: 'Name', value: 'getQuestions'},
      variableDefinitions: [
        {
          kind: 'VariableDefinition',
          variable: {kind: 'Variable', name: {kind: 'Name', value: 'answer'}},
          type: {
            kind: 'NonNullType',
            type: {kind: 'NamedType', name: {kind: 'Name', value: 'Float'}},
          },
          directives: [],
        },
      ],
      directives: [],
      selectionSet: {
        kind: 'SelectionSet',
        selections: [
          {
            kind: 'Field',
            name: {kind: 'Name', value: 'questions'},
            arguments: [
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'answer'},
                value: {
                  kind: 'Variable',
                  name: {kind: 'Name', value: 'answer'},
                },
              },
            ],
            directives: [],
            selectionSet: {
              kind: 'SelectionSet',
              selections: [
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'id'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'code'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'question'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'url'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'parentQuestion'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'questionType'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'description'},
                  arguments: [],
                  directives: [],
                  selectionSet: {
                    kind: 'SelectionSet',
                    selections: [
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'arabic'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'english'},
                        arguments: [],
                        directives: [],
                      },
                    ],
                  },
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'answers'},
                  arguments: [],
                  directives: [],
                  selectionSet: {
                    kind: 'SelectionSet',
                    selections: [
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'id'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'code'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'answer'},
                        arguments: [],
                        directives: [],
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
};
export const EvaluateDocument: TypedDocumentNode<
  EvaluateQuery,
  EvaluateQueryVariables
> = {
  kind: 'Document',
  definitions: [
    {
      kind: 'OperationDefinition',
      operation: 'query',
      name: {kind: 'Name', value: 'evaluate'},
      variableDefinitions: [
        {
          kind: 'VariableDefinition',
          variable: {
            kind: 'Variable',
            name: {kind: 'Name', value: 'questions'},
          },
          type: {
            kind: 'NonNullType',
            type: {
              kind: 'ListType',
              type: {
                kind: 'NonNullType',
                type: {kind: 'NamedType', name: {kind: 'Name', value: 'QADto'}},
              },
            },
          },
          directives: [],
        },
      ],
      directives: [],
      selectionSet: {
        kind: 'SelectionSet',
        selections: [
          {
            kind: 'Field',
            name: {kind: 'Name', value: 'evaluateAnswers'},
            arguments: [
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'questions'},
                value: {
                  kind: 'Variable',
                  name: {kind: 'Name', value: 'questions'},
                },
              },
            ],
            directives: [],
            selectionSet: {
              kind: 'SelectionSet',
              selections: [
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'id'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'code'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'disease'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'rank'},
                  arguments: [],
                  directives: [],
                },
              ],
            },
          },
        ],
      },
    },
  ],
};
export const SavePredictionDocument: TypedDocumentNode<
  SavePredictionMutation,
  SavePredictionMutationVariables
> = {
  kind: 'Document',
  definitions: [
    {
      kind: 'OperationDefinition',
      operation: 'mutation',
      name: {kind: 'Name', value: 'savePrediction'},
      variableDefinitions: [
        {
          kind: 'VariableDefinition',
          variable: {
            kind: 'Variable',
            name: {kind: 'Name', value: 'questions'},
          },
          type: {
            kind: 'NonNullType',
            type: {
              kind: 'ListType',
              type: {
                kind: 'NonNullType',
                type: {kind: 'NamedType', name: {kind: 'Name', value: 'QADto'}},
              },
            },
          },
          directives: [],
        },
        {
          kind: 'VariableDefinition',
          variable: {
            kind: 'Variable',
            name: {kind: 'Name', value: 'userProps'},
          },
          type: {
            kind: 'NonNullType',
            type: {
              kind: 'NamedType',
              name: {kind: 'Name', value: 'SaveUserPropsDto'},
            },
          },
          directives: [],
        },
        {
          kind: 'VariableDefinition',
          variable: {kind: 'Variable', name: {kind: 'Name', value: 'diseases'}},
          type: {
            kind: 'NonNullType',
            type: {
              kind: 'ListType',
              type: {
                kind: 'NonNullType',
                type: {
                  kind: 'NamedType',
                  name: {kind: 'Name', value: 'SaveDiseasesDto'},
                },
              },
            },
          },
          directives: [],
        },
        {
          kind: 'VariableDefinition',
          variable: {kind: 'Variable', name: {kind: 'Name', value: 'risk'}},
          type: {
            kind: 'NonNullType',
            type: {kind: 'NamedType', name: {kind: 'Name', value: 'Boolean'}},
          },
          directives: [],
        },
      ],
      directives: [],
      selectionSet: {
        kind: 'SelectionSet',
        selections: [
          {
            kind: 'Field',
            name: {kind: 'Name', value: 'saveEvaluation'},
            arguments: [
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'questions'},
                value: {
                  kind: 'Variable',
                  name: {kind: 'Name', value: 'questions'},
                },
              },
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'userProps'},
                value: {
                  kind: 'Variable',
                  name: {kind: 'Name', value: 'userProps'},
                },
              },
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'diseases'},
                value: {
                  kind: 'Variable',
                  name: {kind: 'Name', value: 'diseases'},
                },
              },
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'risk'},
                value: {kind: 'Variable', name: {kind: 'Name', value: 'risk'}},
              },
            ],
            directives: [],
            selectionSet: {
              kind: 'SelectionSet',
              selections: [
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'id'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'user'},
                  arguments: [],
                  directives: [],
                  selectionSet: {
                    kind: 'SelectionSet',
                    selections: [
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'userId'},
                        arguments: [],
                        directives: [],
                      },
                    ],
                  },
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'questions'},
                  arguments: [],
                  directives: [],
                  selectionSet: {
                    kind: 'SelectionSet',
                    selections: [
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'id'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'code'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'question'},
                        arguments: [],
                        directives: [],
                      },
                    ],
                  },
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'diseases'},
                  arguments: [],
                  directives: [],
                  selectionSet: {
                    kind: 'SelectionSet',
                    selections: [
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'id'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'code'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'disease'},
                        arguments: [],
                        directives: [],
                      },
                    ],
                  },
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'risk'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'validated'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'validationResult'},
                  arguments: [],
                  directives: [],
                },
              ],
            },
          },
        ],
      },
    },
  ],
};
export const RegisterValidationDocument: TypedDocumentNode<
  RegisterValidationMutation,
  RegisterValidationMutationVariables
> = {
  kind: 'Document',
  definitions: [
    {
      kind: 'OperationDefinition',
      operation: 'mutation',
      name: {kind: 'Name', value: 'registerValidation'},
      variableDefinitions: [
        {
          kind: 'VariableDefinition',
          variable: {
            kind: 'Variable',
            name: {kind: 'Name', value: 'validation'},
          },
          type: {
            kind: 'NonNullType',
            type: {
              kind: 'NamedType',
              name: {kind: 'Name', value: 'RegisterValidation'},
            },
          },
          directives: [],
        },
      ],
      directives: [],
      selectionSet: {
        kind: 'SelectionSet',
        selections: [
          {
            kind: 'Field',
            name: {kind: 'Name', value: 'registerValidation'},
            arguments: [
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'validation'},
                value: {
                  kind: 'Variable',
                  name: {kind: 'Name', value: 'validation'},
                },
              },
            ],
            directives: [],
            selectionSet: {
              kind: 'SelectionSet',
              selections: [
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'id'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'user'},
                  arguments: [],
                  directives: [],
                  selectionSet: {
                    kind: 'SelectionSet',
                    selections: [
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'userId'},
                        arguments: [],
                        directives: [],
                      },
                    ],
                  },
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'questions'},
                  arguments: [],
                  directives: [],
                  selectionSet: {
                    kind: 'SelectionSet',
                    selections: [
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'id'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'code'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'question'},
                        arguments: [],
                        directives: [],
                      },
                    ],
                  },
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'diseases'},
                  arguments: [],
                  directives: [],
                  selectionSet: {
                    kind: 'SelectionSet',
                    selections: [
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'id'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'code'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'disease'},
                        arguments: [],
                        directives: [],
                      },
                    ],
                  },
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'risk'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'validated'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'validationResult'},
                  arguments: [],
                  directives: [],
                },
              ],
            },
          },
        ],
      },
    },
  ],
};
export const UserEvaluationDocument: TypedDocumentNode<
  UserEvaluationQuery,
  UserEvaluationQueryVariables
> = {
  kind: 'Document',
  definitions: [
    {
      kind: 'OperationDefinition',
      operation: 'query',
      name: {kind: 'Name', value: 'userEvaluation'},
      variableDefinitions: [],
      directives: [],
      selectionSet: {
        kind: 'SelectionSet',
        selections: [
          {
            kind: 'Field',
            name: {kind: 'Name', value: 'userEvaluations'},
            arguments: [],
            directives: [],
            selectionSet: {
              kind: 'SelectionSet',
              selections: [
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'id'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'risk'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'validated'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'validationResult'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'user'},
                  arguments: [],
                  directives: [],
                  selectionSet: {
                    kind: 'SelectionSet',
                    selections: [
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'userId'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'gender'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'age_year'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'weight'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'height'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'alcohol'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'smoke'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'glucose'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'active'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'cholesterol'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'systolic'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'diastolic'},
                        arguments: [],
                        directives: [],
                      },
                    ],
                  },
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'createdAt'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'diseases'},
                  arguments: [],
                  directives: [],
                  selectionSet: {
                    kind: 'SelectionSet',
                    selections: [
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'id'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'code'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'disease'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'rank'},
                        arguments: [],
                        directives: [],
                      },
                    ],
                  },
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'questions'},
                  arguments: [],
                  directives: [],
                  selectionSet: {
                    kind: 'SelectionSet',
                    selections: [
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'id'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'code'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'question'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'answers'},
                        arguments: [],
                        directives: [],
                        selectionSet: {
                          kind: 'SelectionSet',
                          selections: [
                            {
                              kind: 'Field',
                              name: {kind: 'Name', value: 'id'},
                              arguments: [],
                              directives: [],
                            },
                            {
                              kind: 'Field',
                              name: {kind: 'Name', value: 'code'},
                              arguments: [],
                              directives: [],
                            },
                            {
                              kind: 'Field',
                              name: {kind: 'Name', value: 'answer'},
                              arguments: [],
                              directives: [],
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
};
export const DoctorsDocument: TypedDocumentNode<
  DoctorsQuery,
  DoctorsQueryVariables
> = {
  kind: 'Document',
  definitions: [
    {
      kind: 'OperationDefinition',
      operation: 'query',
      name: {kind: 'Name', value: 'doctors'},
      variableDefinitions: [],
      directives: [],
      selectionSet: {
        kind: 'SelectionSet',
        selections: [
          {
            kind: 'Field',
            name: {kind: 'Name', value: 'doctors'},
            arguments: [],
            directives: [],
            selectionSet: {
              kind: 'SelectionSet',
              selections: [
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'id'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'code'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'specialization'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'name'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'address'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'telephone'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'mobile'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'city'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'experience'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'openingTime'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'closingTime'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'createdAt'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'updatedAt'},
                  arguments: [],
                  directives: [],
                },
              ],
            },
          },
        ],
      },
    },
  ],
};
export const CreateAppointmentDocument: TypedDocumentNode<
  CreateAppointmentMutation,
  CreateAppointmentMutationVariables
> = {
  kind: 'Document',
  definitions: [
    {
      kind: 'OperationDefinition',
      operation: 'mutation',
      name: {kind: 'Name', value: 'createAppointment'},
      variableDefinitions: [
        {
          kind: 'VariableDefinition',
          variable: {
            kind: 'Variable',
            name: {kind: 'Name', value: 'appointment'},
          },
          type: {
            kind: 'NonNullType',
            type: {
              kind: 'NamedType',
              name: {kind: 'Name', value: 'CreateAppointmentDto'},
            },
          },
          directives: [],
        },
      ],
      directives: [],
      selectionSet: {
        kind: 'SelectionSet',
        selections: [
          {
            kind: 'Field',
            name: {kind: 'Name', value: 'createAppointment'},
            arguments: [
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'appointment'},
                value: {
                  kind: 'Variable',
                  name: {kind: 'Name', value: 'appointment'},
                },
              },
            ],
            directives: [],
            selectionSet: {
              kind: 'SelectionSet',
              selections: [
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'id'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'doctorId'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'userId'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'date'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'time'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'email'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'mobile'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'name'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'createdAt'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'updatedAt'},
                  arguments: [],
                  directives: [],
                },
              ],
            },
          },
        ],
      },
    },
  ],
};
export const UserAppointmentsDocument: TypedDocumentNode<
  UserAppointmentsQuery,
  UserAppointmentsQueryVariables
> = {
  kind: 'Document',
  definitions: [
    {
      kind: 'OperationDefinition',
      operation: 'query',
      name: {kind: 'Name', value: 'userAppointments'},
      variableDefinitions: [],
      directives: [],
      selectionSet: {
        kind: 'SelectionSet',
        selections: [
          {
            kind: 'Field',
            name: {kind: 'Name', value: 'appointments'},
            arguments: [],
            directives: [],
            selectionSet: {
              kind: 'SelectionSet',
              selections: [
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'id'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'doctorId'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'userId'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'date'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'time'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'email'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'mobile'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'name'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'createdAt'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'updatedAt'},
                  arguments: [],
                  directives: [],
                },
              ],
            },
          },
        ],
      },
    },
  ],
};
export const DiseaseTreatmentsDocument: TypedDocumentNode<
  DiseaseTreatmentsQuery,
  DiseaseTreatmentsQueryVariables
> = {
  kind: 'Document',
  definitions: [
    {
      kind: 'OperationDefinition',
      operation: 'query',
      name: {kind: 'Name', value: 'diseaseTreatments'},
      variableDefinitions: [
        {
          kind: 'VariableDefinition',
          variable: {
            kind: 'Variable',
            name: {kind: 'Name', value: 'diseaseCode'},
          },
          type: {
            kind: 'NonNullType',
            type: {kind: 'NamedType', name: {kind: 'Name', value: 'Float'}},
          },
          directives: [],
        },
      ],
      directives: [],
      selectionSet: {
        kind: 'SelectionSet',
        selections: [
          {
            kind: 'Field',
            name: {kind: 'Name', value: 'diseaseTreatments'},
            arguments: [
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'diseaseCode'},
                value: {
                  kind: 'Variable',
                  name: {kind: 'Name', value: 'diseaseCode'},
                },
              },
            ],
            directives: [],
            selectionSet: {
              kind: 'SelectionSet',
              selections: [
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'id'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'title'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'summary'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'url'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'diseaseCode'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'createdAt'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'updatedAt'},
                  arguments: [],
                  directives: [],
                },
              ],
            },
          },
        ],
      },
    },
  ],
};
export const CreateUserDocument: TypedDocumentNode<
  CreateUserMutation,
  CreateUserMutationVariables
> = {
  kind: 'Document',
  definitions: [
    {
      kind: 'OperationDefinition',
      operation: 'mutation',
      name: {kind: 'Name', value: 'createUser'},
      variableDefinitions: [
        {
          kind: 'VariableDefinition',
          variable: {kind: 'Variable', name: {kind: 'Name', value: 'user'}},
          type: {
            kind: 'NonNullType',
            type: {kind: 'NamedType', name: {kind: 'Name', value: 'CreateDto'}},
          },
          directives: [],
        },
      ],
      directives: [],
      selectionSet: {
        kind: 'SelectionSet',
        selections: [
          {
            kind: 'Field',
            name: {kind: 'Name', value: 'createUser'},
            arguments: [
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'user'},
                value: {kind: 'Variable', name: {kind: 'Name', value: 'user'}},
              },
            ],
            directives: [],
            selectionSet: {
              kind: 'SelectionSet',
              selections: [
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'id'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'email'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'name'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'gender'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'dob'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'height'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'weight'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'createdAt'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'updatedAt'},
                  arguments: [],
                  directives: [],
                },
              ],
            },
          },
        ],
      },
    },
  ],
};
export const LoginDocument: TypedDocumentNode<
  LoginMutation,
  LoginMutationVariables
> = {
  kind: 'Document',
  definitions: [
    {
      kind: 'OperationDefinition',
      operation: 'mutation',
      name: {kind: 'Name', value: 'login'},
      variableDefinitions: [
        {
          kind: 'VariableDefinition',
          variable: {kind: 'Variable', name: {kind: 'Name', value: 'email'}},
          type: {
            kind: 'NonNullType',
            type: {kind: 'NamedType', name: {kind: 'Name', value: 'String'}},
          },
          directives: [],
        },
        {
          kind: 'VariableDefinition',
          variable: {kind: 'Variable', name: {kind: 'Name', value: 'password'}},
          type: {
            kind: 'NonNullType',
            type: {kind: 'NamedType', name: {kind: 'Name', value: 'String'}},
          },
          directives: [],
        },
      ],
      directives: [],
      selectionSet: {
        kind: 'SelectionSet',
        selections: [
          {
            kind: 'Field',
            name: {kind: 'Name', value: 'login'},
            arguments: [
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'email'},
                value: {kind: 'Variable', name: {kind: 'Name', value: 'email'}},
              },
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'password'},
                value: {
                  kind: 'Variable',
                  name: {kind: 'Name', value: 'password'},
                },
              },
            ],
            directives: [],
            selectionSet: {
              kind: 'SelectionSet',
              selections: [
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'user'},
                  arguments: [],
                  directives: [],
                  selectionSet: {
                    kind: 'SelectionSet',
                    selections: [
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'id'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'email'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'name'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'createdAt'},
                        arguments: [],
                        directives: [],
                      },
                      {
                        kind: 'Field',
                        name: {kind: 'Name', value: 'updatedAt'},
                        arguments: [],
                        directives: [],
                      },
                    ],
                  },
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'accessToken'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'refreshToken'},
                  arguments: [],
                  directives: [],
                },
              ],
            },
          },
        ],
      },
    },
  ],
};
export const MeDocument: TypedDocumentNode<MeQuery, MeQueryVariables> = {
  kind: 'Document',
  definitions: [
    {
      kind: 'OperationDefinition',
      operation: 'query',
      name: {kind: 'Name', value: 'me'},
      variableDefinitions: [],
      directives: [],
      selectionSet: {
        kind: 'SelectionSet',
        selections: [
          {
            kind: 'Field',
            name: {kind: 'Name', value: 'me'},
            arguments: [],
            directives: [],
            selectionSet: {
              kind: 'SelectionSet',
              selections: [
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'id'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'email'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'name'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'gender'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'weight'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'height'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'dob'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'userType'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'createdAt'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'updatedAt'},
                  arguments: [],
                  directives: [],
                },
              ],
            },
          },
        ],
      },
    },
  ],
};
export const UpdateUserDocument: TypedDocumentNode<
  UpdateUserMutation,
  UpdateUserMutationVariables
> = {
  kind: 'Document',
  definitions: [
    {
      kind: 'OperationDefinition',
      operation: 'mutation',
      name: {kind: 'Name', value: 'updateUser'},
      variableDefinitions: [
        {
          kind: 'VariableDefinition',
          variable: {kind: 'Variable', name: {kind: 'Name', value: 'user'}},
          type: {
            kind: 'NonNullType',
            type: {kind: 'NamedType', name: {kind: 'Name', value: 'UpdateDto'}},
          },
          directives: [],
        },
      ],
      directives: [],
      selectionSet: {
        kind: 'SelectionSet',
        selections: [
          {
            kind: 'Field',
            name: {kind: 'Name', value: 'updateUser'},
            arguments: [
              {
                kind: 'Argument',
                name: {kind: 'Name', value: 'user'},
                value: {kind: 'Variable', name: {kind: 'Name', value: 'user'}},
              },
            ],
            directives: [],
            selectionSet: {
              kind: 'SelectionSet',
              selections: [
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'id'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'email'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'name'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'gender'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'dob'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'weight'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'height'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'userType'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'createdAt'},
                  arguments: [],
                  directives: [],
                },
                {
                  kind: 'Field',
                  name: {kind: 'Name', value: 'updatedAt'},
                  arguments: [],
                  directives: [],
                },
              ],
            },
          },
        ],
      },
    },
  ],
};
