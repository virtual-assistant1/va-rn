import {
  StyleSheet,
  ScaledSize,
  ViewStyle,
  TextStyle,
  ImageStyle,
} from 'react-native';
import {useDimensions} from './dimensions';
import {useTheme} from './theme';
import {Theme} from 'models';

type NamedStyles<T> = {[P in keyof T]: ViewStyle | TextStyle | ImageStyle};

type ParamStyles = {
  theme: Theme;
  dimensions: ScaledSize;
};

type ThemedStylesFunc<T> = (props: ParamStyles) => T;

export function makeStyles<T extends NamedStyles<T> | NamedStyles<any>>(
  style: ThemedStylesFunc<T>,
) {
  return () => {
    const theme = useTheme();
    const dimensions = useDimensions();

    const styles = StyleSheet.create(style({theme, dimensions})) as {
      [P in keyof T]: any;
    };
    return {theme, styles, dimensions};
  };
}
