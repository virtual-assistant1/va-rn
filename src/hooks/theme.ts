import {useStore} from './store';

export const useTheme = () => useStore(state => state.theme);
