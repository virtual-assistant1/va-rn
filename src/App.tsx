import React, {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {StatusBar, Platform, useColorScheme} from 'react-native';
import {Navigator} from './Navigator';
import {StoreProvider} from 'easy-peasy';
import {ThemeProvider} from 'styled-components';
import {store} from './models';
import {useTheme, useAction} from './hooks';
import changeNavBarColor from 'react-native-navigation-bar-color';
import {ApolloProvider} from '@apollo/client';
import {client} from 'apollo.client';

const AppWithTheme = () => {
  const theme = useTheme();
  const schema = useColorScheme();

  const {setDark, setLight} = useAction((actions) => ({
    setDark: actions.setDarkTheme,
    setLight: actions.setLightTheme,
  }));

  useEffect(() => {
    if (Platform.OS === 'android') {
      changeNavBarColor(theme.colors.card, !theme.dark, false);
    }
  }, [theme]);

  useEffect(() => {
    if (schema === 'dark') {
      setDark();
    } else {
      setLight();
    }
  }, [schema, setDark, setLight]);

  return (
    <ThemeProvider theme={theme}>
      <ApolloProvider client={client}>
        <NavigationContainer theme={theme}>
          <StatusBar
            barStyle="light-content"
            backgroundColor={theme.colors.secondary}
          />
          <Navigator />
        </NavigationContainer>
      </ApolloProvider>
    </ThemeProvider>
  );
};

const App = () => {
  return (
    <StoreProvider store={store}>
      <AppWithTheme />
    </StoreProvider>
  );
};

export default App;
