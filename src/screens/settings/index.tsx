import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Gradient, LeftButton, RightButton} from 'components';
import {Settings} from './settings';

const Stack = createStackNavigator();

export const SettingsStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackground: () => <Gradient />,
        headerTintColor: '#fff',
        headerRight: () => <RightButton />,
      }}>
      <Stack.Screen
        component={Settings}
        name="Settings"
        options={{headerLeft: () => <LeftButton />}}
      />
    </Stack.Navigator>
  );
};
