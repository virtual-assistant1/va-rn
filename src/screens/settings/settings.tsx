import React, {FC} from 'react';
import {View, Text, Card, Button, Loader, Slider, Input} from 'components';
import {makeStyles, useForm} from 'hooks';
import {useNavigation} from '@react-navigation/core';
import {useQuery, useMutation} from '@apollo/client';
import {MeDocument, UpdateUserDocument, UpdateDto} from 'generated/types';

interface SettingsProps {}

export const Settings: FC<SettingsProps> = () => {
  const {styles, theme} = useStyles();
  const navigation = useNavigation();

  const {data, error, loading} = useQuery(MeDocument);
  const [updateUser] = useMutation(UpdateUserDocument);

  const {inputHandlers, handleSubmit, values, handleChange} = useForm<
    UpdateDto
  >({
    initial: {
      name: data?.me.name || '',
      weight: data?.me.weight || 0,
      height: data?.me.height || 0,
    },

    validationSchema: (f) => ({
      name: f.string().required(),
      height: f.number().required(),
      weight: f.number().required(),
    }),
    onSubmit: (f) => {
      updateUser({variables: {user: f}});
      navigation.navigate('HomeStack');
    },
  });

  if (error) {
    return (
      <View style={styles.root}>
        <Text>{JSON.stringify(error)}</Text>
      </View>
    );
  }

  if (loading) {
    return (
      <View style={styles.loaderContainer}>
        <Loader color={theme.colors.primary} size={50} />
      </View>
    );
  }

  if (!data?.me) {
    return (
      <View style={styles.root}>
        <Text>user not found</Text>
      </View>
    );
  }

  return (
    <View style={styles.root}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}> Your Profile Setting </Text>
      </View>

      <Card style={styles.card}>
        <Input
          {...inputHandlers('name')}
          placeholder="enter your name"
          icon={{name: 'user', type: 'Entypo'}}
          keyboardType="email-address"
        />

        <View style={styles.sliderContainer}>
          <Text>Weight {`: ${values.weight} KG`}</Text>
          <Slider
            minimumValue={1}
            maximumValue={200}
            minimumTrackTintColor="#7BE495"
            maximumTrackTintColor="#40AB9B"
            step={1}
            value={values.weight || 0}
            onValueChange={handleChange('weight')}
          />
        </View>

        <View style={styles.sliderContainer}>
          <Text>Height {`: ${values.height} CM`}</Text>
          <Slider
            minimumValue={1}
            maximumValue={200}
            minimumTrackTintColor="#7BE495"
            maximumTrackTintColor="#40AB9B"
            step={1}
            value={values.height || 0}
            onValueChange={handleChange('height')}
          />
        </View>
      </Card>

      <View style={styles.btnContainer}>
        <Button
          style={styles.btn}
          title="Update Profile"
          onPress={handleSubmit}
        />
      </View>
    </View>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    flex: 1,
  },
  titleContainer: {
    flex: 1,
    marginHorizontal: 30,
    justifyContent: 'center',
  },
  title: {
    fontSize: 25,
    opacity: 0.8,
    fontWeight: 'bold',
  },

  card: {
    flex: 2,
    marginHorizontal: 30,
    //flexDirection: 'row',
    paddingVertical: 60,
    paddingHorizontal: 15,
  },
  btn: {
    padding: 15,
    minWidth: 250,
  },
  btnContainer: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loaderContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  sliderContainer: {
    flex: 1,
  },
  slider: {
    flex: 1,
  },
}));
