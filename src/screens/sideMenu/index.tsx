import React from 'react';
import {DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer';
import {DrawerItemList} from './DrawerItemList';
import {Icon, View, Gradient, Text} from 'components';
import {useAction, makeStyles} from 'hooks';
import AsyncStorage from '@react-native-community/async-storage';
import config from 'config';
import {client} from 'apollo.client';
import {MeDocument} from 'generated/types';
import {useQuery} from '@apollo/client';

export const SideMenu = (props: any) => {
  const setIsAuth = useAction((actions) => actions.settings.setIsAuth);

  return (
    <DrawerContentScrollView {...props}>
      <UserCard />
      <DrawerItemList {...props} />
      <DrawerItem
        label="Logout"
        onPress={async () => {
          setIsAuth(false);
          await AsyncStorage.removeItem(config.accessTokenKey);
          await client.resetStore();
        }}
        icon={({color, size}) => (
          <Icon
            name="logout"
            type="MaterialCommunityIcons"
            style={{color, fontSize: size}}
          />
        )}
      />
    </DrawerContentScrollView>
  );
};

const UserCard = () => {
  const {data} = useQuery(MeDocument);
  const {styles} = useStyles();
  return (
    <View style={styles.root}>
      <View style={styles.overlay}>
        <Gradient />
      </View>
      <Gradient />
      <View style={styles.userText}>
        <Icon name="user" type="EvilIcons" style={styles.userIcon} />
        <View>
          <Text style={styles.txt}>Welcome back</Text>
          <Text style={styles.txt}>{data?.me.name}</Text>
        </View>
      </View>
    </View>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    height: 160,
    justifyContent: 'center',
  },

  overlay: {
    transform: [{translateY: -100}],
    height: 100,
    width: 300,
    position: 'absolute',
  },
  txt: {
    color: '#fff',
    fontSize: 16,
  },
  userIcon: {
    fontSize: 150,
    color: '#fff',
  },

  userText: {
    flexDirection: 'row',
    alignItems: 'center',
  },
}));
