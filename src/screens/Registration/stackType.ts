export type GenderType = 'male' | 'female';

export type RegisterStackParamList = {
  Registration: undefined;
  Gender: {email: string; password: string; name: string; userType: string};
  DateOfBirth: {
    gender: GenderType;
    email: string;
    password: string;
    name: string;
    userType: string;
  };
  WeightHeight: {
    gender: GenderType;
    email: string;
    password: string;
    name: string;
    userType: string;
    dob: number;
  };
  Agreement: {
    gender: GenderType;
    email: string;
    password: string;
    name: string;
    userType: string;
    dob: number;
    weight: number;
    height: number;
  };
};
