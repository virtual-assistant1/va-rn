import React, {FC, useState, useCallback} from 'react';
import {View, Text, Card, Button, ButtonInput} from 'components';
import {makeStyles} from 'hooks';
import {Alert, Platform} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import {useNavigation, useRoute} from '@react-navigation/core';
import {StackNavigationProp} from '@react-navigation/stack';
import {RouteProp} from '@react-navigation/native';
import {RegisterStackParamList} from './stackType';

interface DateOfBirthProps {}

const MAX_DATE = new Date();

export const DateOfBirth: FC<DateOfBirthProps> = () => {
  const {styles} = useStyles();
  const navigation = useNavigation<
    StackNavigationProp<RegisterStackParamList>
  >();
  const {params} = useRoute<RouteProp<RegisterStackParamList, 'DateOfBirth'>>();
  const [value, setValue] = useState<Date>(MAX_DATE);
  const [show, toggleShow] = useState(false);

  const handleNext = useCallback(() => {
    if (value === null) {
      Alert.alert('Validation', 'Select your date of birth');
      return;
    }
    navigation.navigate('WeightHeight', {
      ...params,
      dob: moment(value).unix(),
    });
  }, [value, navigation, params]);

  const handleDate = useCallback((_, date) => {
    if (Platform.OS === 'android') {
      toggleShow(false);
    }

    setValue(date);
  }, []);

  const handleShow = useCallback(() => {
    toggleShow((v) => !v);
  }, []);

  return (
    <View style={styles.root}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}> Your Date of birth</Text>
      </View>
      <Card style={styles.card}>
        {Platform.OS === 'ios' ? (
          <DateTimePicker
            maximumDate={MAX_DATE}
            value={value}
            onChange={handleDate}
            mode="date"
          />
        ) : (
          <View style={styles.dataAndroid}>
            <ButtonInput
              placeholder="please select your birth date"
              onPress={handleShow}
              value={moment(value).format('DD MMMM YYYY')}
              icon={{name: 'calendar', type: 'AntDesign'}}
            />
            {show && (
              <DateTimePicker
                maximumDate={MAX_DATE}
                value={value}
                onChange={handleDate}
                mode="date"
              />
            )}
          </View>
        )}
      </Card>

      <View style={styles.btnContainer}>
        <Button style={styles.btn} title="Next" onPress={handleNext} />
      </View>
    </View>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    flex: 1,
  },
  titleContainer: {
    flex: 1,
    marginHorizontal: 30,
    justifyContent: 'center',
  },
  title: {
    fontSize: 25,
    opacity: 0.8,
    fontWeight: 'bold',
  },

  card: {
    flex: 2,
    marginHorizontal: 30,
    justifyContent: 'center',
  },
  btn: {
    padding: 15,
    minWidth: 250,
  },
  btnContainer: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dataAndroid: {
    flex: 1,
    margin: 20,
  },
}));
