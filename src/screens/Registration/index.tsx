import React from 'react';
import {LeftButton, RightButton} from 'components';
import {DateOfBirth} from './dob';
import {Gender} from './gender';
import {WeightHeight} from './weightHeight';
import {Registration} from './registration';
import {Agreement} from './agreement';
import {createStackNavigator} from '@react-navigation/stack';

import {RegisterStackParamList} from './stackType';

const Stack = createStackNavigator<RegisterStackParamList>();

export const RegisterStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerRight: () => <RightButton />,
      }}>
      <Stack.Screen
        component={Registration}
        name="Registration"
        options={{headerLeft: () => <LeftButton />}}
      />

      <Stack.Screen component={Gender} name="Gender" />
      <Stack.Screen component={DateOfBirth} name="DateOfBirth" />
      <Stack.Screen component={WeightHeight} name="WeightHeight" />
      <Stack.Screen component={Agreement} name="Agreement" />
    </Stack.Navigator>
  );
};
