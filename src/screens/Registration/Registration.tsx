import React from 'react';
import {View, Button, Card, Input, Picker} from 'components';
import {makeStyles, useForm} from 'hooks';
import {useNavigation} from '@react-navigation/core';
import {CreateDto} from 'generated/types';
import {StackNavigationProp} from '@react-navigation/stack';
import {RegisterStackParamList} from './stackType';

export const Registration = () => {
  const {styles} = useStyle();
  const navigation = useNavigation<
    StackNavigationProp<RegisterStackParamList>
  >();

  const {inputHandlers, handleSubmit} = useForm<CreateDto>({
    initial: {
      email: '',
      password: '',
      name: '',
      userType: '',
    },
    onSubmit: (f) => {
      navigation.navigate('Gender', {
        email: f.email,
        password: f.password,
        name: f.name,
        userType: f.userType,
      });
    },
    validationSchema: (f) => ({
      email: f.string().email().required(),
      password: f.string().required(),
      name: f.string().required(),
      userType: f.string().required(),
    }),
  });

  return (
    <View style={styles.root}>
      <Card style={styles.card}>
        <Input
          {...inputHandlers('email')}
          placeholder="enter your email"
          icon={{name: 'user', type: 'Entypo'}}
          keyboardType="email-address"
          autoCapitalize="none"
        />
        <Input
          {...inputHandlers('password')}
          placeholder="enter your password"
          icon={{name: 'upcircleo', type: 'AntDesign'}}
          secureTextEntry
        />
        <Input
          {...inputHandlers('name')}
          placeholder="enter your name"
          icon={{name: 'user', type: 'Entypo'}}
          keyboardType="email-address"
        />
        <Picker
          icon={{name: 'users', type: 'FontAwesome5'}}
          placeholder="Select user type"
          items={[
            {value: 'patient', label: 'Patient'},
            {value: 'doctor', label: 'Doctor'},
            {value: 'studentDoctor', label: 'Student Doctor'},
          ]}
          {...inputHandlers('userType')}
        />
        <View style={styles.controls}>
          <Button style={styles.button} title="Next" onPress={handleSubmit} />
        </View>
      </Card>
    </View>
  );
};

const useStyle = makeStyles(() => ({
  root: {
    flex: 1,
  },

  card: {
    margin: 20,
    padding: 20,
  },

  button: {
    padding: 10,
    minWidth: 120,
  },

  controls: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 30,
  },
}));
