import React, {FC, useCallback} from 'react';
import {View, Text, Card, Button} from 'components';
import {makeStyles, useAction} from 'hooks';
import {useRoute} from '@react-navigation/core';
import {RouteProp} from '@react-navigation/native';
import {useMutation} from '@apollo/client';
import {CreateUserDocument, LoginDocument} from 'generated/types';
import {Alert} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import config from 'config';
import moment from 'moment';
import {RegisterStackParamList} from './stackType';

interface AgreementProps {}

export const Agreement: FC<AgreementProps> = () => {
  const {styles} = useStyles();
  const setIsAuth = useAction((actions) => actions.settings.setIsAuth);
  const {params} = useRoute<RouteProp<RegisterStackParamList, 'Agreement'>>();
  const [doLogin, {loading: loginLoading}] = useMutation(LoginDocument, {
    onCompleted: async ({login: {accessToken, refreshToken}}) => {
      await Promise.all([
        AsyncStorage.setItem(config.accessTokenKey, accessToken),
        AsyncStorage.setItem(config.refreshTokenKey, refreshToken),
      ]);
      setIsAuth(true);
    },
  });

  const [doRegister, {loading}] = useMutation(CreateUserDocument, {
    onError: (e) => {
      Alert.alert('Failed', e.message);
    },
    onCompleted: () => {
      doLogin({variables: {email: params.email, password: params.password}});
    },
  });

  const handleNext = useCallback(() => {
    doRegister({
      variables: {
        user: {
          ...params,
          dob: moment.unix(params.dob).toDate(),
        },
      },
    });
  }, [params, doRegister]);

  return (
    <View style={styles.root}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>Agreement</Text>
      </View>
      <Card style={styles.card}>
        <Text>User agreement</Text>
      </Card>

      <View style={styles.btnContainer}>
        <Button
          style={styles.btn}
          title="Register"
          onPress={handleNext}
          loading={loading || loginLoading}
        />
      </View>
    </View>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    flex: 1,
  },
  titleContainer: {
    flex: 1,
    marginHorizontal: 30,
    justifyContent: 'center',
  },
  title: {
    fontSize: 25,
    opacity: 0.8,
    fontWeight: 'bold',
  },

  card: {
    flex: 2,
    marginHorizontal: 30,
    paddingVertical: 60,
    paddingHorizontal: 15,
  },
  btn: {
    padding: 15,
    minWidth: 250,
  },
  btnContainer: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));
