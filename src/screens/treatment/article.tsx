import React, {FC} from 'react';
import {View, Text, Card, Button} from 'components';
import {makeStyles} from 'hooks';
import {Linking} from 'react-native';
import {Treatment} from 'generated/types';

interface ArticleProps {
  route: any;
}

export const Article: FC<ArticleProps> = ({route}) => {
  const {styles} = useStyles();
  const item: Treatment = route.params;

  return (
    <View style={styles.root}>
      <Card key={item.id} style={styles.card}>
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.summary}>{item.summary}</Text>
        <Button
          style={styles.btn}
          title="Read More"
          onPress={() => {
            Linking.canOpenURL(item.url).then((value) => {
              if (value) {
                Linking.openURL(item.url);
              }
            });
          }}
        />
      </Card>
    </View>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    flex: 1,
  },

  card: {
    padding: 10,
    margin: 10,
  },

  title: {
    fontWeight: 'bold',
  },
  summary: {
    opacity: 0.9,
    textAlign: 'justify',
    padding: 5,
    marginTop: 10,
  },

  btn: {
    width: 150,
    alignSelf: 'center',
    padding: 5,
  },
}));
