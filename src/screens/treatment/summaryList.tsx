import React, {FC} from 'react';
import {View, Text, Loader, Card, MenuItem} from 'components';
import {makeStyles} from 'hooks';
import {useQuery} from '@apollo/client';
import {DiseaseTreatmentsDocument} from 'generated/types';
import {ScrollView} from 'react-native';

interface SummaryListProps {
  route: any;
  navigation: any;
}
const icons = ['filetext1', 'file1', 'pdffile1', 'wordfile1'];

const random = () => {
  return icons[Math.floor(Math.random() * icons.length)];
};

export const SummaryList: FC<SummaryListProps> = ({route, navigation}) => {
  const {styles, theme} = useStyles();
  const diseaseCode = route.params.diseaseCode;
  const {loading, error, data} = useQuery(DiseaseTreatmentsDocument, {
    variables: {diseaseCode},
  });

  if (error) {
    return (
      <View style={styles.root}>
        <Text>{JSON.stringify(error)}</Text>
      </View>
    );
  }

  if (loading) {
    return (
      <View style={styles.loaderContainer}>
        <Loader color={theme.colors.primary} size={50} />
      </View>
    );
  }

  return (
    <View style={styles.root}>
      <ScrollView>
        <View style={styles.top}>
          <Text style={styles.title}>
            Below is a list of research articles that highlight the most recent
            treatments being tested or introduced into the medical field. Click
            on articles for more information about these treatments. Please note
            that these are not your only treatment options.
          </Text>
        </View>
        <Card style={styles.card}>
          {data?.diseaseTreatments.map((item) => (
            <MenuItem
              leftIcon={{name: random(), type: 'AntDesign'}}
              key={item.id}
              chevron
              title={item.title}
              titleProps={{style: {padding: 15}}}
              onPress={() => {
                navigation.navigate('Article', {...item});
              }}
            />
          ))}
        </Card>
      </ScrollView>
    </View>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    flex: 1,
  },
  loaderContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  card: {
    padding: 10,
    margin: 10,
    paddingBottom: 50,
  },

  title: {
    fontWeight: 'bold',
  },
  summary: {
    opacity: 0.9,
    textAlign: 'justify',
    padding: 5,
    marginTop: 10,
  },

  btn: {
    width: 150,
    alignSelf: 'center',
    padding: 5,
  },
  top: {
    padding: 20,
  },
  icons: {
    padding: 10,
  },

  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
}));
