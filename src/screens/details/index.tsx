import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {makeStyles} from 'hooks';
import {View, Button, Text} from 'components';

export const Details = () => {
  const navigation = useNavigation();
  const {styles} = useStyles();

  return (
    <View style={styles.root}>
      <Text>Details</Text>
      <Button
        title={{text: 'Back'}}
        onPress={() => {
          navigation.goBack();
        }}
      />
    </View>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
}));
