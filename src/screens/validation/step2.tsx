import React, {FC, useCallback, useState} from 'react';
import {useMutation} from '@apollo/client';
import {View, Text, Card, Button, RadioGroup} from 'components';
import {makeStyles} from 'hooks';
import {Prediction, RegisterValidationDocument} from 'generated/types';
import {Alert} from 'react-native';

interface Step2Props {
  route: any;
  navigation: any;
}

export const Step2: FC<Step2Props> = ({navigation, route}) => {
  const {styles} = useStyles();
  const [selected, setSelected] = useState<number | null>(null);
  const prediction: Prediction = route.params;
  const [registerValidation] = useMutation(RegisterValidationDocument);

  const handleNext = useCallback(() => {
    if (!selected) {
      Alert.alert('Validation', "Please what was your doctor's diagnosis");
      return;
    }

    const validationResults = prediction.diseases.some(
      (k) => k.code === selected,
    );

    registerValidation({
      variables: {validation: {id: prediction.id, validationResults}},
    });
    navigation.navigate('HomeStack');
  }, [navigation, registerValidation, prediction, selected]);

  const handleChange = useCallback(
    (ev) => {
      setSelected(ev.id);
    },
    [setSelected],
  );

  return (
    <View style={styles.root}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>What was your doctor's diagnosis?</Text>
      </View>
      <Card style={styles.card}>
        <RadioGroup options={diseases} onChange={handleChange} />
      </Card>

      <View style={styles.btnContainer}>
        <Button style={styles.btn} title="Next" onPress={handleNext} />
      </View>
    </View>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    flex: 1,
  },
  titleContainer: {
    flex: 1,
    marginHorizontal: 30,
    justifyContent: 'center',
  },
  title: {
    fontSize: 25,
    opacity: 0.8,
    fontWeight: 'bold',
  },

  card: {
    flex: 2,
    marginHorizontal: 30,
    flexDirection: 'row',
    paddingVertical: 60,
    paddingHorizontal: 15,
  },
  btn: {
    padding: 15,
    minWidth: 250,
  },
  btnContainer: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));

const diseases = [
  {label: 'Cardiomyopathy ', id: 3},
  {label: 'Coronary Artery Disease', id: 4},
  {label: 'Deep Vein Thrombosis ', id: 7},
  {label: 'Heart Attack ', id: 5},
  {label: 'Marfan Syndrome', id: 1},
  {label: 'Pulmonary Hypertension ', id: 6},
  {label: 'Stroke ', id: 8},
  {label: 'Arrhythmia', id: 2},
  {label: 'OTHER', id: 2000},
];
