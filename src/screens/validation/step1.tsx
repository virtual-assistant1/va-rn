import React, {FC, useCallback} from 'react';
import {View, Text, Card, RadioGroup} from 'components';
import {makeStyles} from 'hooks';
import {useNavigation} from '@react-navigation/core';
import {InteractionManager} from 'react-native';

interface Step1Props {
  route: any;
}

export const Step1: FC<Step1Props> = () => {
  const {styles} = useStyles();
  const navigation = useNavigation();

  const handleNext = useCallback(
    (ev) => {
      InteractionManager.runAfterInteractions(() => {
        setTimeout(() => {
          if (ev.id === 'yes') {
            navigation.navigate('Step2');
          } else {
            navigation.navigate('Step3');
          }
        }, 250);
      });
    },
    [navigation],
  );

  return (
    <View style={styles.root}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>
          H
          {'AVE YOU HAD A PHYSICAL CONSULTATION AND/OR FURTHER MEDICAL TESTING?'.toLowerCase()}
        </Text>
      </View>

      <Card style={styles.card}>
        <RadioGroup
          options={[
            {id: 'yes', label: 'Yes'},
            {id: 'no', label: 'No'},
          ]}
          onChange={handleNext}
        />
      </Card>

      <View style={styles.btnContainer}>
        {/* <Button style={styles.btn} title="Next" onPress={handleNext} /> */}
      </View>
    </View>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    flex: 1,
  },
  titleContainer: {
    flex: 1,
    marginHorizontal: 30,
    justifyContent: 'center',
  },
  title: {
    fontSize: 25,
    opacity: 0.8,
    fontWeight: 'bold',
  },

  card: {
    flex: 2,
    marginHorizontal: 30,
    flexDirection: 'row',
    paddingVertical: 60,
    paddingHorizontal: 15,
  },
  btn: {
    padding: 15,
    minWidth: 250,
  },
  btnContainer: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));
