import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Gradient, LeftButton, RightButton} from 'components';
import {Step1} from './step1';
import {Step2} from './step2';
import {Step3} from './step3';

const Stack = createStackNavigator();

export const ValidationStack = ({route}: {route: any}) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackground: () => <Gradient />,
        headerTintColor: '#fff',
        headerRight: () => <RightButton />,
      }}>
      <Stack.Screen
        component={Step1}
        name="Step1"
        options={{headerLeft: () => <LeftButton />, headerTitle: 'Validation'}}
        initialParams={{...route.params}}
      />
      <Stack.Screen
        component={Step2}
        name="Step2"
        initialParams={{...route.params}}
      />
      <Stack.Screen
        component={Step3}
        name="Step3"
        initialParams={{...route.params}}
      />
    </Stack.Navigator>
  );
};
