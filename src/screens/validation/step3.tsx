import React, {FC, useCallback} from 'react';
import {View, Text, Card, Button,RadioGroup} from 'components';
import {makeStyles} from 'hooks';
import {InteractionManager} from 'react-native';

interface Step3Props {
  navigation: any;
}

export const Step3: FC<Step3Props> = ({navigation}) => {
  const {styles} = useStyles();

  const handleNext = useCallback(
    (ev) => {
      InteractionManager.runAfterInteractions(() => {
        setTimeout(() => {
          if (ev.id === 'Did not book an appointmente') {
            navigation.navigate('AppointmentStack');
          } else {
            navigation.navigate('HomeStack');
          }
        }, 250);
      });
    },
    [navigation],
  );

  return (
    <View style={styles.root}>
   <View style={styles.titleContainer}>
        <Text style={styles.title}>
        Why?
        </Text>
      </View>
      <Card style={styles.card}>
        <RadioGroup
          options={[
            {id: 'Did not book an appointmente', label: 'Did not book an appointmente'},
            {id: 'Waiting for my appointment', label: 'Waiting for my appointment'},
          ]}
          onChange={handleNext}
        />
      </Card>


      <View style={styles.btnContainer}>
        <Button style={styles.btn} title="Next" onPress={handleNext} />
      </View>
    </View>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    flex: 1,
  },
  titleContainer: {
    flex: 1,
    marginHorizontal: 30,
    justifyContent: 'center',
  },
  title: {
    fontSize: 25,
    opacity: 0.8,
    fontWeight: 'bold',
  },

  card: {
    flex: 2,
    marginHorizontal: 30,
    flexDirection: 'row',
    paddingVertical: 60,
    paddingHorizontal: 15,
  },
  btn: {
    padding: 15,
    minWidth: 250,
  },
  btnContainer: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));
