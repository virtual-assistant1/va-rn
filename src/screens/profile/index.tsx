import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Gradient, LeftButton, RightButton} from 'components';
import {History} from './history';

import {Details} from './details';
import {SummaryList, Article} from '../treatment/';

const Stack = createStackNavigator();

export const ProfileStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackground: () => <Gradient />,
        headerTintColor: '#fff',
        headerRight: () => <RightButton />,
      }}>
      <Stack.Screen
        component={History}
        name="History"
        options={{headerLeft: () => <LeftButton />}}
      />

      <Stack.Screen component={Details} name="Details" />
      <Stack.Screen
        component={SummaryList}
        name="SummaryList"
        options={{headerTitle: 'Treatment Articles'}}
      />
      <Stack.Screen component={Article} name="Article" />
    </Stack.Navigator>
  );
};
