import React, {FC, useCallback} from 'react';
import {ScrollView, StyleSheet} from 'react-native';
import {View, Text, Card, Button} from 'components';
import {makeStyles} from 'hooks';
import {Prediction, MeDocument} from 'generated/types';
import {ResultItem} from '../predictor/ResultsItem';
import {useQuery} from '@apollo/client';

interface DetailsProps {
  route: any;
  navigation: any;
}

export const Details: FC<DetailsProps> = ({route, navigation}) => {
  const {styles, theme} = useStyles();
  const data: Prediction = route.params;

  const {data: userData} = useQuery(MeDocument);

  const handleGoAppointment = useCallback(() => {
    navigation.navigate('AppointmentStack');
  }, [navigation]);

  const handleGoValidate = useCallback(() => {
    navigation.navigate('ValidationStack', {...data});
  }, [navigation, data]);

  return (
    <View style={styles.root}>
      <ScrollView>
        <Card style={styles.card}>
          <Text>Diseases</Text>
          {data.diseases.map((i, index) => (
            <ResultItem
              key={i.code}
              code={i.code}
              disease={i.disease}
              rank={i.rank}
              index={index}
            />
          ))}

          <Text
            style={[
              styles.riskText,
              {color: data.risk ? theme.colors.error : theme.colors.text},
            ]}>
            {data.risk === false
              ? 'Based off our data, you are not at high risk for developing a cardiovascular disease (CVD). '
              : data.risk === true
              ? 'Based off our data, you are at high risk for developing a cardiovascular disease.'
              : ''}
          </Text>
        </Card>

        <View style={styles.btnContainer}>
          {userData?.me.userType === 'patient' && (
            <Button
              style={styles.btn}
              title="Appointment"
              onPress={handleGoAppointment}
              leftIcon={{
                name: 'doctor',
                type: 'MaterialCommunityIcons',
                style: {marginRight: 5},
              }}
            />
          )}
          {!data.validated && (
            <Button
              style={styles.btn}
              title="Validate"
              onPress={handleGoValidate}
              leftIcon={{
                name: 'carryout',
                type: 'AntDesign',
                style: {marginRight: 5},
              }}
              enabled={false}
            />
          )}
        </View>

        <Card style={styles.card}>
          <Text>User Information</Text>
          <View style={styles.row}>
            <Text>Gender</Text>
            <View style={styles.spacer} />
            <Text>{data.user.gender}</Text>
          </View>
          <View style={styles.row}>
            <Text>Age</Text>
            <View style={styles.spacer} />
            <Text>{data.user.age_year}</Text>
          </View>
          <View style={styles.row}>
            <Text>Weight</Text>
            <View style={styles.spacer} />
            <Text>{data.user.weight}</Text>
          </View>

          <View style={styles.row}>
            <Text>Height</Text>
            <View style={styles.spacer} />
            <Text>{data.user.height}</Text>
          </View>
          <View style={styles.row}>
            <Text>Active</Text>
            <View style={styles.spacer} />
            <Text>{data.user.active === 1 ? 'Active' : 'Not Active'}</Text>
          </View>
          <View style={styles.row}>
            <Text>alcohol</Text>
            <View style={styles.spacer} />
            <Text>
              {data.user.alcohol === 1 ? 'Alcoholic' : 'Non Alcoholic'}
            </Text>
          </View>
          <View style={styles.row}>
            <Text>Cholesterol</Text>
            <View style={styles.spacer} />
            <Text>
              {data.user.cholesterol === 1
                ? 'Normal'
                : data.user.cholesterol === 2
                ? 'Above Normal'
                : 'Well Above Normal'}
            </Text>
          </View>

          <View style={styles.row}>
            <Text>Glucose</Text>
            <View style={styles.spacer} />
            <Text>
              {data.user.glucose === 1
                ? 'Normal'
                : data.user.glucose === 2
                ? 'Above Normal'
                : 'Well Above Normal'}
            </Text>
          </View>

          <View style={styles.row}>
            <Text>Smoke</Text>
            <View style={styles.spacer} />
            <Text>{data.user.smoke === 1 ? 'Smoker' : 'Non Smoker'}</Text>
          </View>

          <View style={styles.row}>
            <Text>Systolic</Text>
            <View style={styles.spacer} />
            <Text>{data.user.systolic}</Text>
          </View>

          <View style={styles.row}>
            <Text>Diastolic</Text>
            <View style={styles.spacer} />
            <Text>{data.user.diastolic}</Text>
          </View>
        </Card>

        <Card style={styles.card}>
          <Text>Questions</Text>

          {data.questions.map((i, index) => (
            <View style={styles.row2} key={index}>
              <View style={styles.question}>
                <Text>{i.question}</Text>
              </View>
              <View style={styles.spacer} />
              <View style={styles.answer}>
                <Text>{i.answers.map((a) => a.answer).join(', ')}</Text>
              </View>
            </View>
          ))}
        </Card>
      </ScrollView>
    </View>
  );
};

const useStyles = makeStyles(({theme}) => ({
  root: {
    flex: 1,
  },
  card: {
    padding: 20,
    margin: 20,
  },
  row: {
    flexDirection: 'row',
    marginTop: 10,
    flex: 1,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: theme.colors.border,
    padding: 5,
  },
  spacer: {
    flex: 1,
  },
  row2: {
    flexDirection: 'row',
    marginTop: 10,
    flex: 1,
    justifyContent: 'space-between',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: theme.colors.border,
    padding: 5,
  },
  riskText: {
    marginTop: 20,
    fontStyle: 'italic',
  },
  question: {flex: 3},
  answer: {flex: 1},

  btn: {
    padding: 10,
    marginHorizontal: 7,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));
