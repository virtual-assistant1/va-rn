import React, {FC} from 'react';
import {View, Text, Loader, MenuItem} from 'components';
import {makeStyles} from 'hooks';
import {useQuery} from '@apollo/client';
import {UserEvaluationDocument} from 'generated/types';
import {FlatList} from 'react-native';
import moment from 'moment';

interface HistoryProps {
  navigation: any;
}

export const History: FC<HistoryProps> = ({navigation}) => {
  const {styles, theme} = useStyles();

  const {data, loading, error} = useQuery(UserEvaluationDocument);

  if (error) {
    return (
      <View style={styles.root}>
        <Text>{JSON.stringify(error)}</Text>
      </View>
    );
  }

  if (loading) {
    return (
      <View style={styles.loaderContainer}>
        <Loader color={theme.colors.primary} size={50} />
      </View>
    );
  }

  return (
    <View style={styles.root}>
      <FlatList
        data={data?.userEvaluations || []}
        keyExtractor={(i) => i.id}
        renderItem={({item}) => {
          const subTitle = item.diseases
            .map((i) => i.disease)
            .slice(0, 2)
            .join(', ');
          console.log({subTitle});
          return (
            <MenuItem
              leftIcon={{
                name: 'info',
                style: {color: theme.colors.secondary, padding: 10},
                type: 'Entypo',
              }}
              chevron
              subTitle={subTitle === '' ? 'No Disease' : subTitle}
              title={moment(item.createdAt).fromNow()}
              titleProps={{
                style: {marginHorizontal: 10, color: theme.colors.secondary},
              }}
              subTitleProps={{style: {marginHorizontal: 10, opacity: 0.7}}}
              onPress={() => {
                navigation.navigate('Details', {...item});
              }}
            />
          );
        }}
      />
    </View>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    flex: 1,
  },
  loaderContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
}));
