import React from 'react';
import {View, Loader} from 'components';
import {makeStyles} from 'hooks';

export const Splash = () => {
  const {styles, theme} = useStyles();

  return (
    <View style={styles.root}>
      <Loader color={theme.colors.primary} size={200} type="ThreeBounce" />
    </View>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
}));
