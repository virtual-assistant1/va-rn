import React, {FC, useCallback, useEffect} from 'react';
import {View, Text, Card, Loader, RadioGroup, Icon} from 'components';
import {makeStyles, useAction, useStore} from 'hooks';
import {StyleSheet, InteractionManager} from 'react-native';
import {useNavigation, useRoute} from '@react-navigation/core';
import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {useQuery} from '@apollo/client';
import {PredictorStackParamList} from './types';
import {GetQuestionsDocument} from 'generated/types';

interface QuestionProps {}

export const Question: FC<QuestionProps> = ({}) => {
  const {styles, theme, dimensions} = useStyles();
  const navigation = useNavigation<
    StackNavigationProp<PredictorStackParamList>
  >();

  const {pushQuestion} = useAction((actions) => ({
    pushQuestion: actions.prediction.pushQuestion,
  }));

  const {total, count} = useStore((state) => ({
    total: state.prediction.questionsTotal,
    count: state.prediction.questionsCount,
  }));

  const progress = count >= total ? 1 : count / total;

  const router = useRoute<RouteProp<PredictorStackParamList, 'Question'>>();

  const prevTriggerAnswer = router.params?.triggerAnswer;

  const {loading, data, error} = useQuery(GetQuestionsDocument, {
    variables: {answer: prevTriggerAnswer},
  });

  const handleNext = useCallback(
    (ev) => {
      pushQuestion({
        question: data?.questions[0].code || 0,
        answers: [ev.id],
        questionType: data?.questions[0].questionType || '',
      });
      InteractionManager.runAfterInteractions(() => {
        setTimeout(() => {
          navigation.push('Question', {triggerAnswer: ev.id});
        }, 250);
      });
    },
    [navigation, pushQuestion, data],
  );

  useEffect(() => {
    navigation.setParams({data});
    if (data?.questions.length === 0) {
      navigation.replace('Finish');
    }
  }, [data, navigation]);

  if (error) {
    return (
      <View style={styles.root}>
        <Text>{JSON.stringify(error)}</Text>
      </View>
    );
  }

  if (loading) {
    return (
      <View style={styles.loaderContainer}>
        <Loader color={theme.colors.primary} size={50} />
      </View>
    );
  }

  if (data?.questions.length === 0) {
    return (
      <View style={styles.loaderContainer}>
        <Loader color={theme.colors.primary} size={50} />
      </View>
    );
  }

  const question = data?.questions[0];

  return (
    <View style={styles.root}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>{question?.question}</Text>
      </View>
      <Card style={styles.card}>
        <View style={styles.watermark}>
          <Icon
            name="head-question"
            type="MaterialCommunityIcons"
            style={styles.icon}
          />
        </View>
        <View>
          <RadioGroup
            options={question?.answers.map((a) => ({
              id: a.code,
              label: a.answer,
            }))}
            onChange={handleNext}
          />
        </View>
      </Card>

      <View style={styles.btnContainer}>
        {/* <Button style={styles.btn} title="Back" onPress={handleNext} /> */}
        <Text style={styles.title}>{`${Math.round(progress * 100)}%`}</Text>
        <View
          style={{
            backgroundColor: '#bbb',
            width: dimensions.width - 40,
            borderBottomRightRadius: 15,
            borderTopRightRadius: 15,
            marginTop: 5,
          }}>
          <View
            style={{
              width: (dimensions.width - 40) * progress,
              backgroundColor: theme.colors.primary,
              height: 25,
              borderBottomRightRadius: 15,
              borderTopRightRadius: 15,
            }}
          />
        </View>
      </View>
    </View>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    flex: 1,
  },
  loaderContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleContainer: {
    flex: 1,
    marginHorizontal: 30,
    justifyContent: 'center',
  },
  title: {
    fontSize: 25,
    opacity: 0.8,
    fontWeight: 'bold',
  },

  card: {
    flex: 2,
    marginHorizontal: 30,
    flexDirection: 'row',
    paddingVertical: 60,
    paddingHorizontal: 15,
  },
  btn: {
    padding: 15,
    minWidth: 250,
  },
  btnContainer: {
    flex: 2,
    //flexDirection: 'row',
    justifyContent: 'center',
    //alignItems: 'center',
    marginLeft: 20,
  },
  watermark: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    padding: 30,
    opacity: 0.03,
  },
  icon: {
    fontSize: 200,
    transform: [{skewX: '30deg'}, {skewY: '-30deg'}],
  },
}));
