import {GetQuestionsQuery} from 'generated/types';

export type PredictorStackParamList = {
  FirstQuestion: undefined;
  Question: {triggerAnswer: number; data?: GetQuestionsQuery};
  Finish: undefined;
  SummaryList: undefined;
  Article: undefined;
};
