import React, {FC, useCallback} from 'react';
import {View, Text, Card, Button, Loader} from 'components';
import {makeStyles, useStore} from 'hooks';
import {useNavigation} from '@react-navigation/core';
import {useQuery, useMutation} from '@apollo/client';
import moment from 'moment';
import {
  EvaluateDocument,
  QaDto,
  SavePredictionDocument,
  MeDocument,
} from 'generated/types';
import {ResultItem} from './ResultsItem';

interface FinishProps {}

export const Finish: FC<FinishProps> = () => {
  const {styles, theme} = useStyles();
  const navigation = useNavigation();

  const {questionMap, risk, userProps} = useStore((s) => ({
    questionMap: s.prediction.questions,
    risk: s.prediction.predictionResult,
    userProps: s.prediction.predictionData,
  }));

  const {data: userData} = useQuery(MeDocument);

  const questions: QaDto[] = [];

  questionMap.forEach((v, k) => {
    questions.push({question: k, answers: v});
  });

  const {data, loading, error, called} = useQuery(EvaluateDocument, {
    variables: {
      questions,
    },
  });

  const [savePrediction] = useMutation(SavePredictionDocument);

  const handleSave = useCallback(() => {
    if (!called) {
      return;
    }
    return savePrediction({
      variables: {
        questions,
        userProps: {
          active: userProps.physicalActivity,
          alcohol: userProps.alcoholic,
          cholesterol: userProps.cholesterol,
          diastolic: userProps.diastolic,
          gender: userProps.gender,
          glucose: userProps.glucose,
          height: userProps.height,
          smoke: userProps.smoke,
          systolic: userProps.systolic,
          weight: userProps.weight,
          age_year: moment().diff(moment.unix(userProps.dob), 'years'),
          userId: userData?.me.id || 'dummy',
        },
        risk: risk || false,
        diseases:
          data?.evaluateAnswers.map((i) => ({
            code: i.code,
            disease: i.disease,
            rank: i.rank,
          })) || [],
      },
    });
  }, [savePrediction, userProps, risk, questions, data, userData, called]);

  const handleGoHome = useCallback(() => {
    handleSave();
    navigation.navigate('HomeStack');
  }, [navigation, handleSave]);

  const handleGoAppointment = useCallback(() => {
    handleSave();
    navigation.navigate('AppointmentStack');
  }, [navigation, handleSave]);

  // const handleGoValidate = useCallback(() => {
  //   handleSave()?.then((e) => {
  //     navigation.navigate('ValidationStack', {...e.data?.saveEvaluation});
  //   });
  // }, [navigation, handleSave]);

  if (error) {
    return (
      <View style={styles.root}>
        <Text>{JSON.stringify(error)}</Text>
      </View>
    );
  }

  if (loading) {
    return (
      <View style={styles.loaderContainer}>
        <Loader color={theme.colors.primary} size={50} />
      </View>
    );
  }

  return (
    <View style={styles.root}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>
          We would like to remind you that this is not a diagnosis nor does this
          replace any form of proper health care. We advice you to seek
          immediate medical attention if symptoms progress and/or to book an
          appointment with a local health care provider.
        </Text>
        <Text style={styles.subTitle}>
          Based off the answers you have provided, your symptoms may be due to
          any of the following:
        </Text>
      </View>
      <Card style={styles.card}>
        {data?.evaluateAnswers.map((i, index) => (
          <ResultItem
            key={i.code}
            code={i.code}
            disease={i.disease}
            rank={i.rank}
            index={index}
          />
        ))}

        <Text
          style={[
            styles.riskText,
            {color: risk ? theme.colors.error : theme.colors.text},
          ]}>
          {risk === false
            ? 'Based off our data, you are not at high risk for developing cardiovascular disease (CVD). '
            : risk === true
            ? 'Based off our data, you are at high risk for developing cardiovascular disease.'
            : ''}
        </Text>
      </Card>

      <View style={styles.btnContainer}>
        <Button
          style={styles.btn}
          title="Home"
          leftIcon={{name: 'home', style: {marginRight: 5}}}
          onPress={handleGoHome}
        />
        {userData?.me.userType === 'patient' && (
          <Button
            style={styles.btn}
            title="Appointment"
            onPress={handleGoAppointment}
            leftIcon={{
              name: 'doctor',
              type: 'MaterialCommunityIcons',
              style: {marginRight: 5},
            }}
          />
        )}
        {/* <Button
          style={styles.btn}
          title="Validate"
          onPress={handleGoValidate}
          leftIcon={{
            name: 'carryout',
            type: 'AntDesign',
            style: {marginRight: 5},
          }}
        /> */}
      </View>
    </View>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    flex: 1,
  },
  titleContainer: {
    flex: 1,
    marginHorizontal: 30,
    justifyContent: 'center',
  },
  title: {
    fontSize: 14,
    opacity: 0.8,
    //fontWeight: 'bold',
  },

  card: {
    flex: 2,
    marginHorizontal: 30,
    paddingVertical: 40,
    paddingHorizontal: 15,
  },
  btn: {
    padding: 10,
    marginHorizontal: 7,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loaderContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  subTitle: {
    fontWeight: 'bold',
    marginTop: 12,
    opacity: 0.8,
  },
  riskText: {
    marginTop: 20,
    fontStyle: 'italic',
  },
}));
