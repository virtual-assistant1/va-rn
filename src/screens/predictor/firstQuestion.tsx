import React, {FC, useCallback, useEffect} from 'react';
import {View, Text, Card, Loader, RadioGroup, Icon} from 'components';
import {makeStyles, useAction} from 'hooks';
import {StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/core';
import {useQuery} from '@apollo/client';
import {GetFirstQuestionDocument, QuestionCountDocument} from 'generated/types';

interface FirstQuestionProps {}

export const FirstQuestion: FC<FirstQuestionProps> = () => {
  const {styles, theme} = useStyles();
  const navigation = useNavigation();
  const {pushQuestion, resetQuestions, setQuestionsTotal} = useAction(
    (actions) => ({
      pushQuestion: actions.prediction.pushQuestion,
      resetQuestions: actions.prediction.resetQuestions,
      setQuestionsTotal: actions.prediction.setQuestionTotal,
    }),
  );

  const {loading, data, error} = useQuery(GetFirstQuestionDocument);

  useQuery(QuestionCountDocument, {
    skip: !data,
    variables: {
      questionType: 'main',
      parentQuestion: data?.firstQuestions[0].code || 1,
    },
    onCompleted: (results) => {
      if (results) {
        setQuestionsTotal(results.questionCount);
      }
    },
  });

  const handleNext = useCallback(
    (ev) => {
      pushQuestion({
        question: data?.firstQuestions[0].code || 0,
        answers: [ev.id],
        questionType: data?.firstQuestions[0].questionType || '',
      });

      navigation.navigate('Question', {triggerAnswer: ev.id});
    },
    [navigation, data, pushQuestion],
  );

  useEffect(() => {
    resetQuestions();
  }, [resetQuestions]);

  if (error) {
    return (
      <View style={styles.root}>
        <Text>{JSON.stringify(error)}</Text>
      </View>
    );
  }

  if (loading) {
    return (
      <View style={styles.loaderContainer}>
        <Loader color={theme.colors.primary} size={50} />
      </View>
    );
  }

  if (data?.firstQuestions.length === 0) {
    return (
      <View style={styles.loaderContainer}>
        <Text>no questions available</Text>
      </View>
    );
  }

  const question = data?.firstQuestions[0];

  return (
    <View style={styles.root}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>{question?.question}</Text>
      </View>
      <Card style={styles.card}>
        <View style={styles.watermark}>
          <Icon
            name="head-question"
            type="MaterialCommunityIcons"
            style={styles.icon}
          />
        </View>
        <View>
          <RadioGroup
            options={question?.answers.map((a) => ({
              id: a.code,
              label: a.answer,
            }))}
            onChange={handleNext}
          />
        </View>
      </Card>

      <View style={styles.btnContainer}>
        {/* <Button style={styles.btn} title="Next" onPress={handleNext} /> */}
      </View>
    </View>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    flex: 1,
  },
  loaderContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleContainer: {
    flex: 1,
    marginHorizontal: 30,
    justifyContent: 'center',
  },
  title: {
    fontSize: 25,
    opacity: 0.8,
    fontWeight: 'bold',
  },

  card: {
    flex: 2,
    marginHorizontal: 30,
    flexDirection: 'row',
    paddingVertical: 60,
    paddingHorizontal: 15,
  },
  btn: {
    padding: 15,
    minWidth: 250,
  },
  btnContainer: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  watermark: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    padding: 30,
    opacity: 0.03,
  },
  icon: {
    fontSize: 200,
    transform: [{skewX: '30deg'}, {skewY: '-30deg'}],
  },
}));
