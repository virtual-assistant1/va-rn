import React, {FC, useCallback} from 'react';
import {View, Text, IconButton} from 'components';
import {makeStyles} from 'hooks';
import Animated, {Easing, concat, round} from 'react-native-reanimated';
import {
  useValue,
  tween2d,
  withTimingTransition,
  mix,
  ReText,
} from 'react-native-redash';
import {useNavigation} from '@react-navigation/core';

interface ResultProps {
  code: number;
  disease: string;
  rank: number;
  index: number;
}

export const ResultItem: FC<ResultProps> = ({disease, rank, index, code}) => {
  const {styles, dimensions} = useStyles();
  const start = useValue<0 | 1>(0);
  const width = rank * (dimensions.width - 100);
  const transition = withTimingTransition(start, {
    easing: Easing.inOut(Easing.ease),
    duration: 800,
  });

  const transform = tween2d(
    transition,
    [{translateX: -width}],
    [{translateX: 0}],
  );

  const counter = mix(transition, 0, rank * 100);
  const navigation = useNavigation();
  const handleLayout = useCallback(() => {
    setTimeout(() => {
      start.setValue(1);
    }, index * 150);
  }, [start, index]);

  const handleTreatment = useCallback(() => {
    navigation.navigate('SummaryList', {diseaseCode: code});
  }, [navigation, code]);

  return (
    <View style={styles.root}>
      <View style={styles.textContainer}>
        <Text>{disease}</Text>
        <View style={styles.row}>
          <ReText text={concat(round(counter), '%')} style={styles.rank} />
          <IconButton
            icon={{name: 'medicinebox', type: 'AntDesign'}}
            style={styles.btn}
            onPress={handleTreatment}
          />
        </View>
      </View>
      <Animated.View
        onLayout={handleLayout}
        style={[styles.bar, {width}, {transform}]}
      />
    </View>
  );
};

const useStyles = makeStyles(({theme}) => ({
  root: {
    marginVertical: 10,
    overflow: 'hidden',
  },
  textContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  bar: {
    height: 10,
    width: 10,
    backgroundColor: theme.colors.primary,
    borderBottomRightRadius: 5,
    borderTopRightRadius: 5,
  },
  rank: {
    color: theme.colors.text,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  btn: {
    marginHorizontal: 10,
  },
}));
