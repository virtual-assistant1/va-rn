import React, {useCallback} from 'react';
import {
  createStackNavigator,
  StackNavigationProp,
} from '@react-navigation/stack';
import {Gradient, LeftButton, RightButton, IconButton} from 'components';
import {FirstQuestion} from './firstQuestion';
import {Question} from './question';
import {Finish} from './finish';
import {PredictorStackParamList} from './types';
import {Platform} from 'react-native';
import {useAction} from 'hooks';
import {useRoute, RouteProp, useNavigation} from '@react-navigation/core';
import {SummaryList} from '../treatment/summaryList';
import {Article} from '../treatment/article';

const Stack = createStackNavigator<PredictorStackParamList>();

const PopButton = () => {
  const navigation = useNavigation<
    StackNavigationProp<PredictorStackParamList>
  >();

  const router = useRoute<RouteProp<PredictorStackParamList, 'Question'>>();
  const {popQuestion} = useAction((actions) => ({
    popQuestion: actions.prediction.popQuestion,
  }));

  const {data} = router.params;

  const handlePop = useCallback(() => {
    if (data?.questions) {
      popQuestion({
        question: data?.questions[0].code || 0,
        questionType: data?.questions[0].questionType || '',
      });
    }
    navigation.goBack();
  }, [data, popQuestion, navigation]);

  return (
    <IconButton
      onPress={handlePop}
      icon={{
        name: Platform.OS === 'ios' ? 'chevron-back' : 'arrow-back',
        style: {fontSize: 30, color: '#fff'},
      }}
    />
  );
};

export const PredictorStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackground: () => <Gradient />,
        headerTintColor: '#fff',
        headerRight: () => <RightButton />,
      }}>
      <Stack.Screen
        component={FirstQuestion}
        name="FirstQuestion"
        options={{
          headerLeft: () => <LeftButton />,
        }}
      />
      <Stack.Screen
        component={Question}
        name="Question"
        options={{headerLeft: () => <PopButton />}}
      />
      <Stack.Screen component={Finish} name="Finish" />
      <Stack.Screen
        component={SummaryList}
        name="SummaryList"
        options={{headerTitle: 'Treatment Articles'}}
      />
      <Stack.Screen component={Article} name="Article" />
    </Stack.Navigator>
  );
};
