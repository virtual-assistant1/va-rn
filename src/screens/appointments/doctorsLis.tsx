import React, {FC} from 'react';
import {
  View,
  Text,
  Loader,
  Touchable,
  Card,
  IconButton,
  Icon,
} from 'components';
import {makeStyles} from 'hooks';
import {useQuery} from '@apollo/client';
import {DoctorsDocument} from 'generated/types';
import {FlatList, StyleSheet} from 'react-native';

interface DoctorsLisProps {
  navigation: any;
}

export const DoctorsLis: FC<DoctorsLisProps> = ({navigation}) => {
  const {styles, theme} = useStyles();
  const {data, loading, error} = useQuery(DoctorsDocument);

  if (error) {
    return (
      <View style={styles.root}>
        <Text>{JSON.stringify(error)}</Text>
      </View>
    );
  }

  if (loading) {
    return (
      <View style={styles.loaderContainer}>
        <Loader color={theme.colors.primary} size={50} />
      </View>
    );
  }

  return (
    <View style={styles.root}>
      <FlatList
        data={data?.doctors || []}
        keyExtractor={(item) => item.id}
        renderItem={({item}) => (
          <Touchable
            style={styles.touch}
            onPress={() => {
              navigation.navigate('DoctorDetails', {...item});
            }}>
            <Card style={styles.card}>
              <View style={styles.row}>
                <View>
                  <Icon
                    name="doctor"
                    type="MaterialCommunityIcons"
                    style={styles.icon}
                  />
                </View>
                <View style={styles.userCard}>
                  <Text style={styles.name}>{item.name}</Text>
                  <View style={styles.userInfo}>
                    <Text style={styles.userData}>{item.specialization}</Text>
                    <Text style={styles.userData}>{item.city}</Text>
                    <Text style={styles.userData}>{item.address}</Text>
                    <Text style={styles.userData}>{item.experience}</Text>
                  </View>
                </View>
              </View>
              <View style={[styles.row, styles.tailing]}>
                <View style={styles.closing}>
                  <Text style={styles.open}>Open Today</Text>
                </View>
                <View style={styles.time}>
                  <Text>{`${item.openingTime} - ${item.closingTime}`}</Text>
                </View>
              </View>
            </Card>
          </Touchable>
        )}
      />
    </View>
  );
};

const useStyles = makeStyles(({theme}) => ({
  root: {
    flex: 1,
  },
  loaderContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  card: {
    padding: 10,
  },
  touch: {
    margin: 10,
  },
  row: {
    flexDirection: 'row',
  },
  icon: {
    fontSize: 120,
  },
  userInfo: {
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: theme.colors.border,
    padding: 5,
    borderRadius: 5,
    flex: 1,
  },

  userCard: {
    flex: 1,
    marginHorizontal: 10,
  },

  userData: {
    opacity: 0.7,
  },

  name: {
    fontSize: 22,
    fontWeight: 'bold',
    margin: 10,
  },
  tailing: {
    flex: 1,
    marginVertical: 5,
  },

  closing: {
    minWidth: 130,
    alignItems: 'center',
  },

  time: {
    alignItems: 'center',
    flex: 1,
  },
  open: {
    color: 'green',
  },
}));
