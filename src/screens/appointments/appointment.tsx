import React, {FC} from 'react';
import {View, Text, Icon, Input, Button} from 'components';
import {useQuery, useMutation} from '@apollo/client';
import {makeStyles, useForm} from 'hooks';
import {
  Doctor,
  MeDocument,
  CreateAppointmentDocument,
  CreateAppointmentDto,
} from 'generated/types';
import moment from 'moment';

interface AppointmentProps {
  route: any;
  navigation: any;
}

interface Params {
  date: number;
  time: string;
  doctor: Doctor;
}

const phoneRX = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

export const Appointment: FC<AppointmentProps> = ({route, navigation}) => {
  const {styles} = useStyles();
  const {date, doctor, time} = route.params as Params;
  const {data} = useQuery(MeDocument);
  const [createAppointment, {loading}] = useMutation(
    CreateAppointmentDocument,
    {
      onCompleted: () => {
        navigation.navigate('HomeStack');
      },
    },
  );
  const {inputHandlers, handleSubmit} = useForm<CreateAppointmentDto>({
    initial: {
      doctorId: doctor.id,
      date: moment.unix(date).toDate(),
      time: time,
      mobile: '',
      name: data?.me.name || '',
      email: data?.me.email || '',
    },
    validationSchema: (f) => ({
      date: f.date().required(),
      doctorId: f.string().required(),
      time: f.string().required(),
      email: f.string().email().required(),
      mobile: f
        .string()
        .matches(phoneRX, 'Mobile number is not valid')
        .required(),
      name: f.string().required(),
    }),
    onSubmit: (f) => {
      createAppointment({variables: {appointment: f}});
    },
  });

  return (
    <View style={styles.root}>
      <View style={styles.row}>
        <View>
          <Icon
            name="doctor"
            type="MaterialCommunityIcons"
            style={styles.icon}
          />
        </View>
        <View style={styles.userCard}>
          <Text style={styles.name}>{doctor.name}</Text>
        </View>
      </View>
      <View style={[styles.row, styles.bigBox]}>
        <View style={[styles.part, styles.rightBox]}>
          <Text style={styles.muted}>DATE & TIME</Text>
          <Text>{`${moment.unix(date).format('dddd, DD MMM')}`}</Text>
          <Text>{time}</Text>
        </View>
        <View style={[styles.part]}>
          <Text style={styles.muted}>Consultation Fees</Text>
          <Text>200 NIS</Text>
        </View>
      </View>

      <View style={styles.form}>
        <Input placeholder="Name" {...inputHandlers('name')} />
        <Input placeholder="E-mail" {...inputHandlers('email')} />
        <Input placeholder="Mobile Number" {...inputHandlers('mobile')} />
      </View>

      <View style={styles.btnContainer}>
        <Button
          style={styles.btn}
          title="Book"
          onPress={handleSubmit}
          loading={loading}
        />
      </View>
    </View>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    flex: 1,
  },

  row: {
    flexDirection: 'row',
    margin: 15,
  },
  icon: {
    fontSize: 120,
  },
  userInfo: {
    padding: 5,
    borderRadius: 5,
    flex: 1,
  },

  userCard: {
    flex: 1,
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },

  name: {
    fontSize: 22,
    fontWeight: 'bold',
    margin: 10,
  },

  part: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: 100,
  },
  bigBox: {
    margin: 20,
    borderBottomColor: '#ECECEC',
    borderBottomWidth: 1,
    padding: 10,
  },

  rightBox: {
    borderRightColor: '#ECECEC',
    borderRightWidth: 1,
  },
  muted: {
    opacity: 0.5,
  },
  form: {
    padding: 20,
  },
  btn: {
    padding: 15,
    minWidth: 250,
  },
  btnContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 200,
  },
}));
