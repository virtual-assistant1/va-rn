import React, {FC, useState, useCallback} from 'react';
import {View, Text, Icon, Touchable, Button} from 'components';
import {makeStyles} from 'hooks';
import {Doctor} from 'generated/types';
import Calender from 'react-native-calendar-strip';
import moment from 'moment';
import {StyleSheet, Alert} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const startDate = moment().toDate();
const endDate = moment().add(1, 'month').toDate();
const morningTimes = ['10:00', '11:00', '12:00'];
const afternoonTimes = ['13:00', '14:00', '15:00', '16:00'];
const eveningTimes = ['17:00', '18:00', '19:00', '20:00', '21:00'];
interface DoctorDetailsProps {
  route: any;
  navigation: any;
}

export const DoctorDetails: FC<DoctorDetailsProps> = ({route, navigation}) => {
  const doctor: Doctor = route.params;
  const {styles} = useStyles();

  const [date, setDate] = useState<Date>(startDate);
  const [time, setTime] = useState('');
  const handleNext = useCallback(() => {
    if (time === '') {
      Alert.alert('Time', ' Please choose the appointment time.');
      return;
    }

    navigation.navigate('Appointment', {
      time,
      date: moment(date).unix(),
      doctor,
    });
  }, [navigation, time, date, doctor]);

  return (
    <View style={styles.root}>
      <View style={styles.row}>
        <View>
          <Icon
            name="doctor"
            type="MaterialCommunityIcons"
            style={styles.icon}
          />
        </View>
        <View style={styles.userCard}>
          <Text style={styles.name}>{doctor.name}</Text>
          <View style={styles.userInfo}>
            <Text style={styles.userData}>{doctor.specialization}</Text>
            <Text style={styles.userData}>{doctor.telephone}</Text>
            <Text style={styles.userData}>{doctor.mobile}</Text>
          </View>
        </View>
      </View>

      <View>
        <Calender
          style={styles.calender}
          daySelectionAnimation={{
            type: 'border',
            duration: 1000,
            borderWidth: 1,
            borderHighlightColor: 'green',
          }}
          scrollable
          minDate={startDate}
          maxDate={endDate}
          selectedDate={date}
          onDateSelected={setDate}
        />
      </View>

      <View style={styles.otherBox}>
        <View style={styles.timeCard}>
          <View style={styles.timeTile}>
            <LinearGradient
              colors={['#FFFB91', '#FF9FFF']}
              style={StyleSheet.absoluteFillObject}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 1}}
            />
            <Text>Morning</Text>
          </View>
          <View style={styles.timesContainer}>
            {morningTimes.map((t) => (
              <Touchable
                key={t}
                style={[styles.timeStyle, t === time && styles.active]}
                onPress={() => {
                  setTime(t);
                }}>
                <Text>{t}</Text>
              </Touchable>
            ))}
          </View>
        </View>

        <View style={styles.timeCard}>
          <View style={styles.timeTile}>
            <LinearGradient
              colors={['#E0CCFF', '#C1FFF1']}
              style={StyleSheet.absoluteFillObject}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 1}}
            />
            <Text>Afternoon</Text>
          </View>
          <View style={styles.timesContainer}>
            {afternoonTimes.map((t) => (
              <Touchable
                key={t}
                style={[styles.timeStyle, t === time && styles.active]}
                onPress={() => {
                  setTime(t);
                }}>
                <Text>{t}</Text>
              </Touchable>
            ))}
          </View>
        </View>

        <View style={styles.timeCard}>
          <View style={styles.timeTile}>
            <LinearGradient
              colors={['#90E4FF', '#9FFFF5']}
              style={StyleSheet.absoluteFillObject}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 1}}
            />
            <Text>Evening & Night</Text>
          </View>
          <View style={styles.timesContainer}>
            {eveningTimes.map((t) => (
              <Touchable
                key={t}
                style={[styles.timeStyle, t === time && styles.active]}
                onPress={() => {
                  setTime(t);
                }}>
                <Text>{t}</Text>
              </Touchable>
            ))}
          </View>
        </View>
      </View>
      <View style={styles.btnContainer}>
        <Button style={styles.btn} title="Next" onPress={handleNext} />
      </View>
    </View>
  );
};

const useStyles = makeStyles(({theme}) => ({
  root: {
    flex: 1,
  },

  row: {
    flexDirection: 'row',
    margin: 15,
  },
  icon: {
    fontSize: 120,
  },
  userInfo: {
    padding: 5,
    borderRadius: 5,
    flex: 1,
  },

  userCard: {
    flex: 1,
    marginHorizontal: 10,
  },

  userData: {
    opacity: 0.7,
  },

  name: {
    fontSize: 22,
    fontWeight: 'bold',
    margin: 10,
  },
  calender: {
    height: 120,
    padding: 10,
  },
  otherBox: {
    flex: 1,
  },

  timeCard: {
    padding: 10,
    backgroundColor: theme.dark ? '#fff' : '#FBFBFB',
    borderColor: theme.dark ? '#D6D6D6' : '#D6D6D6',
    borderWidth: StyleSheet.hairlineWidth,
    margin: 20,
    borderRadius: 15,
    paddingTop: 25,
  },
  timeTile: {
    borderRadius: 15,
    backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: 30,
    top: -13,
    paddingHorizontal: 20,
    paddingVertical: 5,
    overflow: 'hidden',
  },

  timesContainer: {
    flexDirection: 'row',
  },

  timeStyle: {
    paddingHorizontal: 15,
    paddingVertical: 7,
  },

  active: {
    backgroundColor: theme.colors.primary,
    borderRadius: 10,
  },
  btn: {
    padding: 15,
    minWidth: 250,
  },
  btnContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 200,
  },
}));
