import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Gradient, LeftButton, RightButton} from 'components';
import {DoctorsLis} from './doctorsLis';
import {DoctorDetails} from './doctorDetails';
import {Appointment} from './appointment';

const Stack = createStackNavigator();

export const AppointmentStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackground: () => <Gradient />,
        headerTintColor: '#fff',
        headerRight: () => <RightButton />,
      }}>
      <Stack.Screen
        component={DoctorsLis}
        name="DoctorsLis"
        options={{headerLeft: () => <LeftButton />}}
      />
      <Stack.Screen component={DoctorDetails} name="DoctorDetails" />
      <Stack.Screen component={Appointment} name="Appointment" />
    </Stack.Navigator>
  );
};
