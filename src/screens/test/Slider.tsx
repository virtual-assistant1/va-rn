import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  Dimensions,
  FlatList,
  View,
  ListRenderItemInfo,
} from 'react-native';
import faker from 'faker';
import Animated from 'react-native-reanimated';
import Gradient from 'react-native-linear-gradient';
import {onScroll, bInterpolate, useValues} from 'react-native-redash';
import {makeStyles} from 'hooks';

const {interpolate, Extrapolate, concat} = Animated;
const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);
const screen = Dimensions.get('window');
const MARGIN = 100;

export interface Card extends Faker.Card {
  image: String;
  index: Number;
}

const fakeData: Array<Card> = Array.apply(null, Array(50)).map((_, index) => ({
  ...faker.helpers.createCard(),
  image: `https://picsum.photos/${screen.width}/600?tag=${faker.random.uuid()}`,
  index,
}));

export interface SliderProps {
  componentId: string;
}

export interface ItemProps {
  item: Card;
  index: number;
  width: number;
  scrollX: Animated.Value<number>;
}

function Item({item, index, width, scrollX}: ItemProps) {
  const {styles} = useStyles();
  const prev = width * (index - 1);
  const current = width * index;
  const next = width * (index + 1);
  const animation = interpolate(scrollX, {
    inputRange: [prev, current, next],
    outputRange: [0, 1, 0],
    extrapolate: Extrapolate.CLAMP,
  });

  const opacity = bInterpolate(animation, 0.3, 1);
  const TextTranslateY = bInterpolate(animation, 20, -20);
  const scale = bInterpolate(animation, 0.85, 1);
  const translateY = bInterpolate(animation, -100, 0);

  const angle = bInterpolate(animation, 10, 0);
  const rotate = concat(angle, 'deg');
  const translateX = bInterpolate(animation, -40, 40);

  return (
    <View style={{width, alignItems: 'center'}}>
      <Animated.View
        style={{
          borderRadius: 10,
          overflow: 'hidden',
          opacity,
          transform: [{scale}],
        }}>
        <View style={{overflow: 'hidden', width, alignItems: 'center'}}>
          <Animated.Image
            source={{
              uri: `${item.image}`,
              width: width + 80,
            }}
            resizeMode="cover"
            style={{
              width: width + 80,
              height: screen.height * 0.6,
              transform: [{translateX}],
            }}
          />
        </View>
        <Gradient
          colors={['rgba(0,0,0,0.4)', 'rgba(0,0,0,0.1)']}
          style={StyleSheet.absoluteFill}
        />
        <View
          style={[
            StyleSheet.absoluteFill,
            {padding: 10, justifyContent: 'space-between'},
          ]}>
          <Animated.View
            style={{
              transform: [{translateY}, {rotate}],
            }}>
            <Text style={{color: '#FFF', fontSize: 20, fontWeight: 'bold'}}>
              {item.name}
            </Text>
          </Animated.View>
          <Animated.View
            style={{
              transform: [{translateY: TextTranslateY}],
              opacity: animation,
              backgroundColor: 'rgba(0,0,0,0.3)',
              borderRadius: 10,
              padding: 10,
            }}>
            <Text style={styles.dataText}>{item.email}</Text>
            <Text style={styles.dataText}>{item.company.name}</Text>
            <Text style={styles.dataText}>{item.website}</Text>
            <Text style={styles.dataText}>{item.phone}</Text>
          </Animated.View>
        </View>
      </Animated.View>
    </View>
  );
}

export function Slider() {
  const {styles} = useStyles();
  const width = screen.width - MARGIN;
  const [scrollX] = useValues([0], []);

  return (
    <SafeAreaView style={styles.page}>
      <View style={styles.header}>
        <Text style={styles.headerTitle}>My App Logic goes here</Text>
      </View>
      <View style={[styles.listContainer, {width: screen.width}]}>
        <AnimatedFlatList
          scrollEventThrottle={16}
          onScroll={onScroll({x: scrollX})}
          showsHorizontalScrollIndicator={false}
          style={[styles.flatList, {width}]}
          horizontal
          pagingEnabled
          data={fakeData}
          renderItem={({item, index}: ListRenderItemInfo<Card>) => (
            <Item scrollX={scrollX} width={width} item={item} index={index} />
          )}
          keyExtractor={(item: Card) => `${item.index}`}
        />
      </View>
    </SafeAreaView>
  );
}

const useStyles = makeStyles(({theme: {colors}}) => ({
  headerTitle: {
    color: colors.text,
  },
  header: {
    flex: 1,
  },
  listContainer: {
    alignItems: 'center',
    marginVertical: 30,
  },
  flatList: {
    overflow: 'visible',
  },
  page: {
    flex: 1,
    backgroundColor: colors.background,
  },
  dataText: {
    color: 'white',
  },
}));
