import React from 'react';
import {createSharedElementStackNavigator} from 'react-navigation-shared-element';
import {HomeHeader} from './Home';
import {SubHome} from './SubHome';

const {Navigator, Screen} = createSharedElementStackNavigator();

export default () => {
  return (
    <Navigator
      screenOptions={{
        title: '',
        headerTransparent: true,
        cardStyleInterpolator: ({current: {progress}}) => {
          const opacity = progress.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolate: 'clamp',
          });
          return {cardStyle: {opacity}};
        },
      }}>
      <Screen name="shared" component={HomeHeader} />
      <Screen
        name="subShared"
        component={SubHome}
        sharedElementsConfig={route => {
          const {item} = route.params;
          return [{id: item.id, resize: 'stretch'}, `gradient-${item.id}`];
        }}
      />
    </Navigator>
  );
};
