import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageSourcePropType,
  Dimensions,
  FlatList,
  ListRenderItemInfo,
  SafeAreaView,
} from 'react-native';
import faker from 'faker';
import Animated from 'react-native-reanimated';
import {onScroll, useValues} from 'react-native-redash';
import Gradient from 'react-native-linear-gradient';
import {makeStyles} from 'hooks';
import {Card, RectButton, Image} from 'components';
import {SharedElement} from 'react-navigation-shared-element';
import {useNavigation} from '@react-navigation/native';

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const screen = Dimensions.get('window');

type Post = {
  id: string;
  title: string;
  description: string;
  image: ImageSourcePropType;
};

type ItemProps = Post & {
  index: number;
};

const fakeData: Array<Post> = Array.apply(null, Array(100)).map(() => ({
  id: faker.random.uuid(),
  title: faker.lorem.sentence(2, 4),
  description: faker.lorem.paragraph(2),
  image: {
    height: 200,
    width: screen.width - 20,
    uri: `https://picsum.photos/${Math.floor(screen.width) -
      20}/300?tag=${faker.random.uuid()}`,
  },
}));

const fakeImage: ImageSourcePropType = {
  uri: `https://picsum.photos/${Math.floor(
    screen.width,
  )}/300?tag=${faker.random.uuid()}`,
  height: 300,
  width: screen.width,
};

const Item = (item: ItemProps) => {
  const navigation = useNavigation();
  const {styles} = useStyles();
  return (
    <View style={styles.itemContainer}>
      <Card style={styles.card}>
        <RectButton
          onPress={() => {
            navigation.navigate('subShared', {item});
          }}>
          <View style={styles.imageContainer}>
            <SharedElement id={item.id}>
              <Image
                source={{uri: item.image.uri}}
                style={{
                  width: item.image.width,
                  height: item.image.height,
                }}
              />
            </SharedElement>
            <SharedElement id={`gradient-${item.id}`} style={styles.fill}>
              <Gradient
                style={styles.fill}
                colors={['rgba(0,0,0,0.6)', 'rgba(0,0,0,0.1)']}
              />
            </SharedElement>
          </View>

          <View style={styles.cardBody}>
            <Text style={styles.title}>{item.title}</Text>
            <Text style={styles.description}>{item.description}</Text>
          </View>
        </RectButton>
      </Card>
    </View>
  );
};

type HeaderProps = {
  scrollY: Animated.Value<number>;
  children: JSX.Element;
};

const ListHeaderComponent = ({scrollY, children}: HeaderProps) => {
  let headerHeight = 10;
  const translateY = Animated.interpolate(scrollY, {
    inputRange: [0, headerHeight],
    outputRange: [0, headerHeight / 2],
  });

  const scale = Animated.interpolate(scrollY, {
    inputRange: [-screen.height, 0],
    outputRange: [5, 1],
    extrapolateRight: Animated.Extrapolate.CLAMP,
  });

  return (
    <Animated.View
      onLayout={({
        nativeEvent: {
          layout: {height},
        },
      }) => {
        headerHeight = height;
      }}
      style={{
        transform: [
          {
            translateY,
          },
          {
            scale,
          },
        ],
      }}>
      {children}
    </Animated.View>
  );
};

export const HomeHeader = () => {
  const [scrollY] = useValues([0], []);
  const handleOnScroll = onScroll({y: scrollY});
  const {styles} = useStyles();

  return (
    <SafeAreaView style={styles.container}>
      <AnimatedFlatList
        scrollEventThrottle={16}
        onScroll={handleOnScroll}
        ListHeaderComponent={
          <ListHeaderComponent scrollY={scrollY}>
            <View>
              <Image
                source={{uri: fakeImage.uri}}
                style={{
                  width: fakeImage.width,
                  height: fakeImage.height,
                }}>
                <Gradient
                  style={styles.fill}
                  colors={['rgba(0,0,0,0.6)', 'rgba(0,0,0,0.1)']}
                />
              </Image>
              <Gradient
                style={styles.fill}
                colors={['rgba(0,0,0,0.6)', 'rgba(0,0,0,0.1)']}
              />
            </View>
          </ListHeaderComponent>
        }
        data={fakeData}
        keyExtractor={(item: Post, index: number) => `${index}-${item.title}`}
        renderItem={({index, item}: ListRenderItemInfo<Post>) => (
          <Item {...item} index={index} />
        )}
      />
    </SafeAreaView>
  );
};

const useStyles = makeStyles(({theme}) => ({
  container: {
    flex: 1,
  },
  itemContainer: {
    backgroundColor: theme.colors.background,
  },
  card: {
    backgroundColor: theme.colors.card,
    margin: 10,
    shadowColor: theme.colors.card,
    shadowOpacity: 0.4,
    shadowRadius: 5,
    borderRadius: 5,
    shadowOffset: {height: 1, width: 1},
    elevation: 2,
  },
  cardBody: {
    padding: 15,
  },
  imageContainer: {
    overflow: 'hidden',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 22,
    color: theme.colors.text,
  },
  description: {
    color: theme.colors.text,
  },
  fill: {
    ...StyleSheet.absoluteFillObject,
  },
}));
