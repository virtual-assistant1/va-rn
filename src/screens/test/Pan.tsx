import React from 'react';
import {View, Text, StyleSheet, Dimensions} from 'react-native';

import {PanGestureHandler, State} from 'react-native-gesture-handler';
import Animated from 'react-native-reanimated';
import {
  onGestureEvent,
  withDecay,
  withOffset,
  bInterpolate,
} from 'react-native-redash';
import AnimatedButton from 'components/AnimatedButton';

const {Value, diffClamp, concat} = Animated;

const screen = Dimensions.get('window');
const MAX_SNAP = screen.height - 200 - 64;

export const Pan = () => {
  const animation = new Value(0);
  const translationY = new Value(0);
  const velocityY = new Value(0);
  const state = new Value(State.UNDETERMINED);
  const handleGesture = onGestureEvent({
    translationY,
    velocityY,
    state,
  });

  // const translateY = withSpring({
  //   value: translationY,
  //   state,
  //   velocity: velocityY,
  //   snapPoints: [0, MAX_SNAP * 0.5, MAX_SNAP],
  //   config: {
  //     damping: 15,
  //     mass: 1,
  //     stiffness: 200,
  //     overshootClamping: false,
  //     restSpeedThreshold: 0.1,
  //     restDisplacementThreshold: 0.1,
  //   },
  // });

  const offsetY = withOffset(translationY, state);
  const decay = withDecay({
    value: offsetY,
    state,
    velocity: velocityY,
  });
  const translateY = diffClamp(decay, 0, MAX_SNAP);
  const scale = bInterpolate(animation, 1, 1.2);
  const opacity = bInterpolate(animation, 1, 0.6);
  const angle = bInterpolate(animation, 0, 10);
  const rotate = concat(angle, 'deg');
  return (
    <View style={styles.container}>
      <AnimatedButton
        animation={animation}
        onPress={() => {
          //Alert.alert('yes');
        }}>
        <Animated.View
          style={[styles.box2, {opacity, transform: [{scale}, {rotate}]}]}>
          <Text>Click</Text>
        </Animated.View>
      </AnimatedButton>
      <PanGestureHandler {...handleGesture}>
        <Animated.View style={[styles.box, {transform: [{translateY}]}]}>
          <Text>Move</Text>
        </Animated.View>
      </PanGestureHandler>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  box: {
    width: 200,
    height: 200,
    backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
  },
  box2: {
    width: 200,
    height: 100,
    backgroundColor: 'green',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
