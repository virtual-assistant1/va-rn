import React from 'react';
import {SafeAreaView} from 'react-native';
import Animated from 'react-native-reanimated';
import {PanGestureHandler, State} from 'react-native-gesture-handler';
import {
  onGestureEvent,
  withOffset,
  withDecay,
  useValues,
} from 'react-native-redash';
import {View, Text, Card} from 'components';
import {makeStyles} from 'hooks';

export function PanText() {
  const {styles} = useStyles();
  const [state, dragX, dragY, velocityX, velocityY] = useValues(
    [State.UNDETERMINED, 0, 0, 0, 0],
    [],
  );

  const deceleration = 0.994;

  const handleGestures = onGestureEvent({
    translationX: dragX,
    translationY: dragY,
    state,
    velocityX,
    velocityY,
  });

  const offsetX = withOffset(dragX, state);
  const offsetY = withOffset(dragY, state);

  const translateX = withDecay({
    value: offsetX,
    velocity: velocityX,
    state,
    deceleration,
  });
  const translateY = withDecay({
    value: offsetY,
    velocity: velocityY,
    state,
    deceleration,
  });

  return (
    <SafeAreaView style={styles.root}>
      <View style={styles.page}>
        <Text>Nimer Farahty</Text>
        <PanGestureHandler {...handleGestures}>
          <Animated.View style={[{transform: [{translateX}, {translateY}]}]}>
            <Card style={styles.box} />
          </Animated.View>
        </PanGestureHandler>
      </View>
    </SafeAreaView>
  );
}

const useStyles = makeStyles(({theme}) => ({
  root: {
    flex: 1,
  },
  page: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  box: {
    width: 200,
    height: 200,
    backgroundColor: theme.colors.primaryDark,
  },
}));
