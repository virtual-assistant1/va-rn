import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {TapGestureHandler, State} from 'react-native-gesture-handler';
import Animated from 'react-native-reanimated';
import {onGestureEvent, useValues} from 'react-native-redash';
import {makeStyles} from 'hooks';

const {
  Value,
  sub,
  Clock,
  interpolate,
  block,
  startClock,
  set,
  spring,
} = Animated;

function followPoint(value: Animated.Adaptable<number>) {
  const clock = new Clock();
  const config = {
    stiffness: new Value(100),
    mass: new Value(1),
    damping: new Value(10),
    overshootClamping: false,
    restSpeedThreshold: 0.001,
    restDisplacementThreshold: 0.001,
    toValue: new Value(0),
  };
  const state = {
    position: new Value(0),
    velocity: new Value(0),
    finished: new Value(0),
    time: new Value(0),
  };

  return block([
    startClock(clock),
    set(config.toValue, value),
    spring(clock, state, config),
    state.position,
  ]);
}

export function Dot() {
  const {styles} = useStyles();
  const [state, x, y] = useValues([State.UNDETERMINED, 0, 0], []);

  const handleGesture = onGestureEvent({y, state, x});
  const top = followPoint(sub(y, 50));
  const left = followPoint(sub(x, 50));
  const scale = interpolate(top, {
    inputRange: [0, 200, 600],
    outputRange: [1, 0.4, 1],
  });
  return (
    <SafeAreaView style={styles.page}>
      <TapGestureHandler {...handleGesture}>
        <Animated.View style={StyleSheet.absoluteFill}>
          <Animated.View
            style={[styles.circle, {top, left, transform: [{scale}]}]}
          />
        </Animated.View>
      </TapGestureHandler>
    </SafeAreaView>
  );
}

const useStyles = makeStyles(({theme}) => ({
  page: {
    flex: 1,
  },
  circle: {
    width: 100,
    height: 100,
    borderRadius: 50,
    backgroundColor: theme.colors.primaryDark,
  },
}));
