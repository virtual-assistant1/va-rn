import React, {Component} from 'react';
import {SafeAreaView, StyleSheet, Button} from 'react-native';
import {loop} from 'react-native-redash';
import {
  Svg,
  Rect,
  Path,
  Circle,
  Ellipse,
  Defs,
  LinearGradient,
  Stop,
} from 'react-native-svg';
import Animated, {Easing} from 'react-native-reanimated';

const {Value, concat, Clock, interpolate, cond, eq} = Animated;
const STATE = {
  STOP: 0,
  PLAY: 1,
};

const AnimatedPath = Animated.createAnimatedComponent(Path);

export interface HomeProps {}

export class TestPath extends Component {
  animation: Animated.Node<number>;
  path: Animated.Node<string>;
  clock: Animated.Clock;
  toggle: Animated.Value<number>;

  constructor(props: HomeProps) {
    super(props);
    this.clock = new Clock();
    this.handlePushScreen = this.handlePushScreen.bind(this);
    this.toggle = new Value(STATE.PLAY);
    const progress = loop({
      clock: this.clock,
      easing: Easing.inOut(Easing.ease),
      duration: 1000,
      boomerang: true,
    });
    this.animation = cond(
      eq(this.toggle, STATE.PLAY),
      interpolate(progress, {
        inputRange: [0, 0.8, 1],
        outputRange: [10, 10, 8],
      }),
      10,
    );
    this.path = concat('M 40 60 A ', this.animation, ' 10 0 0 0 60 60');
  }

  handlePushScreen() {}

  render() {
    return (
      <SafeAreaView style={styles.body}>
        <Button title="PUSH SCREEN" onPress={this.handlePushScreen} />
        <Svg
          width={200}
          height={200}
          viewBox="0 0 100 100"
          onPress={() => {
            this.toggle.setValue(this.toggle ? STATE.PLAY : STATE.STOP);
          }}>
          <Ellipse
            cx="50"
            cy="50"
            fill="red"
            rx="30"
            ry="20"
            stroke="green"
            strokeWidth="10"
          />
        </Svg>

        <Svg width={200} height={200} viewBox="0 0 100 100">
          <Defs>
            <LinearGradient x1="50" y1="0" x2="50" y2="50" id="gradient">
              <Stop offset="0" stopColor="red" stopOpacity="0.6" />
              <Stop offset="1" stopColor="yellow" stopOpacity="0.2" />
            </LinearGradient>
          </Defs>
          <Rect
            width="100"
            height="100"
            fill="url(#gradient)"
            stroke="black"
            strokeWidth={10}
          />
          <Circle cx="50" cy="50" r="30" fill="yellow" />
          <Circle cx="40" cy="40" r="4" fill="black" />
          <Circle cx="60" cy="40" r="4" fill="black" />
          <AnimatedPath d={this.path} stroke="black" />
        </Svg>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    height: 200,
    width: 200,
    backgroundColor: 'red',
  },
});
