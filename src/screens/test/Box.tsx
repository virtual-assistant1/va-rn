import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import Animated from 'react-native-reanimated';
import faker from 'faker';
import {bInterpolate} from 'react-native-redash';
import AnimatedButton from '/components/AnimatedButton';

const {Value, concat, useCode, debug} = Animated;

export function Box() {
  const animation = new Value(0);
  const scale = bInterpolate(animation, 1, 1.8);
  const angle = bInterpolate(animation, 0, 10);
  const opacity = bInterpolate(animation, 1, 0.7);

  useCode(() => debug('Animation', animation), [animation]);
  return (
    <SafeAreaView style={styles.page}>
      <AnimatedButton animation={animation} onPress={() => {}}>
        <Animated.View style={{height: 250, width: 250, overflow: 'hidden'}}>
          <Animated.Image
            source={{
              uri: faker.image.imageUrl(250, 250),
              height: 250,
              width: 250,
            }}
            style={{
              transform: [{scale}, {rotate: concat(angle, 'deg')}],
              opacity,
            }}
          />
        </Animated.View>
      </AnimatedButton>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
