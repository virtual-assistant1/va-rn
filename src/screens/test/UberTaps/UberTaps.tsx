import React, {useState, useRef, FC} from 'react';
import {
  SafeAreaView,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
} from 'react-native';
import Animated from 'react-native-reanimated';
import faker from 'faker';
import {onScroll, useValues, withSpringTransition} from 'react-native-redash';
import {makeStyles} from 'hooks';
import Gradient from 'react-native-linear-gradient';
import {RectButton, Image, Text, View} from 'components';
import {useNavigation} from '@react-navigation/native';
import {SharedElement} from 'react-navigation-shared-element';

const {
  useCode,
  interpolate,
  cond,
  block,
  greaterOrEq,
  lessOrEq,
  and,
  set,
  call,
  onChange,
} = Animated;

const screen = Dimensions.get('window');

const data = faker.random
  .words(8)
  .split(' ')
  .map((_, index) => ({
    id: faker.random.uuid(),
    title: faker.commerce.department(),
    index,
    data: faker.random
      .words(faker.random.number(10))
      .split(' ')
      .map(() => ({
        id: faker.random.uuid(),
        description: faker.lorem.paragraphs(3),
        name: faker.commerce.productName(),
        price: faker.commerce.price(),
        color: faker.commerce.color(),
        image: `https://picsum.photos/${Math.floor(
          screen.width,
        )}/300?tag=${faker.random.uuid()}`,
      })),
  }));

export const UberTaps = () => {
  const {styles} = useStyles();
  const [tabs, setTabs] = useState(Array(data.length).fill(0));
  const [scroll, index] = useValues([-1, 0], []);
  const scrollView = useRef<Animated.ScrollView>(null);

  useCode(
    () =>
      block(
        tabs.map((section, i) =>
          cond(
            i === tabs.length - 1
              ? greaterOrEq(scroll, section)
              : and(
                  greaterOrEq(scroll, section),
                  lessOrEq(scroll, tabs[i + 1]),
                ),
            set(index, i),
          ),
        ),
      ),
    [scroll, tabs],
  );

  const onPress = (i1: number) => {
    if (scrollView.current) {
      scrollView!.current!.getNode().scrollTo({y: tabs[i1] + 1});
    }
  };

  return (
    <SafeAreaView style={styles.root}>
      <View style={styles.container}>
        <Tabs onPress={onPress} index={index} />
        <Animated.ScrollView
          onScroll={onScroll({y: scroll})}
          ref={scrollView}
          scrollEventThrottle={16}>
          <SectionsList
            onMeasurement={(i, tab) => {
              tabs[i] = tab;

              setTabs([...tabs]);
            }}
          />
        </Animated.ScrollView>
      </View>
    </SafeAreaView>
  );
};

const SectionsList: FC<{
  onMeasurement: (index: number, position: number) => void;
}> = ({onMeasurement}) => {
  const {styles} = useStyles();
  return (
    <View>
      {data.map((section, index) => (
        <View
          key={section.id}
          onLayout={({
            nativeEvent: {
              layout: {y},
            },
          }) => onMeasurement(index, y)}>
          <View style={styles.header}>
            <Text style={styles.section}>{section.title}</Text>
          </View>
          <View>
            {section.data.map((item) => (
              <Card item={item} key={item.id} />
            ))}
          </View>
        </View>
      ))}
    </View>
  );
};

const Card: FC<{item: any}> = ({item}) => {
  const {styles} = useStyles();
  const navigation = useNavigation();
  return (
    <View>
      <View>
        <SharedElement id={item.id}>
          <Image source={{uri: item.image}} style={styles.image} />
        </SharedElement>
        <SharedElement
          id={`gradient-${item.id}`}
          {...StyleSheet.absoluteFillObject}>
          <Gradient
            colors={['rgba(0,0,0,0.4)', 'rgba(0,0,0,0.1)']}
            style={StyleSheet.absoluteFill}
          />
        </SharedElement>
        <RectButton
          style={StyleSheet.absoluteFill}
          onPress={() => {
            navigation.navigate('uberPage', {item});
          }}
        />
      </View>
      <Text style={styles.title}>{item.name}</Text>
    </View>
  );
};

const Par: FC<{
  translateX: Animated.Node<number>;
  width: Animated.Node<number>;
}> = ({translateX, width}) => {
  const {styles} = useStyles();
  return (
    <Animated.View
      style={[
        styles.par,
        {
          width,
          transform: [{translateX}],
        },
      ]}
    />
  );
};

const Tabs: FC<{
  onPress: (index: number) => void;
  index: Animated.Node<number>;
}> = ({onPress, index}) => {
  const {styles} = useStyles();
  const [tabs, setTabs] = useState(
    new Array(data.length).fill({position: 0, width: 60}),
  );
  const scrollView = useRef<Animated.ScrollView>(null);

  useCode(
    () =>
      onChange(
        index,
        call([index], ([i]) => {
          if (scrollView.current) {
            scrollView.current.getNode().scrollTo({x: tabs[i].position});
          }
        }),
      ),
    [index, tabs],
  );

  const width = interpolate(index, {
    inputRange: tabs.map((_, i) => i),
    outputRange: tabs.map((tab) => tab.width),
  });

  const translateXAnimation = interpolate(index, {
    inputRange: tabs.map((_, i) => i),
    outputRange: tabs.map((tab) => tab.position),
  });

  const translateX = withSpringTransition(translateXAnimation);

  return (
    <View style={styles.tabs}>
      <Animated.ScrollView
        ref={scrollView}
        horizontal
        scrollEventThrottle={16}
        showsHorizontalScrollIndicator={false}>
        {data.map((section, i) => (
          <Tab
            key={section.id}
            onPress={() => onPress(i)}
            section={section}
            onMeasurement={(position, width2) => {
              tabs[i] = {position, width: width2};
              setTabs([...tabs]);
            }}
          />
        ))}
        <Par translateX={translateX} width={width} />
      </Animated.ScrollView>
    </View>
  );
};

const Tab: FC<{
  onPress: () => void;
  onMeasurement: (x: number, width: number) => void;
  section: any;
}> = ({onPress, onMeasurement, section}) => {
  const {styles} = useStyles();
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[styles.tab]}
      onLayout={({
        nativeEvent: {
          layout: {x, width},
        },
      }) => onMeasurement(x, width)}>
      <Text style={styles.title}>{section && section.title}</Text>
    </TouchableOpacity>
  );
};

const useStyles = makeStyles(({theme, dimensions}) => ({
  root: {flex: 1},
  container: {
    flex: 1,
    paddingHorizontal: 10,
  },
  header: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
  },
  tabs: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    overflow: 'hidden',
    padding: 5,
  },
  tab: {
    padding: 10,
  },
  par: {
    position: 'absolute',
    borderBottomColor: theme.colors.text,
    borderBottomWidth: 2,
    height: 1,
    bottom: 0,
  },
  title: {
    color: theme.colors.text,
  },
  section: {
    color: theme.colors.text,
  },
  image: {
    width: dimensions.width,
    height: 200,
  },
}));
