import React from 'react';
import {View, Text, Image} from 'components';
import {useRoute} from '@react-navigation/native';
import {SharedElement} from 'react-navigation-shared-element';
import {makeStyles} from 'hooks';
import Gradient from 'react-native-linear-gradient';
import {StyleSheet, StatusBar} from 'react-native';

export const UberPage = () => {
  const route = useRoute();
  const {item} = route.params;
  const {styles} = useStyles();
  return (
    <View style={{flex: 1}}>
      <StatusBar barStyle="light-content" />
      <View>
        <SharedElement id={item.id}>
          <Image style={styles.image} source={{uri: item.image}} />
        </SharedElement>
        <SharedElement {...styles.fill} id={`gradient-${item.id}`}>
          <Gradient
            style={styles.fill}
            colors={['rgba(0,0,0,0.4)', 'rgba(0,0,0,0.1)']}
          />
        </SharedElement>
      </View>
      <View style={styles.cardBody}>
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.description}>{item.description}</Text>
      </View>
    </View>
  );
};

const useStyles = makeStyles(({dimensions, theme}) => ({
  image: {
    width: dimensions.width,
    height: 400,
    borderWidth: 0,
  },
  fill: {
    ...StyleSheet.absoluteFillObject,
  },
  cardBody: {
    padding: 15,
    backgroundColor: theme.colors.card,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 22,
    color: theme.colors.text,
  },
  description: {
    color: theme.colors.text,
  },
}));
