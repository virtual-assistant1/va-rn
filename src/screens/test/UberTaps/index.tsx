import React from 'react';
import {createSharedElementStackNavigator} from 'react-navigation-shared-element';
import {UberPage} from './UberPage';
import {UberTaps} from './UberTaps';

const {Navigator, Screen} = createSharedElementStackNavigator();

export default () => {
  return (
    <Navigator
      screenOptions={{
        title: '',
        headerTransparent: true,
        cardStyleInterpolator: ({current: {progress}}) => {
          const translateY = progress.interpolate({
            inputRange: [0, 1],
            outputRange: [150, 0],
            extrapolate: 'clamp',
          });
          return {cardStyle: {opacity: progress, transform: [{translateY}]}};
        },
      }}>
      <Screen name="uberTabs" component={UberTaps} />
      <Screen
        name="uberPage"
        component={UberPage}
        options={{
          headerTintColor: 'white',
        }}
        sharedElementsConfig={(route: any) => {
          const {item} = route.params;
          return [{id: item.id}, `gradient-${item.id}`];
        }}
      />
    </Navigator>
  );
};
