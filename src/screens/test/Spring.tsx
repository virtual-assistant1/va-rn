import React from 'react';
import { Options } from 'react-native-navigation';
import { SafeAreaView, StyleSheet, Text, Button } from 'react-native';
import Animated, { Easing } from 'react-native-reanimated';
import { timing, bInterpolate, loop } from 'react-native-redash';

export interface SpringProps {
  componentId: string;
}

export function Spring(props: SpringProps) {
  const animation = new Animated.Value(1);
  const isStared = new Animated.Value<0 | 1>(1);
  // const animation = timing({
  //   duration: 400,
  //   easing: Easing.inOut(Easing.cubic),
  // });
  const translateY = bInterpolate(animation, -200, 0);
  const opacity = bInterpolate(animation, 0, 1);
  const scale = bInterpolate(animation, 10, 1);
  const rotate = Animated.concat(bInterpolate(animation, 30, 0), 'deg');
  Animated.useCode(
    Animated.block([
      Animated.cond(
        Animated.eq(isStared, 1),
        Animated.set(
          animation,
          timing({ duration: 400, easing: Easing.inOut(Easing.cubic) }),
        ),
      ),
    ]),
    [isStared],
  );
  return (
    <SafeAreaView style={styles.page}>
      <Button
        title="Start"
        onPress={() => {
          isStared.setValue(1);
        }}
      />
      <Animated.View
        style={{
          opacity,
          transform: [{ translateY }, { scale }, { rotate }],
        }}>
        <Text>{'Welcome To Spring'}</Text>
      </Animated.View>
    </SafeAreaView>
  );
}

Spring.options = (): Options => ({
  topBar: {
    title: {
      text: 'Spring',
    },
  },
});

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
