import React, {useCallback} from 'react';
import {View, Button, Card, Input, Image} from 'components';
import {makeStyles, useForm, useAction} from 'hooks';
import {useMutation} from '@apollo/client';
import {LoginDocument, LoginMutationVariables} from 'generated/types';
import {Alert} from 'react-native';
import {useNavigation} from '@react-navigation/core';
import AsyncStorage from '@react-native-community/async-storage';
import config from 'config';

const aspectRation = 1600 / 1200;

export const Login = () => {
  const {styles} = useStyle();
  const navigation = useNavigation();
  const setIsAuth = useAction((actions) => actions.settings.setIsAuth);

  const [doLogin, {loading}] = useMutation(LoginDocument, {
    onCompleted: async ({login: {accessToken, refreshToken}}) => {
      await Promise.all([
        AsyncStorage.setItem(config.accessTokenKey, accessToken),
        AsyncStorage.setItem(config.refreshTokenKey, refreshToken),
      ]);

      setIsAuth(true);
    },
    onError: () => {
      Alert.alert('Login Failed', 'ops , login failed');
    },
  });

  const {inputHandlers, handleSubmit} = useForm<LoginMutationVariables>({
    initial: {
      email: '',
      password: '',
    },
    onSubmit: (f) => {
      doLogin({variables: f});
    },
    validationSchema: (f) => ({
      email: f.string().email().required(),
      password: f.string().required(),
    }),
  });

  const handleSignUp = useCallback(() => {
    navigation.navigate('RegisterStack');
  }, [navigation]);

  return (
    <View style={styles.root}>
      <View style={styles.header}>
        <Image
          source={require('assets/health_check.png')}
          style={styles.image}
        />
      </View>
      <View style={styles.content}>
        <Card style={styles.card}>
          <Input
            {...inputHandlers('email')}
            placeholder="enter your email"
            icon={{name: 'user', type: 'Entypo'}}
            autoCapitalize="none"
            keyboardType="email-address"
          />
          <Input
            {...inputHandlers('password')}
            placeholder="enter your password"
            icon={{
              name: 'form-textbox-password',
              type: 'MaterialCommunityIcons',
            }}
            secureTextEntry
          />
          <View style={styles.controls}>
            <Button
              style={styles.button}
              title="Sign Up"
              onPress={handleSignUp}
            />
            <Button
              style={styles.button}
              title="Login"
              loading={loading}
              onPress={handleSubmit}
            />
          </View>
        </Card>
      </View>
    </View>
  );
};

const useStyle = makeStyles(({dimensions}) => ({
  root: {
    flex: 1,
    paddingTop: 80,
  },

  card: {
    padding: 20,
  },

  button: {
    padding: 10,
    minWidth: 120,
  },

  controls: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 30,
  },
  icon: {
    fontSize: 150,
  },
  header: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    margin: 20,
    flex: 4,
  },
  image: {
    width: dimensions.width * 0.55 * aspectRation,
    height: dimensions.width * 0.55,
  },
}));
