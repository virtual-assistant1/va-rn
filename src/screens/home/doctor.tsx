import React, {FC} from 'react';
import {View, Button} from 'components';
import {makeStyles} from 'hooks';
import {useNavigation} from '@react-navigation/core';
import {User} from 'generated/types';
import moment from 'moment';

interface DoctorProps {
  user: User;
}

export const Doctor: FC<DoctorProps> = ({user}) => {
  const {styles} = useStyles();
  const navigation = useNavigation();

  return (
    <View style={styles.root}>
      <Button
        title="Diagnostic your self"
        style={styles.btn}
        titleProps={{style: {textTransform: 'none'}}}
        onPress={() => {
          navigation.navigate('RegisterUserPropsStack', {
            ...user,
            dob: moment(user.dob).unix(),
          });
        }}
      />
      <Button
        title="Patient Diagnostic"
        titleProps={{style: {textTransform: 'none'}}}
        style={styles.btn}
        onPress={() => {
          navigation.navigate('userPropsStack');
        }}
      />
    </View>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    flexDirection: 'row',
  },
  btn: {
    minWidth: 120,
    padding: 15,
    margin: 10,
  },
}));
