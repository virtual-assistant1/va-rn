import React, {FC} from 'react';
import {makeStyles} from 'hooks';
import {View, Text, Loader} from 'components';
import {useQuery} from '@apollo/client';
import {MeDocument} from 'generated/types';
import LinearGradient from 'react-native-linear-gradient';

import {Doctor} from './doctor';
import {Patient} from './patient';
import {HomeIcon} from './HomeIcon';
import {StyleSheet} from 'react-native';

const aspectRation = 1600 / 1200;

const FACTOR = 0.9;

export const Home = () => {
  const {styles, theme} = useStyles();
  const {data, error, loading} = useQuery(MeDocument);

  if (error) {
    return (
      <View style={styles.root}>
        <Text>{JSON.stringify(error)}</Text>
      </View>
    );
  }

  if (loading) {
    return (
      <View style={styles.loaderContainer}>
        <Loader color={theme.colors.primary} size={50} />
      </View>
    );
  }

  if (!data?.me) {
    return (
      <View style={styles.root}>
        <Text>user not found</Text>
      </View>
    );
  }

  const user = data?.me;

  return (
    <View style={styles.root}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>Virtual Assessment</Text>
        <Text style={styles.subTitle}>DST Advisor</Text>
      </View>
      <View style={styles.container}>
        <View style={styles.rectangle}>
          <Gradient borderRadius={40} />
        </View>
        <View style={styles.centerContainer}>
          <HomeIcon width={350} />
        </View>
      </View>

      <View style={styles.btnContainer}>
        {data?.me.userType === 'patient' ? (
          <Patient user={user} />
        ) : (
          <Doctor user={user} />
        )}
      </View>
    </View>
  );
};

const useStyles = makeStyles(({dimensions}) => ({
  root: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  card: {
    padding: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    minWidth: 120,
    padding: 10,
    margin: 10,
  },
  btnContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  loaderContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  rectangle: {
    width: dimensions.width * FACTOR,
    height: dimensions.width * FACTOR,
    transform: [
      {translateX: -(dimensions.width * FACTOR * 0.5)},
      {rotate: '45deg'},
    ],
    justifyContent: 'center',
    alignItems: 'center',
  },

  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },

  centerContainer: {
    alignItems: 'center',
    position: 'absolute',
  },
  title: {
    opacity: 0.9,
    fontSize: 35,
    fontWeight: 'bold',
  },
  subTitle: {
    fontSize: 30,
    opacity: 0.5,
  },
  titleContainer: {
    marginRight: 30,
    justifyContent: 'center',
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
    flex: 1,
  },

  image: {
    width: dimensions.width * 0.7 * aspectRation,
    height: dimensions.width * 0.7,
  },
}));

const Gradient: FC<{borderRadius: number}> = ({borderRadius}) => {
  return (
    <LinearGradient
      style={{...StyleSheet.absoluteFillObject, borderRadius}}
      colors={['#19769F', '#35D8A6']}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
    />
  );
};
