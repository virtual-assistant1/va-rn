import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Gradient, LeftButton, RightButton} from 'components';
import {Home} from './home';
import {Assessment} from './Assessment';

const Stack = createStackNavigator();

export const HomeStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackground: () => <Gradient />,
        headerTintColor: '#fff',
        headerRight: () => <RightButton />,
      }}>
      <Stack.Screen
        component={Home}
        name="Home"
        options={{headerLeft: () => <LeftButton />}}
      />
      <Stack.Screen component={Assessment} name="Assessment" />
    </Stack.Navigator>
  );
};
