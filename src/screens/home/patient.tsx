import React, {FC} from 'react';
import {View, Button} from 'components';
import {makeStyles} from 'hooks';
import {useNavigation} from '@react-navigation/core';
import moment from 'moment';
import {User} from 'generated/types';

interface PatientProps {
  user: User;
}

export const Patient: FC<PatientProps> = ({user}) => {
  const {styles} = useStyles();
  const navigation = useNavigation();

  return (
    <View style={styles.root}>
      <Button
        title="Lets start"
        style={styles.btn}
        onPress={() => {
          navigation.navigate('RegisterUserPropsStack', {
            ...user,
            dob: moment(user.dob).unix(),
          });
        }}
      />
    </View>
  );
};

const useStyles = makeStyles(() => ({
  root: {},
  btn: {
    minWidth: 120,
    padding: 10,
    margin: 10,
  },
}));
