import React from 'react';
import {View, Button, Card, Input} from 'components';
import {makeStyles, useForm} from 'hooks';
import {useLazyQuery} from '@apollo/client';
import {PredictDocument, PredictQueryVariables} from 'generated/types';
import {Alert} from 'react-native';

export const Assessment = () => {
  const {styles} = useStyle();

  const [doAssessment, {loading}] = useLazyQuery(PredictDocument, {
    onCompleted: (r) => {
      if (r.predict.error) {
        Alert.alert('Error', 'Something went wrong');
      } else {
        Alert.alert('Results', r.predict.message);
      }
    },
    onError: (e) => {
      Alert.alert(JSON.stringify(e));
    },
  });

  const {inputHandlers, handleSubmit} = useForm<PredictQueryVariables>({
    initial: {
      systolic: 119.2,
      diastolic: 80.2,
      age_year: 15,
      cholesterol: 2,
      active: 1,
      alcohol: 1,
      gender: 1,
      glucose: 1,
      height: 1,
      smoke: 1,
      weight: 90,
    },
    onSubmit: (f) => {
      doAssessment({
        variables: {
          age_year: parseFloat(`${f.age_year}`),
          cholesterol: parseFloat(`${f.cholesterol}`),
          diastolic: parseFloat(`${f.diastolic}`),
          systolic: parseFloat(`${f.systolic}`),
          active: parseFloat(`${f.age_year}`),
          alcohol: parseFloat(`${f.age_year}`),
          gender: parseFloat(`${f.age_year}`),
          glucose: parseFloat(`${f.age_year}`),
          height: parseFloat(`${f.age_year}`),
          smoke: parseFloat(`${f.age_year}`),
          weight: parseFloat(`${f.age_year}`),
        },
      });
    },
    validationSchema: (f) => ({
      systolic: f.number().required(),
      diastolic: f.number().required(),
      age_year: f.number().required(),
      cholesterol: f.number().required(),
      active: f.number().required(),
      alcohol: f.number().required(),
      gender: f.number().required(),
      glucose: f.number().required(),
      height: f.number().required(),
      smoke: f.number().required(),
      weight: f.number().required(),
    }),
  });

  return (
    <View style={styles.root}>
      <Card style={styles.card}>
        <Input
          {...inputHandlers('systolic')}
          placeholder="enter your systolic value"
          icon={{name: 'user', type: 'Entypo'}}
          keyboardType="email-address"
          autoCapitalize="none"
        />
        <Input
          {...inputHandlers('diastolic')}
          placeholder="enter your diastolic"
          icon={{name: 'user', type: 'Entypo'}}
          keyboardType="email-address"
          autoCapitalize="none"
        />
        <Input
          {...inputHandlers('age_year')}
          placeholder="enter your age"
          icon={{name: 'upcircleo', type: 'AntDesign'}}
        />
        <Input
          {...inputHandlers('cholesterol')}
          placeholder="enter your cholesterol value 1,2,3"
          icon={{name: 'user', type: 'Entypo'}}
          keyboardType="email-address"
        />

        <View style={styles.controls}>
          <Button
            style={styles.button}
            title="Assess"
            onPress={handleSubmit}
            loading={loading}
          />
        </View>
      </Card>
    </View>
  );
};

const useStyle = makeStyles(() => ({
  root: {
    flex: 1,
    paddingTop: 30,
  },

  card: {
    margin: 20,
    padding: 20,
  },

  button: {
    padding: 10,
    minWidth: 120,
  },

  controls: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 30,
  },
}));
