export type GenderType = 'male' | 'female';

export type userPropsStackParamList = {
  Gender: undefined;
  DateOfBirth: {gender: GenderType};
  WeightHeight: {gender: GenderType; dob: number};
  Habits: {gender: GenderType; dob: number; weight: number; height: number};
  CholesterolAndGlucose: {
    gender: GenderType;
    dob: number;
    weight: number;
    height: number;
    smoke: number;
    alcoholic: number;
    physicalActivity: number;
  };
  HeartPressure: {
    gender: GenderType;
    dob: number;
    weight: number;
    height: number;
    smoke: number;
    alcoholic: number;
    physicalActivity: number;
    cholesterol: number;
    glucose: number;
  };
};
