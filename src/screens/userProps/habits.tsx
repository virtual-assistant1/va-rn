import React, {FC, useState, useCallback} from 'react';
import {View, Text, Card, Button, RadioGroup, Slider} from 'components';
import {makeStyles} from 'hooks';
import {useNavigation, useRoute} from '@react-navigation/core';
import {StackNavigationProp} from '@react-navigation/stack';
import {RouteProp} from '@react-navigation/native';
import {userPropsStackParamList} from './stackType';

interface HabitsProps {}
var smoke_options = [
  {id: 1, label: 'Currently Smoke'},
  {id: 2, label: 'Quit in the last 12 months'},
  {id: 3, label: 'I quit more than 12 months ago'},
  {id: 0, label: "I've never been a smoker"},
];

export const Habits: FC<HabitsProps> = () => {
  const {styles} = useStyles();
  const navigation = useNavigation<
    StackNavigationProp<userPropsStackParamList>
  >();
  const {params} = useRoute<RouteProp<userPropsStackParamList, 'Habits'>>();
  const [smoke, setSmoke] = useState(0);
  const [physicalActivity, setPhysicalActivity] = useState(0);
  const [alcoholic, setAlcoholic] = useState(0);

  console.log(params);
  const handleNext = useCallback(() => {
    const finalSmoke = smoke !== 0 ? 1 : 0;
    const finalAlcoholic = alcoholic <= 10 ? 0 : 1;
    const finalPhysicalActivity = physicalActivity <= 2.5 ? 0 : 1;
    navigation.navigate('CholesterolAndGlucose', {
      ...params,
      alcoholic: finalAlcoholic,
      smoke: finalSmoke,
      physicalActivity: finalPhysicalActivity,
    });
  }, [navigation, params, alcoholic, smoke, physicalActivity]);

  return (
    <View style={styles.root}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}> Your Habits</Text>
      </View>
      <Card style={styles.card}>
        <View style={styles.HabitContainer}>
          <View style={styles.labelContainer}>
            <Text style={styles.label}>What's your smoking status?</Text>
          </View>
          <RadioGroup
            horizontal={false}
            options={smoke_options}
            onChange={({id}) => {
              setSmoke(id);
            }}
            itemStyle={styles.item}
          />
        </View>

        <View style={styles.sliderContainer}>
          <Text style={styles.label}>
            How many alcoholic drinks do you have a week?
          </Text>
          <Text>Alcoholic {`: ${alcoholic} `}</Text>

          <Slider
            minimumValue={0}
            maximumValue={15}
            minimumTrackTintColor="#7BE495"
            maximumTrackTintColor="#40AB9B"
            step={1}
            value={alcoholic}
            onValueChange={setAlcoholic}
          />
        </View>

        <View style={styles.sliderContainer}>
          <Text style={styles.label}>
            How much physical activity do you get each week?
          </Text>
          <Text>Physical activity {`: ${physicalActivity} `}</Text>

          <Slider
            minimumValue={0}
            maximumValue={10}
            minimumTrackTintColor="#7BE495"
            maximumTrackTintColor="#40AB9B"
            step={0.5}
            value={physicalActivity}
            onValueChange={setPhysicalActivity}
          />
        </View>
      </Card>

      <View style={styles.btnContainer}>
        <Button style={styles.btn} title="Next" onPress={handleNext} />
      </View>
    </View>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    flex: 1,
  },
  titleContainer: {
    flex: 1,
    marginHorizontal: 30,
    justifyContent: 'center',
  },
  title: {
    fontSize: 25,
    opacity: 0.8,
    fontWeight: 'bold',
  },

  card: {
    flex: 3,
    marginHorizontal: 20,
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  btn: {
    padding: 15,
    minWidth: 250,
  },
  btnContainer: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  HabitContainer: {},
  labelContainer: {
    marginVertical: 10,
    minWidth: 110,
  },
  label: {
    fontWeight: 'bold',
    marginTop: 10,
  },
  item: {
    minWidth: 130,
  },
  sliderContainer: {
    flex: 1,
  },
}));
