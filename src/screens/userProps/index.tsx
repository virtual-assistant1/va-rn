import React from 'react';
import {Gradient, LeftButton, RightButton} from 'components';
import {DateOfBirth} from './dob';
import {Gender} from './gender';
import {WeightHeight} from './WeightHeight';
import {Habits} from './habits';
import {CholesterolAndGlucose} from './cholesterolAndGlucose';
import {HeartPressure} from './heartPressure';
import {createStackNavigator} from '@react-navigation/stack';
import {userPropsStackParamList} from './stackType';

const Stack = createStackNavigator<userPropsStackParamList>();

export const UserPropsStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackground: () => <Gradient />,
        headerTintColor: '#fff',

        headerRight: () => <RightButton />,
      }}>
      <Stack.Screen
        component={Gender}
        name="Gender"
        options={{headerLeft: () => <LeftButton />}}
      />
      <Stack.Screen component={DateOfBirth} name="DateOfBirth" />
      <Stack.Screen component={WeightHeight} name="WeightHeight" />
      <Stack.Screen component={Habits} name="Habits" />
      <Stack.Screen
        component={CholesterolAndGlucose}
        name="CholesterolAndGlucose"
      />
      <Stack.Screen component={HeartPressure} name="HeartPressure" />
    </Stack.Navigator>
  );
};
