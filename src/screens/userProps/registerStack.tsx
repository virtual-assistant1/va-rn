import React from 'react';
import {Gradient, RightButton} from 'components';
import {Habits} from './habits';
import {CholesterolAndGlucose} from './cholesterolAndGlucose';
import {HeartPressure} from './heartPressure';
import {createStackNavigator} from '@react-navigation/stack';
import {UserRegisterParams} from './registerType';

const Stack = createStackNavigator<UserRegisterParams>();

export const RegisterUserPropsStack = ({route}: {route: any}) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackground: () => <Gradient />,
        headerTintColor: '#fff',

        headerRight: () => <RightButton />,
      }}>
      <Stack.Screen
        component={Habits}
        name="Habits"
        initialParams={{...route.params}}
      />
      <Stack.Screen
        component={CholesterolAndGlucose}
        name="CholesterolAndGlucose"
      />
      <Stack.Screen component={HeartPressure} name="HeartPressure" />
    </Stack.Navigator>
  );
};
