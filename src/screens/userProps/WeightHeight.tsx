import React, {FC, useState, useCallback} from 'react';
import {View, Text, Card, Button, Slider} from 'components';
import {makeStyles} from 'hooks';
import {useNavigation, useRoute} from '@react-navigation/core';
import {StackNavigationProp} from '@react-navigation/stack';
import {RouteProp} from '@react-navigation/native';
import {userPropsStackParamList} from './stackType';

interface WeightHeightProps {}

export const WeightHeight: FC<WeightHeightProps> = () => {
  const {styles} = useStyles();
  const navigation = useNavigation<
    StackNavigationProp<userPropsStackParamList>
  >();
  const {params} = useRoute<
    RouteProp<userPropsStackParamList, 'WeightHeight'>
  >();
  const [weight, setWeight] = useState(80);
  const [height, setHeight] = useState(160);

  const handleNext = useCallback(() => {
    navigation.navigate('Habits', {...params, height, weight});
  }, [weight, navigation, params, height]);

  return (
    <View style={styles.root}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}> Your Weight and Height</Text>
      </View>
      <Card style={styles.card}>
        <View style={styles.sliderContainer}>
          <Text>Weight {`: ${weight} KG`}</Text>
          <Slider
            minimumValue={1}
            maximumValue={200}
            minimumTrackTintColor="#7BE495"
            maximumTrackTintColor="#40AB9B"
            step={1}
            value={weight}
            onValueChange={setWeight}
          />
        </View>

        <View style={styles.sliderContainer}>
          <Text>Height {`: ${height} CM`}</Text>
          <Slider
            minimumValue={1}
            maximumValue={200}
            minimumTrackTintColor="#7BE495"
            maximumTrackTintColor="#40AB9B"
            step={1}
            value={height}
            onValueChange={setHeight}
          />
        </View>
      </Card>

      <View style={styles.btnContainer}>
        <Button style={styles.btn} title="Next" onPress={handleNext} />
      </View>
    </View>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    flex: 1,
  },
  titleContainer: {
    flex: 1,
    marginHorizontal: 30,
    justifyContent: 'center',
  },
  title: {
    fontSize: 25,
    opacity: 0.8,
    fontWeight: 'bold',
  },

  card: {
    flex: 2,
    marginHorizontal: 30,

    paddingVertical: 60,
    paddingHorizontal: 15,
  },
  btn: {
    padding: 15,
    minWidth: 250,
  },
  btnContainer: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  sliderContainer: {
    flex: 1,
  },
  slider: {
    flex: 1,
  },
}));
