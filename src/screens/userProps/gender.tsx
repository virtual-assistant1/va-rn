/* eslint-disable react-native/no-inline-styles */
import React, {FC, useState, useCallback} from 'react';
import {View, Text, Card, Button, Icon, RectButton} from 'components';
import {makeStyles} from 'hooks';
import {StyleSheet, Alert} from 'react-native';
import MaskedView from '@react-native-community/masked-view';
import RadialGradient from 'react-native-radial-gradient';
import {useNavigation} from '@react-navigation/core';
import {StackNavigationProp} from '@react-navigation/stack';
import {userPropsStackParamList, GenderType} from './stackType';

interface GenderProps {}

const ACTIVE_COLORS = ['#7BE495', '#40AB9B', '#35A09C', '#329D9C'];
const INACTIVE_COLORS = ['#ccc', '#bbb', '#000', '#000'];

export const Gender: FC<GenderProps> = () => {
  const {styles} = useStyles();
  const navigation = useNavigation<
    StackNavigationProp<userPropsStackParamList>
  >();

  const [gender, setGender] = useState<GenderType | null>(null);

  const handleMale = useCallback(() => {
    setGender('male');
  }, []);

  const handleFemale = useCallback(() => {
    setGender('female');
  }, []);

  const handleNext = useCallback(() => {
    if (!gender) {
      Alert.alert('Validation', 'Select your gender');
      return;
    }
    navigation.navigate('DateOfBirth', {gender});
  }, [gender, navigation]);

  return (
    <View style={styles.root}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}> Your Gender</Text>
      </View>
      <Card style={styles.card}>
        <RectButton style={styles.genderContainer} onPress={handleMale}>
          <MaskedView
            style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}
            maskElement={
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  flex: 1,
                }}>
                <Icon
                  name="male"
                  type="FontAwesome"
                  style={[styles.genderIcon]}
                />
              </View>
            }>
            <RadialGradient
              style={{flex: 1}}
              colors={gender === 'male' ? ACTIVE_COLORS : INACTIVE_COLORS}
              center={[0, 0]}
              radius={300}
            />
          </MaskedView>
          <Text>Male</Text>
        </RectButton>
        <RectButton style={styles.genderContainer} onPress={handleFemale}>
          <MaskedView
            style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}
            maskElement={
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  flex: 1,
                }}>
                <Icon
                  name="female"
                  type="FontAwesome"
                  style={[styles.genderIcon]}
                />
              </View>
            }>
            <RadialGradient
              style={{flex: 1}}
              colors={gender === 'female' ? ACTIVE_COLORS : INACTIVE_COLORS}
              center={[0, 0]}
              radius={300}
            />
          </MaskedView>
          <Text>Female</Text>
        </RectButton>
      </Card>
      <View style={styles.btnContainer}>
        <Button style={styles.btn} title="Next" onPress={handleNext} />
      </View>
    </View>
  );
};

const useStyles = makeStyles(({theme}) => ({
  root: {
    flex: 1,
  },
  card: {
    flex: 2,
    marginHorizontal: 30,
    flexDirection: 'row',
    paddingVertical: 60,
    paddingHorizontal: 15,
  },
  btnContainer: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  titleContainer: {
    flex: 1,
    marginHorizontal: 30,
    justifyContent: 'center',
  },
  title: {
    fontSize: 25,
    opacity: 0.8,
    fontWeight: 'bold',
  },
  genderContainer: {
    padding: 20,
    margin: 15,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: theme.colors.border,
    borderRadius: 5,
  },
  genderIcon: {
    fontSize: 120,
    opacity: 0.8,
    marginBottom: 10,
  },
  btn: {
    padding: 15,
    minWidth: 250,
  },
}));
