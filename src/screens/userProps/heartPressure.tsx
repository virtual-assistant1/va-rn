import React, {FC, useState, useCallback} from 'react';
import {View, Text, Card, Button, Slider} from 'components';
import {makeStyles, useAction} from 'hooks';
import {useNavigation, useRoute} from '@react-navigation/core';
import {RouteProp} from '@react-navigation/native';
import {userPropsStackParamList} from './stackType';
import {Platform, Linking} from 'react-native';

interface HeartPressureProps {}

const link =
  Platform.OS === 'android'
    ? 'https://play.google.com/store/apps/details?id=com.smartbloodpressure&hl=en'
    : 'https://apps.apple.com/us/app/smartbp-smart-blood-pressure/id519076558';

export const HeartPressure: FC<HeartPressureProps> = () => {
  const {styles} = useStyles();
  const navigation = useNavigation();
  const startPredict = useAction((actions) => actions.prediction.predict);
  const {params} = useRoute<
    RouteProp<userPropsStackParamList, 'HeartPressure'>
  >();
  const [systolic, setSystolic] = useState(80);
  const [diastolic, setDiastolic] = useState(100);

  const handleLink = useCallback(() => {
    Linking.canOpenURL(link).then((yes) => {
      if (yes) {
        Linking.openURL(link);
      }
    });
  }, []);

  const handleNext = useCallback(() => {
    startPredict({
      ...params,
      systolic,
      diastolic,
    });
    navigation.navigate('PredictorStack');
  }, [navigation, params, startPredict, systolic, diastolic]);

  return (
    <View style={styles.root}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>
          Your Systolic and Diastolic blood pressure
        </Text>
      </View>
      <Card style={styles.card}>
        <View style={styles.sliderContainer}>
          <Text>Systolic blood pressure {`: ${systolic} `}</Text>
          <Slider
            minimumValue={90}
            maximumValue={150}
            minimumTrackTintColor="#7BE495"
            maximumTrackTintColor="#40AB9B"
            step={1}
            value={systolic}
            onValueChange={setSystolic}
          />
        </View>

        <View style={styles.sliderContainer}>
          <Text>Diastolic blood pressure {`: ${diastolic} `}</Text>
          <Slider
            minimumValue={60}
            maximumValue={100}
            minimumTrackTintColor="#7BE495"
            maximumTrackTintColor="#40AB9B"
            step={1}
            value={diastolic}
            onValueChange={setDiastolic}
          />
        </View>
        <View>
          <Text style={styles.link} onPress={handleLink}>
            you can use this tool to measure your blood pressure
          </Text>
        </View>
      </Card>

      <View style={styles.btnContainer}>
        <Button style={styles.btn} title="Next" onPress={handleNext} />
      </View>
    </View>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    flex: 1,
  },
  titleContainer: {
    flex: 1,
    marginHorizontal: 30,
    justifyContent: 'center',
  },
  title: {
    fontSize: 25,
    opacity: 0.8,
    fontWeight: 'bold',
  },

  card: {
    flex: 2,
    marginHorizontal: 30,

    paddingVertical: 60,
    paddingHorizontal: 15,
  },
  btn: {
    padding: 15,
    minWidth: 250,
  },
  btnContainer: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  sliderContainer: {
    flex: 1,
  },
  slider: {
    flex: 1,
  },

  link: {
    textDecorationLine: 'underline',
    color: 'blue',
  },
}));
