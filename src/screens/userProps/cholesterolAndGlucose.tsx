import React, {FC, useState, useCallback} from 'react';
import {View, Text, Card, Button, Slider} from 'components';
import {makeStyles} from 'hooks';
import {useNavigation, useRoute} from '@react-navigation/core';
import {StackNavigationProp} from '@react-navigation/stack';
import {RouteProp} from '@react-navigation/native';
import {userPropsStackParamList} from './stackType';

interface CholesterolAndGlucoseProps {}

export const CholesterolAndGlucose: FC<CholesterolAndGlucoseProps> = () => {
  const {styles} = useStyles();
  const navigation = useNavigation<
    StackNavigationProp<userPropsStackParamList>
  >();
  const {params} = useRoute<
    RouteProp<userPropsStackParamList, 'CholesterolAndGlucose'>
  >();
  const [cholesterol, setCholesterol] = useState(0);
  const [glucose, setGlucose] = useState(0);

  const handleNext = useCallback(() => {
    const finalGlucose = glucose <= 140 ? 1 : glucose <= 180 ? 2 : 3;
    const finalCholesterol =
      cholesterol <= 200 ? 1 : cholesterol <= 239 ? 2 : 3;
    navigation.navigate('HeartPressure', {
      ...params,
      cholesterol: finalCholesterol,
      glucose: finalGlucose,
    });
  }, [navigation, params, cholesterol, glucose]);

  return (
    <View style={styles.root}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>Your Cholesterol And Glucose Level</Text>
      </View>
      <Card style={styles.card}>
        <View style={styles.sliderContainer}>
          <Text>Cholesterol {`: ${cholesterol} `}</Text>

          <Slider
            minimumValue={80}
            maximumValue={300}
            minimumTrackTintColor="#7BE495"
            maximumTrackTintColor="#40AB9B"
            step={1}
            value={cholesterol}
            onValueChange={setCholesterol}
          />
        </View>

        <View style={styles.sliderContainer}>
          <Text>Glucose {`: ${glucose} `}</Text>
          <Slider
            minimumValue={60}
            maximumValue={300}
            minimumTrackTintColor="#7BE495"
            maximumTrackTintColor="#40AB9B"
            step={1}
            value={glucose}
            onValueChange={setGlucose}
          />
        </View>
      </Card>

      <View style={styles.btnContainer}>
        <Button style={styles.btn} title="Next" onPress={handleNext} />
      </View>
    </View>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    flex: 1,
  },
  titleContainer: {
    flex: 1,
    marginHorizontal: 30,
    justifyContent: 'center',
  },
  title: {
    fontSize: 25,
    opacity: 0.8,
    fontWeight: 'bold',
  },

  card: {
    flex: 2,
    marginHorizontal: 20,

    paddingVertical: 20,
    paddingHorizontal: 15,
  },
  btn: {
    padding: 15,
    minWidth: 250,
  },
  btnContainer: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  labelContainer: {
    minWidth: 110,
  },
  label: {
    fontWeight: 'bold',
  },
  item: {
    minWidth: 130,
  },
  sliderContainer: {
    flex: 1,
    padding: 10,
  },
}));
