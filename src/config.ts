export default {
  api_url: 'https://va-ds.herokuapp.com/',
  //api_url: 'http://localhost:3000/',
  accessTokenKey: 'va@accessTokenKey',
  refreshTokenKey: 'va@refreshTokenKey',
};
